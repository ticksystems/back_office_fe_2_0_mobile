/*------------------------------------------------------------------
[  ISTALL  ]
*/

cordova create rics fx.rics.app Rics

cd rics

cordova platform add ios

/*------------------------------------------------------------------
[  PLUGINS  ]
*/

cordova plugin add cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-splashscreen
cordova plugin add cordova-plugin-globalization
cordova plugin add cordova-plugin-device
cordova plugin add cordova-plugin-local-notification

/*------------------------------------------------------------------
[  BUILD  ]
*/

cordova build