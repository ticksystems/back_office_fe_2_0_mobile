(function( ng, inspinia ){

    "use strict";

    inspinia.factory('helpers', [
        function() {

        	return {
        		/**
        		 * guid - generate guid
        		 * @return {String} - generated guid
        		 */
	            guid : function() {
		            function s4() {
		                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		            }
		            return ( s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4() ).toUpperCase();
	            },
	            /**
	             * exist - check if the el is undefined and add default value if it is
	             * @param  {String|Object|false} val       - el for check
	             * @param  {String|Object|false} changeTo  - default value
	             * @return {String|Object|false}
	             */
	            exist : function(val, changeTo){
	            	return ((val !== undefined) ? val : (changeTo !== undefined ? changeTo : false) ); 
	            },

	            capitalizeFirstLetter : function(string) {
					return string.charAt(0).toUpperCase() + string.slice(1);
				}
	            
        	}      
        }
    ]);

})( angular , inspinia )