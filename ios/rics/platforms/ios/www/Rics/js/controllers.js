/**
 * BACK OFFICE
 * Version 1
 *
 */

/**
 * MainCtrl - controller
 */
function MainCtrl($scope,$rootScope,$state/*,$route*/,$http,$location,apiAuthentication,$q,apiErrorStack,homePath,/*routes,*/$cookies,$window,helpers,toaster,apiAuthorization,apiLogging) {
// console.log($cookies.get('User'));

    // Welcome Text
    this.helloText = 'Welcome to Rics';
    this.descriptionText = '';

    var self = this;

        $rootScope.ENV = ENV;

    /**
     * Simple Logout actions
     */
    $scope.Logout = function(){

        apiAuthentication.Logout();
        $scope.$broadcast("logout");
        $scope.loggedin = false;

    };

    // Listen when is logged in
    $scope.$on("loggedin", function(event, current, previous, rejection){
        $scope.loggedin = true;
        // snapRemote.open('left',"sidebar");
        
        // Username -> in sidebar
        var user       = helpers.exist($cookies.get('User'),{'username':''});
        self.userName  = JSON.parse(user);
        self.userName  = self.userName.username.toUpperCase();
    })

    // Listen when is logout
    $scope.$on("logout", function(event, current, previous, rejection){
        $scope.loggedin = false;
        // snapRemote.close("sidebar");
    })

    $scope.getDomain = function(){
    	if(Settings.ProjectUrl === undefined){
    		Settings.ProjectUrl = $location.absUrl().split('#')[0];
    		
    	}
    }
    $scope.getDomain();

	/*------------------------------------------------------------------
	[  ON CHANGE ROUTING  ]
	*/

    // Log Page Enter
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                
        // We check for `last visited` cookie 
        var lastVisit   = helpers.exist($cookies.get('lastVisit'),false);
            // if there is 
        if( lastVisit ){
            // get the log
            lastVisit   = JSON.parse(lastVisit);
            // Remove cookie
            $cookies.remove('lastVisit');
            // and log it
            apiLogging.logLastVisit(lastVisit).then(
                function(r){
                    if(r){
                    }
                },
                function(){}
            );
        }

        // If comming from some page
        if(fromState.name.length > 0){
            // Exit from page
            apiLogging.logPageStay(fromState.data.pageTitle,true).then(
                function(r){
                    if(r){
                        // Enter in current page
                        apiLogging.logPageStay(toState.data.pageTitle,false);
                    }
                },
                function(){}
            );
        // After refresh/close/quit or direct link
        }else{
            apiLogging.logPageStay(toState.data.pageTitle,false);
        }
        
    });
    /**
     * onbeforeunload - set a cookie in the browser from last possible visit
     */
    $window.onbeforeunload = function() {

        // Cookie with current close time
        var d = moment().format();
            apiLogging.lastLog.time   = d;
            apiLogging.lastLog.isExit = true;
            
        // We need only logged users
        if(apiLogging.lastLog.userId && apiLogging.lastLog.userId != -1){
            $cookies.put('lastVisit' , JSON.stringify(apiLogging.lastLog) );
        }
    }

    // Google Analytics Send Page Enter
    if(ENV.ga)
        $rootScope.$on('$stateChangeSuccess', function (event,toState) {
            gtag('config', 'UA-127176958-3', {'page_path': toState.templateUrl});
        });

	// Check if we had token or not on route Change
    $rootScope.$on("$locationChangeStart", function(event, current, previous, rejection){

    	// Wait for rendering
    	$rootScope.viewCloak = false;

		// Start overlay-force layer
		$rootScope.isWorkingForce 	= true;

        // console.log("chaged");

    	// If there is no UserToken we redirect to login
    	var getUserToken = $cookies.get('UserToken'); //|| Settings.UserToken;

		if(getUserToken == undefined || getUserToken.length == 0){

			// Login
            switch ($location.path().toLowerCase()){
                case '/index/login':
                case '/':
                case '':
                    // $state.go('index.login', {});
                    // $location.path("/index/login");
                    break;
                default:{
                    
                    // Save attempt url if it not login route
                    apiAuthentication.SaveAttemptUrl();
                    apiAuthentication.RedirectToAttemptedUrl();

                    $scope.$broadcast("logout");

                    apiAuthentication.SetUnLoggedState({
                        "removeParams":true,
                        "withRedirect":true
                        // No "hardRefresh" here to preserve Attempt URL
                    });
                }
            }
        	$rootScope.viewCloak = true;
			$rootScope.isWorking = false;
			
		}else{

			
			$scope.AuthenticationPath().then(function (data){
				
				$rootScope.viewCloak 		= true;

				// if(!helpers.exist($rootScope.permissions)){
	                // Load-Reload authorization rules
	                // apiAuthorization.init().then(function(){
	                // });
            	// }
				

			})
		}
    		
    })

    // When everythig is setup init routes in module array
    // $rootScope.$on("set-active-feature", function(event, current, previous, rejection){
    //     // $scope.modules = $scope.setActiveFeature();
    //     console.log("SDSDSDS");
    //     alert("aaaa");
    // })

    $scope.AuthenticationPath = function () {

            var deferred = $q.defer();

            // Validate Token
            apiAuthentication.ValidateToken()
            .then(function (response){

                // If there is no error
                if(!response.hasError){
                	
                	// Get Token
                	var getUserToken = $cookies.get('UserToken');
                	if(getUserToken)
                		response.data = { "valid" : true };
                	else
                		response.data = { "valid" : false };

                    // If the token is valid and is defined
                    if(
                        response.data.valid && 
                        response.data.valid !== undefined 
                    )
                    {
                    	// get current path
                    	var path  	= $location.path(),
                    		// authAccess 	= false;
                    		authAccess 	= true;

                    	// access features by authorization
                    	/*
						angular.forEach(routes, function(r){
	                        r.features.forEach(function(v,i){
	                        	if(v.url.replace("#","") === path)
	                        	{
	                        		authAccess = true;
	                        	}
	                        })
						})
						*/
                    


                    	// redirect it to home page if it is:
                    	if( path == "/index/login" /*|| path == "/index/register"*/ || path == "/" || !authAccess){
                    		$location.path(homePath.url);
                    	}

                    	// broadcast event
                        $scope.$broadcast("loggedin");

	                	// if (window.innerWidth < 991) {
	                	if (window.innerWidth < 768) {
                            // if(typeof(snapRemote) != undefined)
                            //     // snapRemote.close("sidebar");
						}

                        // return promise true
						deferred.resolve(true);
                    }else{

                    	// If token is not valid or defined - reject promise
                        deferred.reject(false);

                        apiAuthentication.SetUnLoggedState({
                            "removeParams":true,
                            "withRedirect":true,
                            "hardRefresh":true
                        });
                    }
                    return deferred.promise;
                }
                // If there is error re-assign everything 
                else{
					
                    // If token is not valid or defined - reject promise
                    deferred.reject(false);
                    // console.log("a");
                    apiAuthentication.SetUnLoggedState({
                        "removeParams":true,
                        "withRedirect":true,
                        "hardRefresh":true
                    });
                }

            })

            return deferred.promise;
    }

};

angular
    .module('inspinia')
    .controller('MainCtrl', MainCtrl)