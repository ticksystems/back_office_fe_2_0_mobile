angular
    .module('managementModule',['agGrid'])

    // Users
    .controller('users',
        function users( $scope, $location, $cookies, apiAuthentication,redirectToUrlAfterLogin,homePath,$rootScope,$state,DTOptionsBuilder,DTColumnDefBuilder,apiBackOffice,$stateParams,$uibModal,$timeout) {

        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
        // $scope.errorLogin = false;
        // $scope.effect = "";
        

        $rootScope.usersData = [];
        $rootScope.checks    = {
            "Allowed" : {}
        };
        // $scope.note         = "Lorem ipsum dolor sit amet...";
        apiBackOffice.GetUsers().then(function (data) {
            
            var user = {};
            angular.forEach(data,function(v,k){
                user[v['Id']] = {};
                user[v['Id']] = {
                    "Id"        : v['Id'],
                    "UserName"  : v['UserName'],
                    "Password"  : v['Password'],
                    "Claims"    : v['Claims']
                }
            })
            $rootScope.usersData = user;
            
        })

        $scope.openConfigWidget = function (id,size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/users/configRights.html',
                size: (size||'lg'),
                controller: function($scope, $uibModalInstance,apiBackOffice) {
                    $scope.user = {};
                    $scope.user = $rootScope.usersData[id];
                    
                    $scope.userRightsForm = function(form) {
                        if ($scope.user_rights_form.$valid) {
                            // Submit as normal
                            $scope.user['Password'] = ((form.password.$viewValue && form.password.$viewValue != "xxxxxx") ? form.password.$viewValue : 'xxxxxx');

                            apiBackOffice.PutUser($scope.user).then(function (result) {
                                $rootScope.usersData[id] = $scope.user;
                                // Close current modal
                                $uibModalInstance.close();
                            })
                            // Open widget modal
                            // self.openConfigWidget();
                        } 
                        else {
                            $scope.user_rights_form.submitted = true;
                        }
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>lTfgitp')

            .withButtons([
                // {extend: 'copy'},
                // {extend: 'csv'},
                // {extend: 'excel', title: 'ExampleFile'},
                // {extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                //     customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //             .addClass('compact')
                //             .css('font-size', 'inherit');
                //     }
                // },
                {
                    text: 'Close',
                    // key: '1',
                    action: function (e, dt, node, config) {
                        // console.log('Button activated');
                        // console.log(e);
                        // console.log(dt);
                        // console.log(node);
                        // console.log(config);
                    }
                }
            ]);

        $scope.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0).withOption({
                  "columnDefs": [
                    { "width": "20%", "targets": 0 }
                  ]
            })
        ];

    })

    // Symbols
    .controller('symbols',
        function symbols( $scope, $location, $cookies, apiAuthentication,redirectToUrlAfterLogin,homePath,$rootScope,$state,apiBackOffice,$stateParams,$uibModal,$timeout) {

        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
        $rootScope.checks       = {
            "Allowed" : {}
        };
        $rootScope.symbolsData  = [];
        $rootScope.groupsData   = {};
        $rootScope.groupsDataFormated = [];
        $rootScope.isProcessing   = false;

        $scope.datatable    = {
            data : {} 
        };

        /**
         * Custom checkbox - replace for iCheck plugin.
         */
        $rootScope.symbolCollection = {};
        $scope.disabledAssignBtn    = true;

        // Add processing: ajax... 
        $rootScope.isProcessing = true;


        $scope.cellRendererFunc = function(params) {
            return '<button class="btn btn-xs btn-default" usage name="updateSymbol" ng-click="openSymbolUpdateModal(data)" ><i class="fa fa-cog"></i></button>';
        }

        // Init Symbols
        apiBackOffice.GetSymbols().then(function (data) {
            
            $rootScope.symbolsData = data;

            // Define Columns
            var columnDefs = [
                    {headerName: 'Trading Platform', field: 'TradingPlatform', cellClass: 'text-left', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: 'Symbol', field: 'Symbol', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: 'Instrument Type', field: 'InstrumentType', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: 'Group Name', field: 'TradingInstrumentGroupName', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: 'Contract Size Type', field: 'ContractSize', cellRenderer:'agAnimateShowChangeCellRenderer', filter: 'agNumberColumnFilter'},
                    {headerName: 'Underlying Ccy', field: 'UnderlyingCcy', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: 'Country', field: 'Country', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: 'Tick Size', field: 'TickSize', cellRenderer:'agAnimateShowChangeCellRenderer', filter: 'agNumberColumnFilter'},
                    {headerName: 'Tick Value', field: 'TickValue', cellRenderer:'agAnimateShowChangeCellRenderer', filter: 'agNumberColumnFilter'},
                    {headerName: 'Price Multiplier', field: 'PriceMultiplier', cellRenderer:'agAnimateShowChangeCellRenderer', filter: 'agNumberColumnFilter'},
                    {headerName: 'Digits', field: 'Digits', cellRenderer:'agAnimateShowChangeCellRenderer', filter: 'agNumberColumnFilter'},
                    {headerName: 'Trading Platform Security Name', field: 'TradingPlatformSecurityName', cellRenderer:'agAnimateShowChangeCellRenderer'},
                    {headerName: '', field: 'action', width: 90, cellRenderer: $scope.cellRendererFunc, filter: false, suppressFilter: true, suppressSorting: true}
                ],
                rowData    = [],
                inited     = false;

            var datatableInited = false;

            // Is Inited
            if(typeof($scope.datatable.data.gridOptionsApi) !== 'undefined' ){
                // Get the API
                var api      = $scope.datatable.data.gridOptionsApi,
                    apiTotal = $scope.datatable.data.gridTotalOptionsApi;
            }

            // Transform to array
            rowData = _.map(data,function(symbol,k){
                return symbol
            })

            // Configure Datatable
            $scope.datatable.data.gridOptions = {
                defaultColDef: {
                    cellClass: 'text-right'
                },
                getRowNodeId : function(data) {
                    return data.Id;
                    // return data.ticket + data.dealType;
                },

                angularCompileRows : true,

                columnDefs  : columnDefs,
                // rowData     : data,
                rowData     : rowData,
                animateRows : true,

                // Pagination
                pagination: true,
                paginationAutoPageSize:true,
                enableCellChangeFlash: true,
                // suppressHorizontalScroll: true,

                // Sorting
                enableSorting: true,
                enableFilter: true,

                // Resize
                enableColResize: true,

                // Filter
                floatingFilter: true,
                // domLayout: 'autoHeight',
                localeText: {noRowsToShow: ' '},
                // rowSelection:'single',
                rowSelection  : 'multiple',
                rowDeselection: true,
                // suppressRowClickSelection: true,
                rowMultiSelectWithClick: true,

                onGridReady: function(params){
                    api                             = params.api;
                    args                            = params;
                    $scope.datatable.data.gridOptionsApi = api;

                    // resize to fit
                    params.api.sizeColumnsToFit();
                },
                onGridSizeChanged: function(params){
                    // TODO: add functionality for small devices
                    params.api.sizeColumnsToFit();
                },
                onSelectionChanged: function(el) {
                    $timeout(function() {
                        // Get selected Rows
                        $rootScope.symbolCollection = el.api.getSelectedRows();
                        // Make button disabled or enabled
                        $scope.disabledAssignBtn    = !(!!el.api.getSelectedNodes().length);
                    }, 0);
                }

            }

            // Load Datatable Grid
            $scope.datatable.data.loading = true;


            // Load Groups
            apiBackOffice.GetGroups().then(function (groups) {
                // Assign Groups
                $rootScope.groupsData = groups;
                // Reformat Groups
                $rootScope.formatGroups();
                // Remove processing
                $rootScope.isProcessing = false;
            })
            
        })

        $rootScope.formatGroups = function(){
            $rootScope.groupsDataFormated = [];
            angular.forEach($rootScope.groupsData, function(v, k) {
                $rootScope.groupsDataFormated.push(v);
            });
        }

        /**
         * openSymbolUpdateModal
         * @param  {Object} obj 
         * @param  {String} size
         */
        $scope.openSymbolUpdateModal = function (obj,size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/symbols/symbolsUpdate.html',
                size: (size||'lg'),
                resolve:{
                    scope: function () {
                        return $scope;
                    }
                },
                controller: function($scope, $uibModalInstance, scope, apiBackOffice) {
                    // Picked Symbol
                    $scope.pickedSymbol = {};

                    // Is Inited
                    if(typeof(scope.datatable.data.gridOptionsApi) !== 'undefined' ){
                        // Get GRID API
                        api = scope.datatable.data.gridOptionsApi;
                    }

                    // Create Model
                    $scope.symbol = {
                        "country"    : {},
                        "instrument" : {}
                    };
                    $scope.pickedSymbol = obj;

                    // Set Instrument Type
                    angular.forEach($rootScope.instrumentTypes,function(v,k){
                        if(v.name == $scope.pickedSymbol['InstrumentType'])
                            $scope.symbol.instrument = v;
                    })

                    // Set Country
                    angular.forEach($rootScope.countries,function(v,k){
                        if(v.name == $scope.pickedSymbol['Country'])
                            $scope.symbol.country = v;
                    })

                    // Set UnderlyingCcy/rates
                    $scope.symbol.underlyingCcy = $rootScope.rates[$scope.pickedSymbol['UnderlyingCcy']];
                    // Set priceMultiplier
                    $scope.symbol.priceMultiplier = $scope.pickedSymbol['PriceMultiplier'];
                    // Set group
                    $scope.symbol.tradingInstrumentGroupName = $rootScope.groupsData[$scope.pickedSymbol['TradingInstrumentInfoGroupId']];

                    $scope.updateSymbol = function(form) {

                        $scope.pickedSymbol['TradingInstrumentGroupName']   = $scope.symbol['tradingInstrumentGroupName'].Name;
                        $scope.pickedSymbol['TradingInstrumentInfoGroupId'] = $scope.symbol['tradingInstrumentGroupName'].Id;

                        $scope.pickedSymbol['InstrumentType']               = $scope.symbol['instrument']['name'];
                        $scope.pickedSymbol['Country']                      = $scope.symbol['country']['name'];
                        $scope.pickedSymbol['UnderlyingCcy']                = $scope.symbol['underlyingCcy'];
                        $scope.pickedSymbol['PriceMultiplier']              = $scope.symbol['priceMultiplier'];

                        // Add processing
                        $rootScope.isProcessing = true;
                        
                        // Update Group
                        apiBackOffice.UpdateSymbol($scope.pickedSymbol).then(function (data) {

                            // Update Symbol Table
                            $rootScope.symbolsData[$scope.pickedSymbol.Id] = $scope.pickedSymbol
                            // Remove processing
                            $rootScope.isProcessing = false;

                            // Get Grid row 
                            var rowNode = api.getRowNode($scope.pickedSymbol.Id);

                            // If there is data update it
                            if(rowNode){
                                api.batchUpdateRowData({
                                    update: [$scope.pickedSymbol]
                                })
                            }

                            // Close Modal
                            $uibModalInstance.dismiss('cancel');

                            // Deselect all rows
                            api.deselectAll();

                        }, function(){
                            // On Error Remove processing
                            $rootScope.isProcessing = false;
                        })
                        // 
                        return false;
                    };


                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        };

        /**
         * openEditGroupsModal
         * @param  {[type]} size
         */
        $scope.openEditGroupsModal = function (size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/symbols/symbolsEditGroups.html',
                size: (size||'lg'),
                resolve:{
                    scope: function () {
                        return $scope;
                    }
                },
                controller: function($scope, $uibModalInstance, scope, apiBackOffice) {

                    // Create Model
                    $scope.symbol = {
                        'newGroup'   : '',
                        // 'groups'     : $rootScope.groupsDataFormated
                        'groups'     : []
                    };

                    // Is Inited
                    if(typeof(scope.datatable.data.gridOptionsApi) !== 'undefined' ){
                        // Get GRID API
                        api = scope.datatable.data.gridOptionsApi;
                    }

                    // Copy all groups
                    angular.copy($rootScope.groupsDataFormated, $scope.symbol.groups);

                    $scope.updateGroup = function(field,id,oldValue) {
                        if($scope.form[field].$viewValue.length == 0){
                            // Set Error
                            $scope.form[field].$invalid = true;
                            $scope.form[field].$dirty = true;
                            $scope.form[field].$error.required = true;
                            return false;
                        }
                        else{   
                            // log
                            var payload = {
                                'Name': $scope.form[field].$viewValue,
                                'Id'  : id
                            },
                                oldValue = oldValue || "";

                            // Add processing
                            $rootScope.isProcessing = true;

                            // Update Group
                            apiBackOffice.UpdateGroup(payload).then(function (data) {
                                $scope.form[field].$invalid = false;
                                $scope.form[field].$error.required = false;

                                // Update name in the array
                                $rootScope.groupsData[payload.Id].Name = payload.Name;

                                // Reformat groups
                                $rootScope.formatGroups();

                                // Update Model
                                // $scope.symbol.groups = $rootScope.groupsDataFormated;

                                // Copy all groups
                                angular.copy($rootScope.groupsDataFormated, $scope.symbol.groups);

                                // Put Group in the search
                                $scope.selectGroup = payload.Name;

                                // Remove processing
                                $rootScope.isProcessing = false;

                                // Change GroupName in the symbol result table
                                angular.forEach($rootScope.symbolsData,function(v,k){
                                    if(v.TradingInstrumentGroupName == oldValue){
                                        v.TradingInstrumentGroupName = payload.Name


                                        // Get Grid row 
                                        var rowNode = api.getRowNode(v.Id);

                                        // If there is data update it
                                        if(rowNode){
                                            api.batchUpdateRowData({
                                                update: [v]
                                            })
                                        }
                                    }
                                })
                            }, function(){
                                // Remove processing
                                $rootScope.isProcessing = false;
                            })
                        }
                    };
                    $scope.addGroup = function(field) {
                        // Check for empty
                        if( $scope.form[field].$viewValue.length == 0 ){

                            // Set Error
                            $scope.form[field].$invalid = true;
                            $scope.form[field].$dirty = true;
                            $scope.form[field].$error.required = true;

                            return false;
                        }
                        else{    
                            var payload = {
                                'Name':$scope.form[field].$viewValue,
                                'Id': 0
                            }

                            // Add processing
                            $rootScope.isProcessing = true;

                            apiBackOffice.AddGroup(payload).then(function (data) {

                                // Reset the field
                                $scope.symbol.newGroup = '';
                                $scope.form[field].$setPristine();
                                $scope.form[field].$setUntouched();    

                                // Add to groupsData
                                $rootScope.groupsData[data.Id] = {
                                    "Id"   : data.Id,
                                    "Name" : data.Name
                                };

                                // Reformat groups
                                $rootScope.formatGroups();
                                
                                // Update Model
                                // $scope.symbol.groups = $rootScope.groupsDataFormated;
                                // Copy all groups
                                angular.copy($rootScope.groupsDataFormated, $scope.symbol.groups);

                                // Put Group in the search
                                $scope.selectGroup = data.Name;

                                // Remove processing
                                $rootScope.isProcessing = false;
                                
                            }, function(){
                                // Remove processing
                                $rootScope.isProcessing = false;
                            })
                        }
                    };
                    $scope.submitForm = function() {
                        if (!$scope.doSubmit) {
                            return;
                        }
                        $scope.doSubmit = false;
                    }

                    $scope.modelOptions = {
                        debounce: {
                            default: 500,
                            blur: 250,
                            updateOn: ''
                        },
                        getterSetter: true
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        };


        /**
         * openEditGroupsModal
         * @param  {[type]} size
         */
        $scope.openAssignGroupsModal = function (size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/symbols/symbolsAssignGroups.html',
                size: (size||'lg'),
                resolve:{
                    scope: function () {
                        return $scope;
                    }
                },

                controller: function($scope, $uibModalInstance,scope,apiBackOffice) {
                    
                    $scope.selectedSymbols = [];

                    // Is Inited
                    if(typeof(scope.datatable.data.gridOptionsApi) !== 'undefined' ){
                        // Get GRID API
                        api = scope.datatable.data.gridOptionsApi;
                    }

                    // Collect only selected
                    // angular.forEach($rootScope.symbolCollection,function(v,k){
                    //     if(v)
                    //         $scope.selectedSymbols.push($rootScope.symbolsData[k]);
                    // });

                    // Assign selected symbols
                    $scope.selectedSymbols = $rootScope.symbolCollection;

                    $scope.assignToGroup = function(group) {

                        // assign new group and id to selected symbols
                        angular.forEach($scope.selectedSymbols,function(v,k){
                            v['TradingInstrumentGroupName']   = group.Name;
                            v['TradingInstrumentInfoGroupId'] = group.Id;
                        });

                        // Add processing
                        $rootScope.isProcessing = true;
                        
                        // array of list for prepared queue
                        var queueList = $scope.selectedSymbols;

                        // the recursive function
                        function putRecursive() {

                            // terminate if array exhausted
                            if (queueList.length === 0){
                                // Remove processing
                                $rootScope.isProcessing = false;

                                // Close Modal
                                $uibModalInstance.dismiss('cancel');

                                // Deselect all rows
                                api.deselectAll();
                                return;
                            }                                

                            // current 
                            var symbol = queueList[0];

                            // pop top value
                            queueList.shift();

                            // Update Group
                            apiBackOffice.UpdateSymbol(symbol).then(function (data) {

                                // Change GroupName in the symbol result table
                                $scope.symbolsData[symbol.Id]['TradingInstrumentGroupName']   = symbol['TradingInstrumentGroupName']
                                $scope.symbolsData[symbol.Id]['TradingInstrumentInfoGroupId'] = symbol['TradingInstrumentInfoGroupId']
                                
                                // Get Grid row 
                                var rowNode = api.getRowNode(symbol.Id);

                                // If there is data update it
                                if(rowNode){
                                    api.batchUpdateRowData({
                                        update: [symbol]
                                    })
                                }

                                // Update Symbol Table
                                putRecursive();

                            }, function(){
                                // On Error
                                $rootScope.isProcessing = false;
                            })
                        }

                        // first call
                        putRecursive();

                        return false;
                    };


                    $scope.submitForm = function() {
                        if (!$scope.doSubmit) {
                            return;
                        }
                        $scope.doSubmit = false;
                    }

                    $scope.modelOptions = {
                        debounce: {
                            default: 500,
                            blur: 250,
                            updateOn: ''
                        },
                        getterSetter: true
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        };

        /**
         * addToCollection - collect all checked elements/symbol
         * @param {Number} id
         */
        $scope.addToCollection = function(id){  
            if($rootScope.symbolCollection.hasOwnProperty(id))
                delete $rootScope.symbolCollection[id];
            else
                $rootScope.symbolCollection[id] = true;           

            $scope.disabledAssignBtn = angular.equals({},$rootScope.symbolCollection);
        }
        
        /*------------------------------------------------------------------
        [  SOME DEFAULT DATA  ]
        */
        /**
         * instrumentTypes
         * @type {Array}
         */
        $rootScope.instrumentTypes = [
            {
                "name"  : "Fx",
                "value" : 0
            },
            {
                "name"  : "Commodity",
                "value" : 1
            },
            {
                "name"  : "Index",
                "value" : 2
            },
            {
                "name"  : "Equity",
                "value" : 3
            }
        ];

        /**
         * rates
         * @type {Object}
         */
        $rootScope.rates = {
            "EUR" : "EUR",
            "USD" : "USD",
            "CHF" : "CHF",
            "GBP" : "GBP",
            "JPY" : "JPY",
            "TRY" : "TRY",
            "CAD" : "CAD",
            "DKK" : "DKK",
            "HKD" : "HKD",
            "MXN" : "MXN",
            "NOK" : "NOK",
            "NZD" : "NZD",
            "SEK" : "SEK",
            "SGD" : "SGD",
            "THB" : "THB",
            "ZAR" : "ZAR",
            "AUD" : "AUD",
            "CZK" : "CZK",
            "HUF" : "HUF",
            "PLN" : "PLN",
            "SAR" : "SAR",
            "INR" : "INR",
            "CNY" : "CNY",
            "TWD" : "TWD",
            "SWE" : "SWE",
            "RUB" : "RUB",
            "CNH" : "CNH",
            "NGN" : "NGN"
        };

        /**
         * countries
         * @type {Array}
         */
        $rootScope.countries = [
            {"name": "Global", "code": "GL"},
            {"name": "Afghanistan", "code": "AF"},
            {"name": "Åland Islands", "code": "AX"},
            {"name": "Albania", "code": "AL"},
            {"name": "Algeria", "code": "DZ"},
            {"name": "American Samoa", "code": "AS"},
            {"name": "AndorrA", "code": "AD"},
            {"name": "Angola", "code": "AO"},
            {"name": "Anguilla", "code": "AI"},
            {"name": "Antarctica", "code": "AQ"},
            {"name": "Antigua and Barbuda", "code": "AG"},
            {"name": "Argentina", "code": "AR"},
            {"name": "Armenia", "code": "AM"},
            {"name": "Aruba", "code": "AW"},
            {"name": "Australia", "code": "AU"},
            {"name": "Austria", "code": "AT"},
            {"name": "Azerbaijan", "code": "AZ"},
            {"name": "Bahamas", "code": "BS"},
            {"name": "Bahrain", "code": "BH"},
            {"name": "Bangladesh", "code": "BD"},
            {"name": "Barbados", "code": "BB"},
            {"name": "Belarus", "code": "BY"},
            {"name": "Belgium", "code": "BE"},
            {"name": "Belize", "code": "BZ"},
            {"name": "Benin", "code": "BJ"},
            {"name": "Bermuda", "code": "BM"},
            {"name": "Bhutan", "code": "BT"},
            {"name": "Bolivia", "code": "BO"},
            {"name": "Bosnia and Herzegovina", "code": "BA"},
            {"name": "Botswana", "code": "BW"},
            {"name": "Bouvet Island", "code": "BV"},
            {"name": "Brazil", "code": "BR"},
            {"name": "British Indian Ocean Territory", "code": "IO"},
            {"name": "Brunei Darussalam", "code": "BN"},
            {"name": "Bulgaria", "code": "BG"},
            {"name": "Burkina Faso", "code": "BF"},
            {"name": "Burundi", "code": "BI"},
            {"name": "Cambodia", "code": "KH"},
            {"name": "Cameroon", "code": "CM"},
            {"name": "Canada", "code": "CA"},
            {"name": "Cape Verde", "code": "CV"},
            {"name": "Cayman Islands", "code": "KY"},
            {"name": "Central African Republic", "code": "CF"},
            {"name": "Chad", "code": "TD"},
            {"name": "Chile", "code": "CL"},
            {"name": "China", "code": "CN"},
            {"name": "Christmas Island", "code": "CX"},
            {"name": "Cocos (Keeling) Islands", "code": "CC"},
            {"name": "Colombia", "code": "CO"},
            {"name": "Comoros", "code": "KM"},
            {"name": "Congo", "code": "CG"},
            {"name": "Congo, The Democratic Republic of the", "code": "CD"},
            {"name": "Cook Islands", "code": "CK"},
            {"name": "Costa Rica", "code": "CR"},
            {"name": "Cote D\"Ivoire", "code": "CI"},
            {"name": "Croatia", "code": "HR"},
            {"name": "Cuba", "code": "CU"},
            {"name": "Cyprus", "code": "CY"},
            {"name": "Czech Republic", "code": "CZ"},
            {"name": "Denmark", "code": "DK"},
            {"name": "Djibouti", "code": "DJ"},
            {"name": "Dominica", "code": "DM"},
            {"name": "Dominican Republic", "code": "DO"},
            {"name": "Ecuador", "code": "EC"},
            {"name": "Egypt", "code": "EG"},
            {"name": "El Salvador", "code": "SV"},
            {"name": "Equatorial Guinea", "code": "GQ"},
            {"name": "Eritrea", "code": "ER"},
            {"name": "Estonia", "code": "EE"},
            {"name": "Ethiopia", "code": "ET"},
            {"name": "Falkland Islands (Malvinas)", "code": "FK"},
            {"name": "Faroe Islands", "code": "FO"},
            {"name": "Fiji", "code": "FJ"},
            {"name": "Finland", "code": "FI"},
            {"name": "France", "code": "FR"},
            {"name": "French Guiana", "code": "GF"},
            {"name": "French Polynesia", "code": "PF"},
            {"name": "French Southern Territories", "code": "TF"},
            {"name": "Gabon", "code": "GA"},
            {"name": "Gambia", "code": "GM"},
            {"name": "Georgia", "code": "GE"},
            {"name": "Germany", "code": "DE"},
            {"name": "Ghana", "code": "GH"},
            {"name": "Gibraltar", "code": "GI"},
            {"name": "Greece", "code": "GR"},
            {"name": "Greenland", "code": "GL"},
            {"name": "Grenada", "code": "GD"},
            {"name": "Guadeloupe", "code": "GP"},
            {"name": "Guam", "code": "GU"},
            {"name": "Guatemala", "code": "GT"},
            {"name": "Guernsey", "code": "GG"},
            {"name": "Guinea", "code": "GN"},
            {"name": "Guinea-Bissau", "code": "GW"},
            {"name": "Guyana", "code": "GY"},
            {"name": "Haiti", "code": "HT"},
            {"name": "Heard Island and Mcdonald Islands", "code": "HM"},
            {"name": "Holy See (Vatican City State)", "code": "VA"},
            {"name": "Honduras", "code": "HN"},
            {"name": "Hong Kong", "code": "HK"},
            {"name": "Hungary", "code": "HU"},
            {"name": "Iceland", "code": "IS"},
            {"name": "India", "code": "IN"},
            {"name": "Indonesia", "code": "ID"},
            {"name": "Iran, Islamic Republic Of", "code": "IR"},
            {"name": "Iraq", "code": "IQ"},
            {"name": "Ireland", "code": "IE"},
            {"name": "Isle of Man", "code": "IM"},
            {"name": "Israel", "code": "IL"},
            {"name": "Italy", "code": "IT"},
            {"name": "Jamaica", "code": "JM"},
            {"name": "Japan", "code": "JP"},
            {"name": "Jersey", "code": "JE"},
            {"name": "Jordan", "code": "JO"},
            {"name": "Kazakhstan", "code": "KZ"},
            {"name": "Kenya", "code": "KE"},
            {"name": "Kiribati", "code": "KI"},
            {"name": "Korea, Democratic People\"S Republic of", "code": "KP"},
            {"name": "Korea, Republic of", "code": "KR"},
            {"name": "Kuwait", "code": "KW"},
            {"name": "Kyrgyzstan", "code": "KG"},
            {"name": "Lao People\"S Democratic Republic", "code": "LA"},
            {"name": "Latvia", "code": "LV"},
            {"name": "Lebanon", "code": "LB"},
            {"name": "Lesotho", "code": "LS"},
            {"name": "Liberia", "code": "LR"},
            {"name": "Libyan Arab Jamahiriya", "code": "LY"},
            {"name": "Liechtenstein", "code": "LI"},
            {"name": "Lithuania", "code": "LT"},
            {"name": "Luxembourg", "code": "LU"},
            {"name": "Macao", "code": "MO"},
            {"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"},
            {"name": "Madagascar", "code": "MG"},
            {"name": "Malawi", "code": "MW"},
            {"name": "Malaysia", "code": "MY"},
            {"name": "Maldives", "code": "MV"},
            {"name": "Mali", "code": "ML"},
            {"name": "Malta", "code": "MT"},
            {"name": "Marshall Islands", "code": "MH"},
            {"name": "Martinique", "code": "MQ"},
            {"name": "Mauritania", "code": "MR"},
            {"name": "Mauritius", "code": "MU"},
            {"name": "Mayotte", "code": "YT"},
            {"name": "Mexico", "code": "MX"},
            {"name": "Micronesia, Federated States of", "code": "FM"},
            {"name": "Moldova, Republic of", "code": "MD"},
            {"name": "Monaco", "code": "MC"},
            {"name": "Mongolia", "code": "MN"},
            {"name": "Montserrat", "code": "MS"},
            {"name": "Morocco", "code": "MA"},
            {"name": "Mozambique", "code": "MZ"},
            {"name": "Myanmar", "code": "MM"},
            {"name": "Namibia", "code": "NA"},
            {"name": "Nauru", "code": "NR"},
            {"name": "Nepal", "code": "NP"},
            {"name": "Netherlands", "code": "NL"},
            {"name": "Netherlands Antilles", "code": "AN"},
            {"name": "New Caledonia", "code": "NC"},
            {"name": "New Zealand", "code": "NZ"},
            {"name": "Nicaragua", "code": "NI"},
            {"name": "Niger", "code": "NE"},
            {"name": "Nigeria", "code": "NG"},
            {"name": "Niue", "code": "NU"},
            {"name": "Norfolk Island", "code": "NF"},
            {"name": "Northern Mariana Islands", "code": "MP"},
            {"name": "Norway", "code": "NO"},
            {"name": "Oman", "code": "OM"},
            {"name": "Pakistan", "code": "PK"},
            {"name": "Palau", "code": "PW"},
            {"name": "Palestinian Territory, Occupied", "code": "PS"},
            {"name": "Panama", "code": "PA"},
            {"name": "Papua New Guinea", "code": "PG"},
            {"name": "Paraguay", "code": "PY"},
            {"name": "Peru", "code": "PE"},
            {"name": "Philippines", "code": "PH"},
            {"name": "Pitcairn", "code": "PN"},
            {"name": "Poland", "code": "PL"},
            {"name": "Portugal", "code": "PT"},
            {"name": "Puerto Rico", "code": "PR"},
            {"name": "Qatar", "code": "QA"},
            {"name": "Reunion", "code": "RE"},
            {"name": "Romania", "code": "RO"},
            {"name": "Russian Federation", "code": "RU"},
            {"name": "RWANDA", "code": "RW"},
            {"name": "Saint Helena", "code": "SH"},
            {"name": "Saint Kitts and Nevis", "code": "KN"},
            {"name": "Saint Lucia", "code": "LC"},
            {"name": "Saint Pierre and Miquelon", "code": "PM"},
            {"name": "Saint Vincent and the Grenadines", "code": "VC"},
            {"name": "Samoa", "code": "WS"},
            {"name": "San Marino", "code": "SM"},
            {"name": "Sao Tome and Principe", "code": "ST"},
            {"name": "Saudi Arabia", "code": "SA"},
            {"name": "Senegal", "code": "SN"},
            {"name": "Serbia and Montenegro", "code": "CS"},
            {"name": "Seychelles", "code": "SC"},
            {"name": "Sierra Leone", "code": "SL"},
            {"name": "Singapore", "code": "SG"},
            {"name": "Slovakia", "code": "SK"},
            {"name": "Slovenia", "code": "SI"},
            {"name": "Solomon Islands", "code": "SB"},
            {"name": "Somalia", "code": "SO"},
            {"name": "South Africa", "code": "ZA"},
            {"name": "South Georgia and the South Sandwich Islands", "code": "GS"},
            {"name": "Spain", "code": "ES"},
            {"name": "Sri Lanka", "code": "LK"},
            {"name": "Sudan", "code": "SD"},
            {"name": "Suriname", "code": "SR"},
            {"name": "Svalbard and Jan Mayen", "code": "SJ"},
            {"name": "Swaziland", "code": "SZ"},
            {"name": "Sweden", "code": "SE"},
            {"name": "Switzerland", "code": "CH"},
            {"name": "Syrian Arab Republic", "code": "SY"},
            {"name": "Taiwan, Province of China", "code": "TW"},
            {"name": "Tajikistan", "code": "TJ"},
            {"name": "Tanzania, United Republic of", "code": "TZ"},
            {"name": "Thailand", "code": "TH"},
            {"name": "Timor-Leste", "code": "TL"},
            {"name": "Togo", "code": "TG"},
            {"name": "Tokelau", "code": "TK"},
            {"name": "Tonga", "code": "TO"},
            {"name": "Trinidad and Tobago", "code": "TT"},
            {"name": "Tunisia", "code": "TN"},
            {"name": "Turkey", "code": "TR"},
            {"name": "Turkmenistan", "code": "TM"},
            {"name": "Turks and Caicos Islands", "code": "TC"},
            {"name": "Tuvalu", "code": "TV"},
            {"name": "Uganda", "code": "UG"},
            {"name": "Ukraine", "code": "UA"},
            {"name": "United Arab Emirates", "code": "AE"},
            {"name": "United Kingdom", "code": "GB"},
            {"name": "United States", "code": "US"},
            {"name": "United States Minor Outlying Islands", "code": "UM"},
            {"name": "Uruguay", "code": "UY"},
            {"name": "Uzbekistan", "code": "UZ"},
            {"name": "Vanuatu", "code": "VU"},
            {"name": "Venezuela", "code": "VE"},
            {"name": "Viet Nam", "code": "VN"},
            {"name": "Virgin Islands, British", "code": "VG"},
            {"name": "Virgin Islands, U.S.", "code": "VI"},
            {"name": "Wallis and Futuna", "code": "WF"},
            {"name": "Western Sahara", "code": "EH"},
            {"name": "Yemen", "code": "YE"},
            {"name": "Zambia", "code": "ZM"},
            {"name": "Zimbabwe", "code": "ZW"}
        ]

    })

    // Alerts Configuration 
    .controller('monitorConfiguration',
        function monitorConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, helpers ){
            managementConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, "monitorConfiguration", helpers );
    })
    
    // Alerts 
    .controller('monitor',
        function monitor( $scope, $rootScope, apiBackOffice, $uibModal, $state ){
            
        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
       
        $scope.alerts       = [];
        $scope.alertsNumber = 0;

        $rootScope.isProcessing     = false;
        $scope.selectedAlert = ""; 
        $scope.modelOptions  = {
            debounce: {
                default: 500,
                blur: 250//,
                // updateOn: ''
            },
            getterSetter: true
        };

        /*------------------------------------------------------------------
        [  Init All Configuration  ]
        */
        $scope.loadInitialData = function(){
            // Add processing: ajax... 
            $rootScope.isProcessing     = true;

            apiBackOffice.GetAlerts().then(function (data) {

                // Stop processing: ajax... 
                $rootScope.isProcessing     = false;

                // Assign alerts
                angular.forEach(data, function(v){
                    $scope.alerts.push(v);
                    $scope.alertsNumber += 1;    
                })
                
            })
        }

        $scope.loadInitialData();


        /**
         * addNewAlert
         */
        $scope.addNewAlert = function () {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modals/configuration/configAddNewAlert.html',
                controller: ModalCtrl,
                resolve: {
                    scope: function () {
                        return $scope;
                    }
                }
            });

            function ModalCtrl($scope, $uibModalInstance, scope) {
                
                /*------------------------------------------------------------------
                [  Get All Configuration Types  ]
                */
                $scope.getAllConfigurationTypes = function(){
                    // Add processing: ajax... 
                    $rootScope.isProcessing     = true;

                    // Api call 
                    apiBackOffice.GetAllConfigurationTypes().then(function (data) {

                        $scope.config = data;

                        // Stop processing: ajax... 
                        $rootScope.isProcessing     = false;
                        
                    },function(){
                        // Stop processing: ajax... 
                        $rootScope.isProcessing     = false;
                    })
                }

                $scope.getAllConfigurationTypes();


                $scope.ok = function () {
                    // Check for empty option
                    if($scope.config.Value.length == 0)
                        return false;
                    else{
                        $uibModalInstance.close();
                        $state.go('management.alertConfiguration', {"id": $scope.config.Value });
                    }
                }
                $scope.cancel = function () {
                    $uibModalInstance.close();
                }
            }
        }

    })

    // Account Types Configuration
    .controller('accountTypesConfiguration',
        function accountTypesConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, helpers ){
            managementConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, "accountTypesConfiguration", helpers );
    })

    // Account Types
    .controller('accountTypes',
        function accountTypes( $scope, $rootScope, apiBackOffice, $uibModal, $state ){
            
        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
       
        $scope.accountTypes       = [];
        $scope.accountTypesNumber = 0;

        $rootScope.isProcessing     = false;
        $scope.selectedAccountType  = ""; 
        $scope.modelOptions  = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        /*------------------------------------------------------------------
        [  Init All Configuration  ]
        */
        $scope.loadInitialData = function(){
            // Add processing: ajax... 
            $rootScope.isProcessing     = true;

            apiBackOffice.GetAccountTypes().then(function (data) {

                // Stop processing: ajax... 
                $rootScope.isProcessing     = false;

                // Assign alerts
                angular.forEach(data, function(v){
                    $scope.accountTypes.push(v);
                    $scope.accountTypesNumber += 1;    
                })
                
            })
        }

        // Call initial load
        $scope.loadInitialData();

    })

    // Directives
    .directive('myRepeatDirective', function() {
        return {
            link : function(scope, element, attrs) {
            }
        }
    })
    .filter('capitalize', function() {
        return function(input) {
          return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    })

    /**
     * handleAction - custom usage for logging
     * @param  {Object} apiLogging - logging factory
     */
    .directive('handleAction', function($compile) {
        return {
            restrict: "A",

            link: function(scope, element, attrs) {
                console.log(scope);
                element.bind('click', function() {
                    
                    // Log 
                    // apiLogging.logUsage(element,$location.absUrl(),attrs);
                });
            }
        }
    });
