// (function( ng, inspinia ){
    angular
        .module('dashboardMobile',['gridstack-angular','agGrid'])

        .controller('dashboardMobile',
            function dashboard( $scope, 
                                $rootScope, 
                                $http, 
                                $uibModal, 
                                $timeout, 
                                $log,
                                apiBackOffice, 
                                $state, 
                                $stateParams,
                                widgetSvc, 
                                WidgetsHubEvents, 
                                $window, 
                                $filter,
                                helpers,
                                apiAlerts,
                                toaster ){

                var self = $scope;

                /*------------------------------------------------------------------
                [  MODELS  ]
                */
                $scope.container = {};
                $scope.widget    = {};
                $scope.proxy     = {};

                $scope.dashboards= {};
                $scope.dashboardsLight = {};

                $scope.mapWidgetsObj = {
                    "<wdatatable></wdatatable>" : "wdatatableData",
                    "<aggregated></aggregated>" : "aggregatedData",
                    "<barchart></barchart>"     : "barchartData",
                    "<hbarchart></hbarchart>"   : "hbarchartData",
                    "<linechart></linechart>"   : "linechartData",
                };
                // Show Order Button
                $scope.showOrderBtn = true;

                $scope.subscribedDashboards = [];

                /*------------------------------------------------------------------
                [  DASHBOARD WIDGETS LAYOUT METHODS  ]
                */
                
                $scope.panelOrderWidgets = {};

                var width = 0;
                var height = 0;
                var cols = 12;
                var rows = 12;
                var vmargin = 20;
                var lastHeight = 0;
                var gridObj = 0;

                $scope.options = {
                    // cellHeight: 'auto',
                    verticalMargin: 10,

                    /* Additional properties 
                    acceptWidgets: '.grid-stack-item',
                    float: true,
                    cellHeight: 'auto',
                    cellHeightUnit: 'px',
                    resizable: false
                    width: cols,
                    height: rows,
                    float: false,
                    cellHeight: 'auto',
                    resizable: { handles: 'e, se, s, sw, w' },
                    */ 
                    // auto: false,
                    // float: true,
                    float: false,
                    animate: true,
                    disableResize: true,
                    disableDrag: false,
                    
                    cellHeightUnit: 'px',
                    handleClass: 'grid-stack-item-content',
                    removable: '',
                    removeTimeout: 100,
                    verticalMargin: vmargin,
                    acceptWidgets: '.grid-stack-item',
                    
                    resizable: { autoHide: true, handles: 'se' },
                    alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
                };

                // Remove Widget with Modal Prompt
                $scope.removeWidget = function(w) {

                    var modalInstance = $uibModal.open({
                        templateUrl: 'views/modals/dashboard/removeWidget.html',
                        size: (!!typeof(size)?'md':size),
                        controller: DeleteWidgetModalCtrl,
                        resolve:{
                            itemNumber: function () {
                                return w.Id;
                            },
                            scope: function () {
                                return $scope;
                            }
                        }
                    });
                    /**
                     *
                     * @param $scope - it's the scope of the modal
                     * @param $modalInstance - reference to modal
                     * @param scope - reference to main scope
                     * @param itemNumber - item number of the row that have to be removed
                     * @constructor
                     */
                    function DeleteWidgetModalCtrl($scope, $uibModalInstance, scope, itemNumber) {
                        $scope.ok = function () {

                            // Add processing: ajax... 
                            $rootScope.isProcessing     = true;
                            
                            // Api call 
                            apiBackOffice.RemoveWidget(itemNumber).then(function (data) {

                                $scope.config = data;
                                
                                // Remove it from widgets global model
                                delete scope.widgets[itemNumber];

                                // Remove it from display model
                                delete scope.dashboards[scope.activeDashboard].widgets[itemNumber];

                                // Stop processing: ajax... 
                                $rootScope.isProcessing     = false;

                                // Close modal
                                $uibModalInstance.close();
                                
                            },function(){
                                // Stop processing: ajax... 
                                $rootScope.isProcessing     = false;
                                $uibModalInstance.close();
                            })
                        }
                        $scope.cancel = function () {
                            $uibModalInstance.close();
                        }
                    }
                }

                $scope.onChange = function(event, items) {
                    // $log.log("onChange event: "+event+" items:"+items);
                };

                $scope.onDragStart = function(event, ui) {
                    // $log.log("onDragStart event: "+event+" ui:"+ui);
                    // console.log("onDragStart event: "+event," ui:"+ui);
                };

                $scope.onDragStop = function(event, ui) {
                    // $log.log("onDragStop event: "+event+" ui:"+ui);
                    $timeout(function() {
                        $window.dispatchEvent(new Event("resize"));
                    }, 100);
                };

                $scope.onResizeStart = function(event, ui) {
                    // $log.log("onResizeStart event: "+event+" ui:"+ui);
                };

                $scope.onResizeStop = function(event, ui) {
                    $timeout(function() {
                        $window.dispatchEvent(new Event("resize"));
                    }, 100);
                    // $log.log("onResizeStop event: "+event+" ui:"+ui);
                };

                $scope.onItemAdded = function(item) {
                    // $log.log("onItemAdded item: "+item);
                };

                $scope.onItemRemoved = function(item) {
                    // $log.log("onItemRemoved item: "+item);
                };

                /**
                 * serialize - Save Dashboard Layout
                 */
                $scope.serialize = function(dashboardId){
                    // console.log(dashboardId);
                    var payload = {},
                        res = _.map($('.grid-stack[data-panel="'+$scope.activeDashboard+'"] .grid-stack-item:visible'), function (el) {
                        
                            el = $(el);

                            // current widget
                            var node = el.data('_gridstack_node');
                            
                            if(typeof $scope.widgets[el.attr('widgetID')] !== 'undefined')
                            {
                                var widget = $scope.widgets[el.attr('widgetID')];

                                // Apply new changes in model
                                widget['view']['width']  = node.width;
                                widget['view']['height'] = node.height;
                                widget['view']['x']      = node.x;
                                widget['view']['y']      = node.y;
                            

                                // Prepare payload
                                payload[el.attr('widgetID')] = {
                                    "Id"        : widget['Id'],
                                    "Title"     : widget['Title'],
                                    "IsEnabled" : widget['IsEnabled'],
                                    "view"      : {
                                        x: node.x,
                                        y: node.y,
                                        width: node.width,
                                        height: node.height,
                                        url: widget['view']['url'],
                                        autopos: false,
                                        dashboardId: widget['view']['dashboardId'],
                                        minWidth: widget['view']['minWidth'],
                                        minHeight: widget['view']['minHeight']
                                    }
                                }

                            }

                            return {
                                id: el.attr('widgetID'),
                                x: node.x,
                                y: node.y,
                                width: node.width,
                                height: node.height,
                                // autopos: node.autoPosition,
                                autopos: false,
                                minWidth: node.minWidth,
                                minHeight: node.minHeight
                            };
                    });
                    
                    // Add processing: ajax... 
                    $rootScope.isProcessing     = true;

                    apiBackOffice.UpdateWidgetsLayout(dashboardId, payload).then(
                        function(data){
                            // Stop processing: ajax... 
                            $rootScope.isProcessing     = false;
                            payload = null;
                            $scope.destroyDraggable('.connectPanels');
                        },
                        function(){
                            // Stop processing: ajax... 
                            $rootScope.isProcessing     = false;   
                            payload = null;
                            $scope.destroyDraggable('.connectPanels');
                        }
                    )
                    // console.log(JSON.stringify(res));            
                };

                /**
                 * toggleDrag - default | true : noMove,noResize
                 * @type {Boolean}
                 */
                $scope.toggleDrag = true;

                // Toggle Drag|Resize actions per dashboards
                $scope.destroyDraggable = function( panel ){
                    $timeout(function() {
                        var panel = $('.grid-stack[data-panel="'+$scope.activeDashboard+'"] .grid-stack-item:visible');

                        $(panel).each(function(index,el) {
                            el = $(el);
                            var node = el.data('_gridstack_node');
                            // console.log(node);
                            if (typeof node == 'undefined' || node === null || typeof $.ui === 'undefined') {
                                return;
                            }

                            if ($scope.dashboards[$scope.activeDashboard]['toggleDrag']) {

                                el.draggable('disable');
                                el.resizable('disable');
                                el.removeClass('ui-draggable-handle');

                            } else {

                                el.draggable('enable');
                                el.resizable('enable');
                                el.addClass('ui-draggable-handle');
                            }
                            
                        });

                        // Toggle classes
                        if ($scope.dashboards[$scope.activeDashboard]['toggleDrag']) {

                            $scope.toggleTabsDisabled($scope.activeDashboard, false);
                            $( panel ).closest(".panel-body").removeClass("highlight");
                            $scope.showOrderBtn = true;

                        } else {
                            $scope.toggleTabsDisabled($scope.activeDashboard, true);
                            $scope.showOrderBtn = false;
                            $( panel).closest(".panel-body").addClass("highlight");
                        }
                        

                        // Toggle drag state
                        $scope.dashboards[$scope.activeDashboard]['toggleDrag'] = !$scope.dashboards[$scope.activeDashboard]['toggleDrag'];
                    },100);
                };

                /*------------------------------------------------------------------
                [  DESTROY & LEAVE THE PAGE  ]
                */
                $scope.$on('$destroy', function () {
                    // console.log($scope);
                    // console.log($scope.$$listeners);
                    
                    if( $scope.proxy ){
                        // unsubscribe    
                        $scope.proxy.proxies["riskwidgetshub"].server.unSubscribeForWidgetsData();
                        // stop hub
                        widgetSvc.stopHub($scope.proxy);
                    }
                });

                /*------------------------------------------------------------------
                [  TABS METHODS  ]
                */
                $scope.toggleTabsInactive = function(current, state) {
                    _.map($scope.dashboards,function(el,k){
                        el['active'] = (k == current ? true : state);
                    })
                    // console.log($scope.dashboards);  
                }

                $scope.toggleTabsDisabled = function(current, state) {
                    _.map($scope.dashboards,function(el,k){
                        el['disabled'] = (k == current ? false : state);
                    })
                    // console.log($scope.dashboards);
                }

                /*------------------------------------------------------------------
                [  DASHBOARD MODALS METHODS ]
                */
                /**
                 * addNewWidget - Add new Widget Modal
                 * @param  {Number} id   
                 * @param  {String} size 
                 */
                $scope.addNewWidget = function (id,size) {
                    
                    var modalInstance = $uibModal.open({
                        templateUrl: 'views/modals/dashboard/addWidget.html',
                        // size: (!!typeof(size)?size:'lg'),
                        size: 'lg',
                        controller: ModalCtrl,
                        resolve: {
                            scope: function () {
                                return $scope;
                            }
                        }
                    });

                    function ModalCtrl($scope, $uibModalInstance, scope) {
                        
                        /*------------------------------------------------------------------
                        [  Edit Dashboard  ]
                        */
                        $scope.isEdit = (typeof id == 'undefined'? false : true);

                        $scope.config = {};

                        /*------------------------------------------------------------------
                        [  Get All Configuration Types  ]
                        */
                        $scope.getAvailableWidgets = function(id){

                            if(!helpers.exist(id,false))
                                return false;

                            // Add processing: ajax... 
                            $rootScope.isProcessing     = true;
                            
                                // Get Available Widgets 
                                apiBackOffice.GetAvailableWidgets(id).then(function (data) {

                                    $scope.config.widgets   = data;

                                    // Set default state
                                    $scope.config.widgets.Value = data["DropDownValues"][0]['Value'];

                                    // Stop processing: ajax... 
                                    $rootScope.isProcessing     = false;
                                    
                                },function(){
                                    // Stop processing: ajax... 
                                    $rootScope.isProcessing     = false;
                                })
                        }

                        /*------------------------------------------------------------------
                        [  Get All Configuration Types Calculators  ]
                        */
                        $scope.getAvailableCalculators = function(){

                            // Add processing: ajax... 
                            $rootScope.isProcessing     = true;

                            // Get Available calculators 
                            apiBackOffice.GetAvailableCalculators().then(function (data) {
                                $scope.config.calculators     = data;

                                $scope.dashboards       = scope.dashboardsLight;

                                if($scope.isEdit)
                                    $scope.config.DashboardId = id;

                                // Stop processing: ajax... 
                                $rootScope.isProcessing     = false;
                                
                            },function(){
                                // Stop processing: ajax... 
                                $rootScope.isProcessing     = false;
                            })

                        }
                        // Init
                        $scope.getAvailableCalculators();

                        // $scope.getAvailableWidgets();

                        $scope.ok = function () {
                            // Check for empty option
                            if($scope.config.calculators.Value.length == 0)
                                return false;
                            else{
                                $uibModalInstance.close();
                                // $state.go('index.dashboardWidgetConfiguration', {"id": $scope.config.Value, "dashboardId" : $scope.config.DashboardId });
                                $state.go('index.dashboardWidgetConfiguration', {"id": $scope.config.calculators.Value+"-"+$scope.config.widgets.Value, "dashboardId" : $scope.config.DashboardId }); 
                            }
                        }
                        $scope.cancel = function () {
                            $uibModalInstance.close();
                        }
                    }
                }

                /**
                 * addNewDashboard - Add new Dashboard modal
                 * @param  {Number} id   
                 * @param  {String} size 
                 */
                $scope.addNewDashboard = function (id,size) {

                    var modalInstance = $uibModal.open({
                        templateUrl: 'views/modals/dashboard/addDashboard.html',
                        size: (!!typeof(size)?size:'md'),
                        controller: ModalCtrl,
                        resolve: {
                            scope: function () {
                                return $scope;
                            }
                        }
                    });

                    function ModalCtrl($scope, $uibModalInstance, scope) {
                        $scope.container = {
                            Id   : null,
                            Name : null
                        }

                        /*------------------------------------------------------------------
                        [  Edit Dashboard  ]
                        */
                        $scope.isEdit = (typeof id == 'undefined'? false : true);
                        
                        if($scope.isEdit){
                            $rootScope.isProcessing     = true;

                            apiBackOffice.GetDashboardById(id).then(function (data) {
                                // console.log(data);
                                $scope.container = data;
                                $rootScope.isProcessing     = false;
                            })    
                        }  
                        /*------------------------------------------------------------------
                        [  Create Dashboard  ]
                        */
                        $scope.dashboardForm = function(){

                            if ($scope.dashboard_form.$valid) {

                                // Add processing: ajax... 
                                $rootScope.isProcessing     = true;

                                // New
                                if(!$scope.isEdit){

                                    $scope.container.Id = 0;

                                    // Add Dashboard
                                    apiBackOffice.AddDashboard($scope.container).then(function (data) {

                                        // Get Dashboards
                                        apiBackOffice.GetDashboards().then(function(data){

                                            
                                            _.map(data,function(el,i) {
                                                
                                                // there is no widgets
                                                if(typeof scope.dashboards[el.Id] == 'undefined'){

                                                    scope.dashboardsLight[el.Id] = el;

                                                    el['widgets']                = {};
                                                    scope.dashboards[el.Id]      = el;

                                                    // Set new Dash tab active
                                                    scope.toggleTabsInactive(el.Id,false);

                                                    scope.destroyDraggable('.connectPanels');

                                                    // close modal
                                                    $uibModalInstance.close();
                                                }
                                                return el;
                                            })
                                            
                                            // Stop processing: ajax... 
                                            $rootScope.isProcessing     = false;


                                        })

                                        
                                    },function(){
                                        // Stop processing: ajax... 
                                        $rootScope.isProcessing     = false;
                                    })
                                }
                                // Edit 
                                else{
                                    // Update Dashboard
                                    apiBackOffice.UpdateDashboard($scope.container).then(function (data) {

                                        // Stop processing: ajax... 
                                        $rootScope.isProcessing     = false;
                                        
                                        // Set edited to tabs
                                        _.map(scope.dashboards,function(el,i) {
                                            if(el.Id == id){
                                                scope.dashboards[id].Name = $scope.container.Name;
                                                scope.dashboardsLight[id].Name = $scope.container.Name;
                                            }
                                            return el;
                                        })
                                        $uibModalInstance.close();
                                    },function(){
                                        // Stop processing: ajax... 
                                        $rootScope.isProcessing     = false;
                                        // $uibModalInstance.close();
                                    })
                                }
                            }
                        }

                        
                        $scope.cancel = function () {
                            // scope.dashboards[0].active = true;
                            $uibModalInstance.close();
                        }
                    }
                };

                /**
                 * removeDashboard - Remove dashboard and all it widgets (from back-end)
                 * @param  {Number} id   
                 * @param  {String} size 
                 */
                $scope.removeDashboard = function (id,size) {

                    var modalInstance = $uibModal.open({
                        templateUrl: 'views/modals/dashboard/removeDashboard.html',
                        size: (!!typeof(size)?size:'md'),
                        controller: ModalCtrl,
                        resolve: {
                            scope: function () {
                                return $scope;
                            }
                        }
                    });

                    function ModalCtrl($scope, $uibModalInstance, scope) {
                        
                        

                        // Can't delete default dashboard
                        if(id == 0){
                            $scope.isDefault = true;
                        }else{
                            $scope.isDefault = false;
                        }

                        $scope.ok = function () {

                            if(!$scope.isDefault){
                        
                                // Add processing: ajax... 
                                $rootScope.isProcessing     = true;

                                // Remove Dashboard 
                                apiBackOffice.RemoveDashboard(id).then(function (data) {

                                    // Remove Dashboard in real time
                                    angular.forEach(scope.dashboards,function(v,k){
                                        if(v.Id == id){
                                            delete scope.dashboards[k];
                                            delete scope.dashboardsLight[k];

                                            // Set first Dash tab active
                                            scope.toggleTabsInactive(0,false);

                                            // Stop processing: ajax... 
                                            $rootScope.isProcessing     = false;
                                            $uibModalInstance.close();
                                        }    
                                    })

                                    
                                    
                                },function(){
                                    // Stop processing: ajax... 
                                    $rootScope.isProcessing     = false;
                                    $uibModalInstance.close();
                                })

                            }
                        }
                        $scope.cancel = function () {
                            $uibModalInstance.close();
                        }
                    }
                };


                /*------------------------------------------------------------------
                [  DASHBOARD LOAD & SUBSCRIBTION METHODS  ]
                */

                $scope.loadAllWidgets = function(){

                    // Add processing: ajax... 
                    $rootScope.isProcessing     = true;

                    // Get Dashboards
                    apiBackOffice.GetDashboards().then(function(data){
                        

                        // Load tabs when data is ready
                        $scope.loadTabs = false;

                        if($stateParams.id && helpers.exist(data[$stateParams.id]))
                        {
                            // Specific Dashboard to load
                            $scope.activeDashboard = Number($stateParams.id);
                        }else{
                            // Default Dashboard to load
                            $scope.activeDashboard = 0;
                        }
                        

                        // Add some settings to widgets shell in dashboard
                        angular.forEach(data,function(v,k){

                            v["widgets"]    = {};
                            v["toggleDrag"] = true;
                            v["disabled"]   = true;
                            v["active"]     = false;
                        })

                        /**
                         * dashboards - Assign Dashboards to model
                         * @type {Object}
                         */
                        $scope.dashboards = data;

                        // Create light model
                        angular.copy(data,$scope.dashboardsLight);

                        // Get Widgets
                        apiBackOffice.GetWidgets().then( function(data){ 
                            
                            // Charts initial states
                            angular.forEach(data,function(v,k){
                                // console.log(v);
                                // Default values
                                v.bg = "";
                                // Is mobile
                                v.mobile = $rootScope.ENV.mobile;
                                // if($scope.activeDashboard == 47)
                                    // v.view.autopos = true;
                                    // v.view.x = '100%';
                                if(v.mobile){
                                    v.view.autopos = false;
                                    v.view.width = 12;
                                    // v.view.height = 0;
                                    v.view.x = 12;
                                    v.view.y = 0;
                                }
                                else
                                    v.view.autopos = false;

                                if(v.view.url == "<wdatatable></wdatatable>")
                                    v.data      = {
                                        table  : [],
                                        loading: false
                                    };                           
                                if(v.view.url == "<linechart></linechart>")
                                    v.data = {
                                        chart: {
                                            zoomType: 'x',
                                            // Responsive height
                                            // height: (9/16 * 100) + '%'
                                            marginBottom: 60,
                                            panning: true,
                                            panKey: 'shift'
                                        },
                                        series:[{
                                            name:"",
                                            data:[]
                                        }],
                                        // data: {
                                        //     enablePolling: true
                                        // },
                                        yAxis: {
                                            title: {
                                                text: null
                                            },
                                            labels:{
                                                enabled: false
                                            }
                                        },
                                        plotOptions: {
                                            area: {
                                                marker: {
                                                    radius: 1
                                                },
                                                lineWidth: 1,
                                                states: {
                                                    hover: {
                                                        lineWidth: 1
                                                    }
                                                },
                                                threshold: null
                                            }
                                        },
                                        xAxis: {
                                            // tickInterval: 5,
                                            tickInterval: 24 * 3600 * 1000,
                                            type: 'datetime'//,
                                            // tickWidth: 50
                                        },
                                        tooltip: {
                                            formatter: function () {
                                                time = new Date(this.x);
                                                return 'The value was <b>' + $filter('currency')(this.y,'') + '</b> in ' + time.getHours()+":"+time.getMinutes()
                                            }
                                        },
                                        title: {
                                            text: ''
                                        },
                                        
                                        func: function(chart) {
                                            // console.log(chart);
                                            this.charting = chart;
                                            return chart;
                                        },
                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    // maxWidth: 500,
                                                    // minHeight: 250
                                                    // maxHeight : 300 
                                                },
                                                chartOptions: {
                                                    colors: ["#1ab394"],
                                                    chart: {
                                                        height: 250
                                                    }
                                                }
                                            }]
                                        },
                                        loading : false
                                    }
                                if(v.view.url == "<hbarchart></hbarchart>")
                                    v.data = {
                                        chart: {
                                            type: 'bar',
                                            zoomType: 'xy'
                                        },
                                        series:[{
                                            name:"",
                                            data:[]
                                        }],
                                        yAxis: {
                                            title: {
                                                text: null
                                            },
                                            labels:{
                                                enabled: false
                                            }
                                        },
                                        xAxis: [{
                                            type: 'category',
                                            categories: [''],
                                            title: {
                                                text: null
                                            },
                                            labels: {
                                                step: 1
                                            }
                                        }],
                                        plotOptions:{
                                            series: {
                                                stacking: 'normal',
                                                // dataLabels:{
                                                //     enabled : true,
                                                //     color: '#000',
                                                //     align: 'left',

                                                //     x: 0,
                                                //     style: {fontWeight:'bolder'},
                                                //     formatter: function() {return this.series.name},
                                                //     inside : false
                                                // },
                                                // floating: 1,
                                                // pointPadding: 0,
                                                // groupPadding: 0,
                                                // borderWidth: 1
                                            }
                                        },
                                        tooltip: {
                                            formatter: function () {
                                                return 'The value is <b>' + $filter('currency')(this.y,'') + '</b> for ' + this.series.name
                                            }
                                        },
                                        title: {
                                            text: ''
                                        },
                                        loading : false
                                    }
                                if(v.view.url == "<aggregated></aggregated>")
                                    v.data = {
                                        value  : "0.00",
                                        loading: false
                                    }
                                if(v.view.url == "<barchart></barchart>")
                                    v.data = {
                                        "chart": {
                                            "height": 250,
                                            "type": "column",
                                            zoomType: 'xy',
                                            events: {
                                                load : function(){
                                                    // console.log("getted");
                                                    // console.log(this);
                                                    // // $scope.widgets[v.Id].inited
                                                    // v.inited = this;
                                                    // console.log(v);
                                                }
                                            }
                                        },
                                        // "boost":{
                                        //     useGPUTranslations: true
                                        // },
                                        "plotOptions": {
                                            "series": {
                                                "stacking": ""
                                            }
                                        },
                                        "series": [{
                                            name:"",
                                            data:[]
                                        }],
                                        "xAxis": {
                                            categories: [''],
                                            title: {
                                                text: null
                                            },
                                            type: 'datetime'//,
                                            // tickInterval: 15
                                        },
                                        "yAxis": {
                                            title: {
                                                text: null
                                            },
                                            labels:{
                                                enabled: false
                                            }
                                        },
                                        tooltip: {
                                            formatter: function () {
                                                return 'The value was <b>' + $filter('currency')(this.y,'') + '</b> in ' + this.series.name
                                            }
                                        },
                                        "title": {
                                            "text": ""
                                        },
                                        plotOptions:{
                                            series: {
                                                pointPadding: 0,
                                                groupPadding: 0,
                                                borderWidth: 0
                                            }
                                        },
                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    // maxWidth: 500
                                                },
                                                chartOptions: {
                                                    chart: {
                                                        height: 250
                                                    }
                                                }
                                            }]
                                        },
                                        loading: false
                                    }
                                

                                // Assign every widget to it's own dashboard
                                if($scope.dashboards[v.view.dashboardId]){
                                    $scope.dashboards[v.view.dashboardId]['widgets'][v.Id] = {};
                                    $scope.dashboards[v.view.dashboardId]['widgets'][v.Id] = v;
                                }

                            })
                            
                            // Assign widgets
                            $scope.widgets = data;

                            // console.log(_.filter($scope.widgets,function(x){
                            //     return x.view.dashboardId == $scope.activeDashboard;
                            // }));

                            // WidgetsHubEvents.off();
                            WidgetsHubEvents.init($scope);

                            // Stop processing: ajax... 
                            $rootScope.isProcessing     = false;

                            // console.log($rootScope.$$listeners);
                            // console.log($scope.$$listeners);

                            // widgetSvc.initialize($scope.$parent.userData, 'riskWidgetsHub').then(function (proxy) {
                            widgetSvc.initialize($scope,$scope.widgets,'riskWidgetsHub').then(function (proxy) {

                                $scope.loadTabs = true;

                                // Get proxy object
                                $scope.proxy = proxy;

                                // Subscribe Default
                                $scope.subscribeMe($scope.activeDashboard);

                                // Set Draggable Panel
                                // $scope.destroyDraggable('.connectPanels');
                            }); 

                        }, 

                        // On Error get Widgets
                        function(){
                            
                            apiAlerts.addAlert({
                                "type"           : 'zE',
                                "title"          : "Error",
                                "timeout"        : 6000, 
                                "body"           : "bind-hub-msg",
                                "bodyOutputType" : "directive",
                                "directiveData"  : { 'data': 'Error occurred when getting page widgets. Please refresh page' },
                                "toasterId"      : "quick-hub-messages",
                                "clickHandler"   : function(){
                                    return true;
                                }
                            });

                            // Stop processing: ajax...  
                            $rootScope.isProcessing     = false;
                        })
                    }, function(){

                        // On Error get Dashboards
                        apiAlerts.addAlert({
                            "type"           : 'zE',
                            "title"          : "Error",
                            "timeout"        : 6000, 
                            "body"           : "bind-hub-msg",
                            "bodyOutputType" : "directive",
                            "directiveData"  : { 'data': 'Error occurred when getting page dashboards. Please refresh page' },
                            "toasterId"      : "quick-hub-messages",
                            "clickHandler"   : function(){
                                return true;
                            }
                        });

                        // Stop processing: ajax... 
                        $rootScope.isProcessing     = false;
                    })  

                    // $scope.numberValueParser = function(params) {
                    //     // return Number(params.newValue.toFixed(2));
                    //     return Math.floor(params).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    // }

                }

                /**
                 * subscribeForDashboard - get initial data for selected dashboard
                 *                         and subscribe it
                 * @param  {[type]} dashboardId [description]
                 * @return {[type]}             [description]
                 */
                $scope.subscribeForDashboard = function(dashboardId) {

                        dashboardId = helpers.exist(dashboardId);

                        // Initial Data
                        var deferred = $scope.proxy.proxies["riskwidgetshub"].server.getWidgetsData(dashboardId);
                        
                        // Dummy data 
                        // var deferred = $http.get('../../../json-kufte1.json');
                        
                        deferred.then(function (data) {
                                
                            // Initial Data to widgets
                            angular.forEach(data,function(v,k){
                                $scope.triggerWidgetChange(v);
                            })

                            // Subscribe For Dashboard
                            $timeout(function() {
                                $scope.proxy.proxies["riskwidgetshub"].server.subscribeForWidgetsData(dashboardId);
                            },1000)
                            
                        }, function(){
                            apiAlerts.addAlert({
                                "type"           : 'zE',
                                "title"          : "Error",
                                "body"           : "bind-hub-msg",
                                "bodyOutputType" : "directive",
                                "directiveData"  : { 'data': 'Server error occured. Please refresh the page.' },
                                "toasterId"      : "quick-hub-messages",
                                "clickHandler"   : function(){
                                    return true;
                                }
                            });                                    
                        })
                }

                /**
                 * subscribeMe - call subscribeForDashboard method and register current
                 *               dashboard as subscribed
                 * @param  {String|Number} dashboardId
                 */
                $scope.subscribeMe = function(dashboardId) {
                    
                    $scope.activeDashboard = dashboardId;
                    if($scope.subscribedDashboards.indexOf(dashboardId) == -1){

                        // add subscribed dash to watched list
                        $scope.subscribedDashboards.push(dashboardId);

                        // subscribe current dashboard
                        $scope.subscribeForDashboard(dashboardId);

                        // toggle drag mode
                        $scope.destroyDraggable('.connectPanels'); 
                        // all others tabs is active
                        $scope.toggleTabsInactive(dashboardId,false);
                        // all others tabs is enabled
                        $scope.toggleTabsDisabled(dashboardId,false);
                    }
                }

                /**
                 * triggerWidgetChange 
                 * @param  {Object} change 
                 */
                $scope.triggerWidgetChange = function(change) {

                    // get widgets
                    var widgets = $scope.widgets;

                    // trigger change event
                    if(typeof(widgets[change.WidgetId]) !== 'undefined'){
                        // if(
                        //     $scope.mapWidgetsObj[widgets[change.WidgetId].view.url] != "wdatatableData" //&&
                        //     // $scope.mapWidgetsObj[widgets[change.WidgetId].view.url] != "linechartData" 

                        // )
                            $scope.$broadcast($scope.mapWidgetsObj[widgets[change.WidgetId].view.url], change);
                    }

                }

                /**
                 * getWidgetSnapshot
                 * @param  {Number} id
                 */
                $scope.getWidgetSnapshot = function(id) {

                    apiBackOffice.GetWidgetSnapshot(id).then(
                        function(data){
                            // TODO: Some handler
                        },
                        function(){
                            // On error
                        }
                    )                    

                }

                /*------------------------------------------------------------------
                [  INIT  ]
                */
                var chartsData = {};
                /**
                 * DOM Ready
                 */
                angular.element(function(){ 
                    
                    $scope.loadAllWidgets();

                });

                /**
                 * mapChartData - create states -> ADD | UPDATE | DELETE
                 * @param  {Object} tickData - all tick info
                 */
                function mapChartData(tickData) {

                    var widgetID = tickData.widgetID,
                        value      = tickData.value,
                        type       = tickData.type,
                        tickID     = tickData.id,
                        startIndex = tickData.key;// || false;

                    if(typeof(chartsData[widgetID]) != 'undefined'){

                        // Add
                        if(!type){

                            chartsData[widgetID]['total'] = chartsData[widgetID]['total'] + 1;

                            state = {
                                "value" : value,
                                "index" : chartsData[widgetID]['total']
                            };   
                            chartsData[widgetID][tickID] = state;
                                                    
                        // Update
                        }else if(type == 1){
                            if(typeof chartsData[widgetID][tickID] !== 'undefined')
                                chartsData[widgetID][tickID]["value"] = value;
                            else{
                                chartsData[widgetID]['total'] = chartsData[widgetID]['total'] + 1;

                                state = {
                                    "value" : value,
                                    "index" : chartsData[widgetID]['total']
                                };   
                                chartsData[widgetID][tickID] = state;
                            }
                        // Delete
                        }else{
                            delete chartsData[widgetID][tickID];
                            if( chartsData[widgetID]['total'] )
                                chartsData[widgetID]['total'] = chartsData[widgetID]['total'] - 1;
                            else 
                                chartsData[widgetID]['total'] = 0;
                        }
                    }else{

                        chartsData[widgetID]          = {};
                        chartsData[widgetID][tickID]  = {};
                        chartsData[widgetID]['total'] = startIndex;

                        // Update
                        state = {
                            "value" : value,
                            "index" : startIndex
                        };   
                        chartsData[widgetID][tickID]  = state;
                            
                    }
                    

                    // return chartsData;
                    return chartsData[widgetID][tickID];
                }

                /*------------------------------------------------------------------
                [  LISTENERS  ]
                */

                /**
                 * Listener for horizontal barchart
                 */
                $scope.$on("hbarchartData", function (event, data, widgets) {
                    var widgets = $scope.widgets;
                    if(typeof(widgets[data.WidgetId]) !== 'undefined'){
                        $scope.$evalAsync(function () {

                            var inited = false;
                            if(typeof(widgets[data.WidgetId].inited) !== 'undefined' )
                                inited = true;

                            if(inited){
                                var charts = widgets[data.WidgetId].inited.getChartObj();
                            }

                            var willShow = {};
                                update   = {},
                                minutes  = 0,
                                newData  = [],

                            
                            angular.forEach(data.DataStateChanges,function(v,k){

                                // Get Total
                                if(v.DataId == $rootScope.widgetTotal){
                                    
                                    // Add Total
                                    // widgets[data.WidgetId].data['title'].text = (v.Data ? $filter('currency')(v.Data,'') : "$0.00");
                                    return false;   
                                }


                                // Check for Name & Value
                                if(v.Data && typeof(v.Data.Name) === 'undefined')
                                    return false;
                                if(v.Data == null)
                                    return false;
                                if(v.Data && typeof(v.Data.Value) === 'undefined' || typeof(v.Data.Value) == null)
                                    return false;

                                value = v.Data.Value || 0;
                                name  = v.Data.Name  || "";

                                // Assemble name-value
                                valueChart = [name,Number(value.toFixed(2))];

                                // Assemble object for collection
                                update = {
                                    "widgetID" : data.WidgetId,
                                    "value"    : valueChart,
                                    "type"     : v.ModificationType,
                                    "id"       : v.DataId,
                                    "key"      : k
                                }

                                // Put Object in collection
                                mapChartData(update);
                                if(widgets[data.WidgetId].data['xAxis'][0]['categories'].indexOf(name) == -1)
                                    widgets[data.WidgetId].data['xAxis'][0]['categories'].push(name);

                                // Assemble object for chart
                                settedValue = {
                                    "name": name,
                                    "data": [chartsData[data.WidgetId][v.DataId]['value']],
                                    "id"  : v.DataId,
                                    "color": '#1ab394',

                                    "negativeColor": '#ff0000',
                                    "pointWidth": 15,
                                    "showInLegend": false

                                }

                                if(!inited){
                                    newData[chartsData[data.WidgetId][v.DataId]['index']] = settedValue;
                                }
                                else{

                                    settedValue['animation'] = true;

                                    // Add 
                                    if(!update.type){
                                        widgets[data.WidgetId].inited.series.push(settedValue);
                                    }
                                    // Update
                                    else if(update.type == 1){
                                        widgets[data.WidgetId].inited.series[chartsData[data.WidgetId][v.DataId]['index']] == settedValue;
                                    }
                                    // Delete
                                    else{
                                        // console.log("DELETED");
                                        $timeout(function() {

                                            charts
                                                .series[chartsData[data.WidgetId][v.DataId]['index']].setVisible(false,true);
                                            }, 1000
                                        );
                                    }
                                }
                                
                            });  
                            
                            if(!inited){

                                // Initiation 
                                widgets[data.WidgetId].data['series'] = newData;
                            }

                            // Assign Wraning Count
                            widgets[data.WidgetId].WarningsCount = data.WarningsCount;

                            // Enable chart
                            if(!widgets[data.WidgetId].data.loading)
                                widgets[data.WidgetId].data.loading = true;

                            
                        })
                    }
                });
                /**
                 * Listener for barchart
                 */
                $scope.$on("barchartData", function (event, data, widgets) {
                    var widgets = $scope.widgets;
                    if(typeof(widgets[data.WidgetId]) !== 'undefined'){

                        $scope.$evalAsync(function () {

                            var inited = false;
                            if(typeof(widgets[data.WidgetId].inited) !== 'undefined' )
                                inited = true;

                            if(inited){
                                var charts = widgets[data.WidgetId].inited.getChartObj();
                            }

                            var willShow = {},
                                update   = {},
                                minutes  = 0,
                                newData  = [];

                            
                            angular.forEach(data.DataStateChanges,function(v,k){

                                // Get Total
                                if(v.DataId == $rootScope.widgetTotal){
                                    
                                    // Add Total
                                    widgets[data.WidgetId].data['title'].text = (v.Data ? $filter('currency')(v.Data,'') : "$0.00");
                                    return false;   
                                }


                                // Check for Time & Value
                                if(v.Data && typeof(v.Data.Time) === 'undefined')
                                    return false;
                                if(typeof(v.Data.Value) === 'undefined')
                                    return false;

                                value = v.Data.Value || 0;
                                time  = v.Data.Time  || "";

                                // Get Time for Tooltip
                                timeObj = new Date(time);

                                // Assemble time-value
                                valueChart = [Date.parse(time),Number(value.toFixed(2))];

                                // Assemble object for collection
                                update = {
                                    "widgetID" : data.WidgetId,
                                    // "value"    : valueChart,
                                    "value"    : value,
                                    "type"     : v.ModificationType,
                                    "id"       : v.DataId,
                                    "key"      : k
                                }

                                // Put Object in collection
                                mapChartData(update);

                                // Assemble object for chart
                                settedValue = {
                                    "name": timeObj.getHours()+":"+minutes,
                                    "data": [chartsData[data.WidgetId][v.DataId]['value']],
                                    "id"  : v.DataId,
                                    "color": '#1ab394',
                                    "pointWidth": 15,
                                    "showInLegend": false,
                                    "showInNavigator": true

                                }

                                // Get minutes
                                minutes = timeObj.getMinutes();
                                minutes = ( minutes < 10 ? "0"+minutes : minutes );

                                if(!inited){
                                    newData[chartsData[data.WidgetId][v.DataId]['index']] = settedValue;
                                }
                                else{

                                    settedValue['animation'] = true;

                                    // Add 
                                    if(!update.type){
                                        widgets[data.WidgetId].inited.series.push(settedValue);
                                    }
                                    // Update
                                    else if(update.type == 1){
                                        widgets[data.WidgetId].inited.series[chartsData[data.WidgetId][v.DataId]['index']] == settedValue;
                                    }
                                    // Delete
                                    else{

                                        $timeout(function() {
                                            charts
                                                .series[chartsData[data.WidgetId][v.DataId]['index']].setVisible(false,true);
                                            }, 1000
                                        );
                                    }
                                }
                                
                            });  
                            
                            if(!inited){

                                // Initiation 
                                widgets[data.WidgetId].data['series'] = newData;
                            }

                            // Assign Wraning Count
                            widgets[data.WidgetId].WarningsCount = data.WarningsCount;

                            // Enable chart
                            if(!widgets[data.WidgetId].data.loading)
                                widgets[data.WidgetId].data.loading = true;

                            
                        })
                    }
                });
                /**
                 * Listener for linechart
                 */
                $scope.$on("linechartData", function (event, data, widgets) {
                    var widgets = $scope.widgets;
                    if(typeof(widgets[data.WidgetId]) !== 'undefined'){

                        $scope.$evalAsync(function () {
                            
                            var newData  = [],
                                willShow = {};
                                init     = false,
                                update   = {};
                                
                            angular.forEach(data.DataStateChanges,function(v,k){
 
                                if(v.Data && typeof(v.Data.Time)  === 'undefined')
                                    return false;
                                if(v.Data && typeof(v.Data.Value) === 'undefined')
                                    return false;

                                value = v.Data.Value || 0;
                                time  = v.Data.Time  || "";
                                
                                valueChart = [Date.parse(time),Number(value.toFixed(2))];

                                update = {
                                    "widgetID" : data.WidgetId,
                                    "value"    : valueChart,
                                    // "value"    : value,
                                    "type"     : v.ModificationType,
                                    "id"       : v.DataId,
                                    "key"      : k
                                }
                                mapChartData(update);

                                widgets[data.WidgetId].data['series'][0].data[chartsData[data.WidgetId][v.DataId]['index']] = chartsData[data.WidgetId][v.DataId]['value'];

                                // Get last tick-value and time
                                if(k+1 === data.DataStateChanges.length){
                                    widgets[data.WidgetId].data.value = parseInt(value);
                                    widgets[data.WidgetId].data.date  = Date.parse(time); 
                                }
                            }); 
                            // Assign Wraning Count
                            widgets[data.WidgetId].WarningsCount = data.WarningsCount;

                            // Enable chart
                            if(!widgets[data.WidgetId].data.loading)
                                widgets[data.WidgetId].data.loading = true;
                        })
                    }
                });
                /**
                 * Listener for aggregated
                 */
                $scope.$on("aggregatedData", function (event, data, widgets) {
                    var widgets = $scope.widgets;
                    if(typeof(widgets[data.WidgetId]) !== 'undefined'){
                        $scope.$evalAsync(function () {
                            value = data.DataStateChanges[0].Data;

                            if(value < 0){
                                widgets[data.WidgetId].bg = "red-bg";
                            }else if(value > 0){
                                widgets[data.WidgetId].bg = "navy-bg";
                            }
                            else{
                                widgets[data.WidgetId].bg = "";
                            }
                            widgets[data.WidgetId].data.value = value.toFixed(2);

                            // Assign Wraning Count
                            widgets[data.WidgetId].WarningsCount = data.WarningsCount;

                            // is mobile
                            widgets[data.WidgetId].mobile        = $rootScope.ENV.mobile;

                            // Enable chart
                            if(!widgets[data.WidgetId].data.loading)
                                widgets[data.WidgetId].data.loading = true;
                        })
                    }
                });
                /**
                 * Listener for wdatatable
                 */
                $scope.$on("wdatatableData", function (event, data, widgets) {
                    var widgets = $scope.widgets;
                    if(typeof(widgets[data.WidgetId]) !== 'undefined'){
                        
                        $scope.$evalAsync(function () {
                                var inited = false;

                                if(typeof(widgets[data.WidgetId].data.api) !== 'undefined' )
                                    inited = true;

                                if(inited){
                                    var api = widgets[data.WidgetId].data.api;
                                }

                                var columnDefs  = (widgets[data.WidgetId].data.gridOptions ? widgets[data.WidgetId].data.gridOptions.columnDefs : []),
                                    rowData     = [];

                                if(data.DataStateChanges.length){

                                    // Get Data
                                    angular.forEach(data.DataStateChanges, function(v,k){
                                        // Get Columns Name
                                        if(v.DataId == $rootScope.widgetTotal){

                                            if( !columnDefs.length ){

                                                angular.forEach(v.Data, function(d,i){
                                                    columnDefs.push({
                                                        enableCellChangeFlash:true,
                                                        headerName: d.DisplayName, 
                                                        field: d.PropertyName, 
                                                        // valueParser: $scope.numberValueParser//, 
                                                        // valueParser: function(params) {
                                                        //     // return Number(params.newValue.toFixed(2));
                                                        //     return Math.floor(params).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,").toFixed(2);
                                                        // },
                                                        // cellRenderer:'agGroupCellRenderer'
                                                        // cellRenderer:'agAnimateSlideCellRenderer'
                                                        cellRenderer:'agAnimateShowChangeCellRenderer'
                                                    });
                                                })
                                            }
                                        }
                                        // Set / Update / Delete data
                                        else{
                                            
                                            // Load initial data
                                            if(!inited){
                                                
                                                if(v.Data){
                                                    v.Data['id'] = v.DataId;
                                                    rowData.push(v.Data);
                                                }
                                            }
                                            // Update 
                                            else{

                                                switch(v.ModificationType){
                                                    // Add
                                                    case 0:

                                                        var rowNode = api.getRowNode(v.DataId);
                                                        v.Data['id'] = v.DataId;
                                                            rowData.push(v.Data);
                                                        break;

                                                    // Update
                                                    case 1:
                                                        api = widgets[data.WidgetId].data.api;
                                                        var rowNode = api.getRowNode(v.DataId);

                                                        // If there is data update it
                                                        if(rowNode){
                                                            
                                                            v.Data['id'] = v.DataId;
                                                            api.batchUpdateRowData({
                                                                update: [v.Data]
                                                            })
                                                        }
                                                        break;

                                                    // Delete
                                                    case 2:

                                                        api         = widgets[data.WidgetId].data.api;
                                                        var rowNode = api.getRowNode(v.DataId);
                                                        api.updateRowData({remove: [rowNode]});
                                                        

                                                        break;
                                                }

                                            }
                                        }
                                            
                                    })
                                }

                                if(!inited){

                                    widgets[data.WidgetId].data.gridOptions = {}

                                    // Assign Table
                                    widgets[data.WidgetId].data.gridOptions = {
                                        defaultColDef: {

                                            // Additional Props
                                            // valueFormatter: function (params) {
                                            //     return formatNumber(params.value);
                                            // },
                                            // cellClass: 'align-right',
                                            
                                            enableCellChangeFlash: true,
                                            cellClass: 'text-right'
                                        },
                                        // columnDefs: columnDefs,
                                        getRowNodeId : function(data) {
                                            if(typeof data.id != undefined)
                                                return data.id;
                                        },

                                        columnDefs  : columnDefs,
                                        rowData     : rowData,
                                        
                                        // Pagination
                                        pagination: true,
                                        paginationAutoPageSize:true,

                                        // Sorting
                                        enableSorting: true,
                                        enableFilter: true,

                                        rowSelection: 'multiple',
                                        enableColResize: true,

                                        // Addition Props:
                                        // enableCellChangeFlash : true,
                                        // refreshCells : true,
                                        // animateRows: true,
                                        // deltaRowDataMode:true,
                                        // defaultColDef:{
                                        //     editable: true,
                                        // }
                                        // rowGroupPanelShow: 'always',
                                        // pivotPanelShow: 'always',
                                        // enableRangeSelection: true,
                                        // columnDefs: columnDefs,

                                        // Filter
                                        floatingFilter:true,

                                        onGridReady: function(params){
                                            api                             = params.api;
                                            args                            = params;
                                            widgets[data.WidgetId].data.api = params.api;
                                        }

                                    }


                                }

                                // Assign Wraning Count
                                widgets[data.WidgetId].WarningsCount = data.WarningsCount;

                                // Enable chart
                                if(!widgets[data.WidgetId].data.loading)
                                    widgets[data.WidgetId].data.loading = true;
                            
                        })
                    }
                });

            }
        )

        // Dashboard Widget Configuration 
        // .controller('dashboardWidgetConfiguration',
        //     function dashboardWidgetConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, helpers ){
        //         managementConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, "dashboardWidgetConfiguration", helpers );
        // })

        /*------------------------------------------------------------------
        [  WIDGETS CONTROLLERS & DIRECTIVES  ]
        */

        .controller('widgetDatatableCtrl',
            /**
             * widget Datatable - 
             * used in Widget view
             */
            function widgetDatatableCtrl($scope,$rootScope,DTOptionsBuilder,DTColumnDefBuilder) {
            })

        .controller('widgetAggregatedCtrl',/**
             * widgetAggregatedCtrl - 
             * used in Widget view
             */
            function widgetAggregatedCtrl($scope,$rootScope) {
            })


        .controller('widgetHBarchartCtrl',
            /**
             * widgetHBarchartCtrl - Controller for Chartist library
             */
            function widgetHBarchartCtrl($scope,$rootScope) {
            })

        .controller('widgetBarchartCtrl',
            /**
             * flotChartCtrl - Controller for data for All flot chart
             * used in Flot chart view
             */
            function widgetBarchartCtrl($scope, $rootScope) {
            })

        .controller('widgetLinechartCtrl',
            /**
             * flotChartCtrl - Controller for data for All flot chart
             * used in Flot chart view
             */
            function widgetLinechartCtrl($scope,$rootScope) {

            })

        //////////////////


        .directive('compileDirective', function($compile) {
            return {
                // restrict: "E",
                restrict: "A",
                replace: true,
                link: function(scope, element, attr) {

                    scope.$watch(
                        function() {
                            return attr.directive;
                        }, 
                        function(val) {
                            element.html("");
                            if (val) {
                                var directive = $compile(angular.element(val))(scope);
                                element.append(directive);
                            }
                        }
                    );
                }
            };
        })
   
        .directive('aggregated', function() {
            
            return {
                templateUrl: 'views/widgets/widget-aggregated.html'
            };
        })

        .directive('hbarchart', function() {
            
            return {
                templateUrl: 'views/widgets/widget-hbarchart.html'
            };
        })

        .directive('barchart', function() {
            
            return {
                templateUrl: 'views/widgets/widget-barchart.html'
            };
        })        

        .directive('wdatatable', function() {
            
            return {
                templateUrl: 'views/widgets/widget-datatable.html'
            };
        })

        .directive('linechart', function() {
            
            return {
                templateUrl: 'views/widgets/widget-linechart.html'
            };
        })


        /*------------------------------------------------------------------
        [  FILTERS  ]
        */
       
       .filter('toDashboard', function() {
            return function(widgets,dashboardId) {

                var filtered = {};
                
                angular.forEach(widgets,function(v,k){
                    if(v.view.dashboardId == dashboardId)
                        filtered[v.Id] = v;
                })
                return filtered;
            }
        })
        .filter('isEmpty', [function() {
            return function(object) {
                return (angular.equals({}, object));// || (typeof object == undefined));
            }
        }])
        .filter('deCapitalize', [function() {
            return function(input) {
                return (!!input) ? input.charAt(0).toLowerCase() + input.substr(1) : '';
            }
        }])



