angular
    .module('registerModule',[])
    .controller('register',
        function register( $scope, $location, $cookies, apiBackOffice ,redirectToUrlAfterLogin,homePath,$rootScope,$state) {

        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
        $scope.errorLogin = false;
        $scope.effect = "";


        // $scope.note         = "Lorem ipsum dolor sit amet...";
        apiBackOffice.GetUsers().then(function (data) {
            var user = {};
            angular.forEach(data,function(v,k){
                user[v['Id']] = {};
                user[v['Id']] = {
                    "Id"        : v['Id'],
                    "UserName"  : v['UserName'],
                    "Password"  : v['Password'],
                    "Claims"    : v['Claims']
                }
                // $scope.usersData.push(user); 
            })
            $rootScope.usersData = user;
        })

        /**
         * Submit - make login
         * You can access the values by:
         * form.username.$viewValue
         * $scope.login.username
         * this
         * @param  {Object} form pass the form object
         */
        $scope.submit = function(form){
            
            // Check is the values are undefined
            if(
                form.username.$viewValue == undefined || 
                form.password.$viewValue == undefined 
                // form.email.$viewValue    == undefined
            ){
                $scope.errorLogin = true;
                $scope.effect = "shake animated";
                return false;
            }else
            {

                // Login Proccess - GetToken
                apiBackOffice.CreateUser( form.username.$viewValue , form.password.$viewValue ).then(function (data) {
                    
                   // If there is no error
                    if(!data.hasError){

                        console.log(data);
                        $state.go('management.users', {});

                    }else
                    {
                        $rootScope.isWorkingForce = false;
                        $rootScope.isWorking = false;
                        $scope.errorLogin = true;
                        $scope.effect = "shake animated";
                    }
                });

            }
        };

    })