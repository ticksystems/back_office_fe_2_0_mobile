angular
    .module('changePasswordModule',[])
    .controller('changePassword',
        function register( $scope, $location, $cookies, apiBackOffice,$rootScope,$state,apiAlerts) {


        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
        $scope.errorLogin = false;
        $scope.effect = "";

        var User    = JSON.parse($cookies.get('User')) || Settings.User,
        UserRigths  = JSON.parse($cookies.get("UserRigths")) || Settings.UserRigths;



// console.log($rootScope.permissions);            
// console.log($rootScope.$state.current.data.permissions[0]); 

        // $scope.hasRights = false;

        // _.map(UserRigths,function(item){
        //     if($rootScope.$state.current.data.permissions[0] == item.Route)
        //         $scope.hasRights = true;
        // })
        // console.log($scope.hasRights);
        // if (!$scope.hasRights) {
        //     console.log("aa");
        //     return false;
        // };
        /**
         * Submit - make login
         * You can access the values by:
         * form.username.$viewValue
         * $scope.login.username
         * this
         * @param  {Object} form pass the form object
         */
        $scope.submit = function(form){
            
            // Check is the values are undefined
            if(
                form.oldPassword.$viewValue == undefined ||
                form.newPassword.$viewValue == undefined 
            ){
                $scope.errorLogin = true;
                $scope.effect = "shake animated";
                return false;
            }else
            {

                // Login Proccess - GetToken
                apiBackOffice.ChangePassword( User.user_id,form.oldPassword.$viewValue, form.newPassword.$viewValue ).then(function (data) {
                    
                    // If there is no error
                    if(!data.hasError){
                        
                        apiAlerts.addAlert({
                            "type"    : 'success',
                            "title"   : "Success",
                            "timeout" : 2000, 
                            "body"    : '' ,
                            "toasterId" : "quick-toaster-container",
                            "clickHandler" : function(){
                                return true;
                            }
                        });

                    }else
                    {
                        $rootScope.isWorkingForce = false;
                        $rootScope.isWorking = false;
                        $scope.errorLogin = true;
                        $scope.effect = "shake animated";
                    }
                });

            }
        };

    })