angular
    .module('loginModule',[])
    .controller('login',
        function login( $scope, $location, $cookies, apiAuthentication,redirectToUrlAfterLogin,homePath,$rootScope,$state, apiLogging) {

        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
        $scope.errorLogin = false;
        $scope.effect = "";

        /**
         * Submit - make login
         * You can access the values by:
         * form.username.$viewValue
         * $scope.login.username
         * this
         * @param  {Object} form pass the form object
         */
        $scope.submit = function(form){
            
            // Check is the values are undefined
            if(
                form.username.$viewValue == undefined || 
                form.password.$viewValue == undefined
            ){
                $scope.errorLogin = true;
                $scope.effect = "shake animated";
                return false;
            }else
            {
                // Login Proccess - GetToken
                apiAuthentication.Login( form.username.$viewValue , form.password.$viewValue ).then(function (data) {

                   // If there is no error
                    if(!data.hasError){

                        // Combine all data 
                        var params = {
                            "caller"    : Settings.Caller,
                            "userToken" : data.access_token,
                            "tokenType" : data.token_type,
                            "expiresIn" : data.expires_in,
                            "instanceId": Settings.InstanceID,
                            "userRigths": JSON.stringify(JSON.parse(decodeURIComponent(data.user_rigths)))
                        };
                        // Add username to reposnse
                        data.username       = form.username.$viewValue;

                        // Set Token
                        Settings.UserToken  = params.userToken;
                        Settings.TokenType  = params.tokenType;
                        Settings.UserRigths = params.userRigths;
                        Settings.User       = JSON.stringify(data);

                        // Cookie with time
                        var d = new Date();
                        d.setMinutes(d.getMinutes() + Settings.ExpireTime);

                        // Set User Token
                        $cookies.put('UserToken'    , Settings.UserToken, {"expires": d } );
                        // Set Token Type
                        $cookies.put('TokenType'    , Settings.TokenType, {"expires": d } );
                        // Set User Rights
                        $cookies.put('UserRigths'   , Settings.UserRigths, {"expires": d } );
                        // Set User
                        $cookies.put('User'         , Settings.User, {"expires": d } );

                        // Log user's data
                        apiLogging.logSystem(data.user_id);

                        // Emmit event and redirect to url
                        $scope.setLogged();

                    }else
                    {
                        apiAuthentication.SetUnLoggedState({});
                        $scope.errorLogin = true;
                    }
                },  function(data){
                        apiAuthentication.SetUnLoggedState({});
                        $scope.errorLogin = true;
                    }
                );

            }
        };

        /**
         * setLogged - Setup some user adjustments 
         */
        $scope.setLogged = function() {

            if(redirectToUrlAfterLogin.url != '/')
                // Redirect to last requested url
                apiAuthentication.RedirectToAttemptedUrl();
            else{
                // Redirect to the welcome
                $location.path(homePath.url);
                // $state.go('index.dashboard', {});
            }
        }
    })