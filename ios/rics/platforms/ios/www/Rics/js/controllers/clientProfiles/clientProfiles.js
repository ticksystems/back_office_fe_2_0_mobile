angular
    .module('clientProfilesModule',['agGrid','angular-flatpickr'])
    .controller('clientProfiles',
        function clientProfiles( $scope, $location, $cookies, apiBackOffice ,redirectToUrlAfterLogin,homePath,$rootScope,$state,$stateParams,$q, $filter, $timeout, helpers) {


        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
       
        $rootScope.isProcessing = false;

        $scope.clientProfile    = undefined;
        $scope.showEmpty        = false;

        $scope.modelOptions     = {
            debounce: {
                default: 500,
                blur: 250,
                updateOn: ''
            },
            minLength : 1,
            getterSetter: true
        };

        $scope.linechart    = {};
        $scope.piechart     = {};
        $scope.datatable    = {
            data : {} 
        };

        // Inner chart / line color 
        $scope.innerChart   = {
            // color : "#ba2f7d"
            color : "#08c"
        };
        


        /*------------------------------------------------------------------
        [  FORM METHODS  ]
        */
       
        /**
         * searchClientProfile - Search request in API
         */
        $scope.searchClientProfile  = function(value){ 

            var deferred            = $q.defer();

            $rootScope.isProcessing = true;
            $scope.showEmpty        = false;
            
            apiBackOffice.GetAvailableClientLogins(value).then(
                function(data) {

                    $rootScope.isProcessing = false;
                    
                    deferred.resolve(data);
                
                }, function() {

                    $scope.showEmpty = true;
                    deferred.reject([]);

                    $rootScope.isProcessing = false;
                }
            )

            return deferred.promise;
        }

        /**
         * submitClientProfile  - Search request in API via `Submit` button 
         * @param  {String}     - field 
         */
        $scope.submitClientProfile = function(field) {
            
            // Copy value
            var val = $scope.form[field].$viewValue;

            // Set to empty (little click typeahead hack)
            $scope.clientProfile = '';
            $scope.form[field].$setViewValue('');

            // Return value to open
            $scope.form[field].$setViewValue(val);
            $scope.clientProfile = val;

            // Check for empty
            if( !$scope.form[field].$viewValue || $scope.form[field].$viewValue.length == 0 ){

                // Set Error
                $scope.form[field].$invalid = true;
                $scope.form[field].$dirty = true;
                $scope.form[field].$error.required = true;

                return false;
            }
            else{    
                // Search
                $scope.searchClientProfile($scope.form[field].$viewValue)
            }
        }

        /**
         * selectClient - when something from typeahead is selected
         *                it will trigger ajax to get current profile data with
         *                selected value
         */
        $scope.selectClient = function( item, model, label, event, field ) {
            
            if(item.TradingPlatform == $stateParams['platform'] && item.Login == $stateParams['login'])
                return false;
            else
                $location.path("/index/clientProfiles/"+item.TradingPlatform+"/"+item.Login);
        }

        // SUBMIT
        $scope.submitForm = function() {
            if (!$scope.doSubmit) {
                return;
            }
            $scope.doSubmit = false;
        }

        /*------------------------------------------------------------------
        [  INIT  ]
        */

        $scope.init = function(platform, client) {
            
            platform = platform;
            client   = Number(client);
            $scope.allIsReady = false;

            var promises = [];

            var balance = apiBackOffice.GetBalanceOperations(platform,client).then(function(data){
                return data;
            })
            var deals = apiBackOffice.GetDeals(platform,client).then(function(data){
                return data;
            })            
            var equity = apiBackOffice.GetEquity(platform,client).then(function(data){
                return data;
            })
            var swaps = apiBackOffice.GetSwaps(platform,client).then(function(data){
                return data;
            })
            var commissions = apiBackOffice.GetCommissions(platform,client).then(function(data){
                return data;
            })

            // add in queue
            promises.push(balance);
            promises.push(deals);
            promises.push(equity);
            promises.push(swaps);
            promises.push(commissions);

            // All complete
            $q.all(promises).then(

                // On success
                function(data){

                    var result = {};

                    // Result mapping
                    _.map(data, function(resource,i){ 

                        switch(i){
                            case 0:
                                // balance
                                result['balance']     = resource;
                                break;
                            case 1:
                                // deals
                                result['deals']       = resource;
                                break;
                            case 2:
                                // equity
                                result['equity']      = resource;
                                break;
                            case 3:
                                // swaps
                                result['swaps']       = resource;
                                break;
                            case 4:
                                // commissions
                                result['commissions'] = resource;
                                break;
                        }
                    })
                    
                    // Cpy data
                    $scope.originalData = result;

                    // Draw everything
                    $scope.drawAllCharts(result)

            },  function(){})

        }

        /*------------------------------------------------------------------
        [  METHODS  ]
        */

        /**
         * exportExcel - collect all data from REST API and export it to excel
         */
        $scope.exportExcel = function(e) {
            e.preventDefault();

            if($scope.originalData){

                var wb = XLSX.utils.book_new();

                $.each($scope.originalData, function(k, data) {

                    var createXLSLFormatObj = [];

                    /* XLS Head Columns */
                    var xlsHeader = [];

                    /* XLS Rows Data */
                    var xlsRows = data;
                    
                    // Get Headers  
                    $.each(xlsRows, function(index, value) {
                        if(index == 0)
                            // Get Headers
                            $.each(value, function(i, v) {
                                    
                                xlsHeader.push(i);
                            })
                        return false;
                    })

                    createXLSLFormatObj.push(xlsHeader);

                    $.each(xlsRows, function(index, value) {
                        var innerRowData = [];
                        $.each(value, function(i, v) {
                            
                            innerRowData.push(v);
                        });
                        createXLSLFormatObj.push(innerRowData);
                    });

                    /* Sheet Name */
                    var ws_name = k;
                    var ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

                    /* Add worksheet to workbook */
                    XLSX.utils.book_append_sheet(wb, ws, ws_name);

                });

                var fileName = new Date();

                    /* File Name */
                    fileName =  "cp-tp"+$stateParams['platform'] + 
                                "-"+$stateParams['client'] + 
                                "-"+ fileName.getFullYear() +
                                "-"+ (fileName.getMonth()+1) +
                                "-"+ fileName.getDate() +
                                "-"+ fileName.getHours() +
                                "-"+ fileName.getMinutes() +
                                ".xlsx";

                    /* Write workbook and Download */
                    XLSX.writeFile(wb, fileName);

            }
        }

        /**
         * loadPiechart
         * @param  {String} name         depositWithdrawal | profitFactor | wintoLoseRatio
         * @param  {Array} data          data
         * @param  {String|Number} profitFactor
         */
        $scope.loadPiechart = function(name, data, profitFactor) {

            var colors = ['#1ab394','#bababa'],

                // Pie configuration
                pieConfig = {
                    "chart": {
                        "type": "pie",
                        "height": 300,
                    },

                    "plotOptions": {
                        "series": {
                            "stacking": ""
                        },

                        pie: {
                            size: '80%',
                            dataLabels: {
                                enabled: true,
                                distance: 10,
                                allowOverlap: false,
                                borderRadius: 4,

                                formatter: function() {
                                    
                                    fontSize = '12px';
                                    if(this.key == "Lose" || this.key == "Win")
                                        return '<b style="font-size:'+fontSize+'}">'+this.key+'</b><br><span style="color:'+(this.key == "Lose"?'red':'green')+'">' +Highcharts.formatSingle(',.2f',this.y)+' %</span>';
                                    else{
                                        if(this.key == "Loss" || this.key == "Withdrawal")
                                            return '<b style="font-size:'+fontSize+'">'+this.key+'</b><br><span style="color:red">' +( this.y != 0 ? '-':'')  +Highcharts.formatSingle(',.2f',this.y)+'</span>';
                                        else
                                            return '<b style="font-size:'+fontSize+'">'+this.key+'</b><br><span style="color:green">' +Highcharts.formatSingle(',.2f',this.y)+'</span>';
                                    }
                                },
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                                    textShadow: 'none'
                                }
                            },
                            startAngle: 0,
                            endAngle: 360,
                        }
                    },
                    tooltip: {
                        enabled: false
                    },
                    series: [
                        {
                            type: 'pie',
                            innerSize: '75%',
                            data: data,
                            startAngle: 0,
                            showInLegend: true,
                            allowPointSelect: true

                        }
                    ],
                    "title": {
                        "text": ""
                    },
                    "credits": {
                        "enabled": false
                    },
                    "loading" : false
                }

                switch (name) {
                    case 'depositWithdrawal' :
                            // init
                            $scope.piechart.depositWithdrawal = {};

                            // extend basic config to current chart
                            pieConfig.colors   = ['#1ab394','#ed5565'];
                            pieConfig.subtitle = "";

                            // set data and config
                            $scope.piechart.depositWithdrawal.data = pieConfig;

                            if( (_.isNaN(data[0][1]) || _.isNaN(data[1][1])) ||
                                (!data[0][1] && !data[1][1])
                            ){
                                $scope.piechart.depositWithdrawal.data.loading = false;    
                            }else
                                // Load chart
                                $scope.piechart.depositWithdrawal.data.loading = true;

                        break;

                    case 'profitFactor' : 
                    
                            // init
                            $scope.piechart.profitFactor = {};

                            // extend basic config to current chart
                            pieConfig.colors   = ['#1ab394','#ed5565'];
                            pieConfig.subtitle = {
                                text: profitFactor,
                                align: 'center',
                                verticalAlign: 'middle',
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px'
                                },
                                y: -10
                            },
                            pieConfig.series[0].data = data.reverse();

                            // set data and config
                            $scope.piechart.profitFactor.data = pieConfig;

                            if( (_.isNaN(data[0][1]) || _.isNaN(data[1][1])) ||
                                (!data[0][1] && !data[1][1])
                            ){
                                $scope.piechart.profitFactor.data.loading = false;    
                            }else
                            // Load chart
                            $scope.piechart.profitFactor.data.loading = true;

                        break;

                    case 'wintoLoseRatio' : 
                            // init
                            $scope.piechart.wintoLoseRatio = {};


                            // extend basic config to current chart
                            pieConfig.colors   = ['#1ab394','#ed5565'];
                            pieConfig.subtitle = "";

                            // set data and config
                            $scope.piechart.wintoLoseRatio.data = pieConfig;

                            if( (_.isNaN(data[0][1]) || _.isNaN(data[1][1])) ||
                                (!data[0][1] && !data[1][1])
                            ){
                                $scope.piechart.profitFactor.data.loading = false;    
                            }else
                                // Load chart
                                $scope.piechart.wintoLoseRatio.data.loading = true;

                        break;
                }
        }

        /**
         * resetDefault - restore default initial state
         * @param  {[type]} e
         */
        $scope.resetDefault = function(e) {

            e.preventDefault();

            // Restore all data
            $scope.drawAllCharts($scope.originalData);
        }

        /**
         * filterInRange - Take some array and filter it in current date range
         * @param  {Array} data 
         * @return {Array}      
         */
        $scope.filterInRange = function( data ) {

            var filteredStart = moment($scope.filteredRange.Date.startDate).format("YYYY-MM-DD"),
                filteredEnd   = moment($scope.filteredRange.Date.endDate).format("YYYY-MM-DD"),
                iteration     = 0,
                filteredData  = [];

            _.map(data, function(x) {

                if(helpers.exist(x.Time, false))
                    iteration = moment(Date.parse(x.Time)).format("YYYY-MM-DD");
                else
                    iteration = moment(Date.parse(x.TimeStamp)).format("YYYY-MM-DD");

                if( (filteredStart <= iteration) && (iteration <= filteredEnd) )
                    filteredData.push(x);
            });

            return filteredData;
        }

        /**
         * filterData
         */
        $scope.filterData = function(e) {
            
            var allData       = {};

            _.map($scope.originalData, function(x, key){
                allData[key] = $scope.filterInRange( x );
            })

            // Re-draw everything
            $scope.drawAllCharts(allData);
        }

        /**
         * dateRangeTimePicker 
         */
        $scope.dateRangeTimePicker = function(startDate,endDate) {

            // Convert to strings
            startDate             = new Date(startDate).getTime() || new Date().getTime();
            endDate               = new Date(endDate).getTime()   || new Date().getTime();
            $scope.timeFilterConf = {}

            $scope.filteredRange  = {
                Date : {
                    startDate: startDate, 
                    endDate: endDate
                }
            }

            // Mobile datepicker
            $scope.datePostSetup = function(fp) { }
            $scope.dateOpts = {
                dateFormat: 'Y-m-d',
                defaultDate: [startDate, endDate],
                mode: "range",
                locale: { rangeSeparator: ' - ' },
                onChange: function(selectedDates, dateStr, instance){
                    // Update model                    
                    if(selectedDates.length == 2){
                        $scope.filteredRange.Date.startDate = selectedDates[0]
                        $scope.filteredRange.Date.endDate = selectedDates[1]
                    }
                }
            };

            /**
             * daterange - Used as initial model for data range picker in Advanced form view
             */
            $scope.timeFilterConf.daterange = {
                
                "minDate"       : startDate, 
                "maxDate"       : endDate,                
                "startDate"     : startDate, 
                "endDate"       : endDate,
                "showDropdowns" : true,
                "timePicker"    : false,
                "opens"         : "left",
                "timePickerIncrement": 15,

                // Custom filters
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            };

        }

        /**
         * loadLinechart - initialize linechart data
         *                 and initialize daterangepicker 
         */
        $scope.loadLinechart = function() {

            var data   = [],
                update = {};

            // Get daterangepicker start | end date
            if($scope.linechartData.length > 0){
                // Get start
                var startDate = $scope.linechartData[0]['Time'],
                // Get end
                    endDate   = $scope.linechartData[$scope.linechartData.length-1]['Time'];
            }
            // Initialise dateragnepicker
            $scope.dateRangeTimePicker(startDate, endDate);

            // Collect line data
            angular.forEach($scope.linechartData,function(v,k){

                // Create desired format for date
                date         = Date.parse(v.Time);

                data.push([ date, Number(v.EquityCurve) ]);
            })

            // Set linechart
            $scope.linechart.data = {
                chart: {
                    zoomType: 'x',
                    marginBottom: 60,
                    panning: true,
                    followTouchMove: (ENV.mobile ? false : true),
                    panKey: 'shift'
                },
                title: {
                    text : ''
                },
                series: [],
                data: {
                    enablePolling: true
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    plotLines: [{
                        dashStyle: 'shortdash',
                        value: 0,
                        color: '#ed5565',
                        width: 2
                    }]
                },
                plotOptions: {
                    series: {
                        animation: {
                            duration: 2000
                        },
                        marker: {
                            enabledThreshold : 20
                        }
                    },
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },
                xAxis: {
                    title: {
                        text: null
                    },
                    // tickInterval: 20,
                    type: 'datetime',
                    "minTickInterval": (28 * 24 * 3600 * 1000),
                    "dateTimeLabelFormats": {
                        month: '%e %b'
                    },
                    labels: {
                        enabled: true,
                        y: 25
                    },
                    showFirstLabel: true,
                    // startOnTick: true

                },
                
                legend : {
                    enabled : true,
                    y: 20
                },
                tooltip: {
                    crosshairs: {
                        dashStyle: 'solid'
                    },
                    shared: true,
                    // split: true,
                    distance: 30,
                    padding: 5,
                    followPointer: (ENV.mobile ? false : true),
                    valueDecimals: 2,
                    userHTML : true,
                    formatter : function() {

                        s = '<b>' + Highcharts.formatSingle('%Y.%m.%d',this.x) + '<br/>';

                        $.each(this.points, function(index, el) {
                            tickValue = Highcharts.formatSingle(',.2f',this.y);
                            s+= '<span style="color: '+el.color+'">● ' + this.series.name + '</span>: <strong><span style="color:'+( this.y < 0 ? 'red':'green')+'">' + tickValue + '</span></strong><br/>'
                        });

                        return s;
                    }
                },
                responsive: {
                    rules: [{
                        condition: {
                            // maxWidth: 500,
                            // minHeight: 250
                            // maxHeight : 300 
                        },
                        chartOptions: {
                            colors: ["#1ab394"],
                            chart: {
                                height: 250
                            }
                        }
                    }]
                },
                // loading : false
                loading : true,
                credits : {
                    "enabled": false
                }
            }

            // Set linechart data
            $scope.linechart.data['series'][0] = {
                "name": "Equity Curve",
                "tickInterval": 24 * 3600 * 1000,
                "data": data//,
                // type: 'area'
                
            };

            // Load chart
            $scope.linechart.data.loading = true;

        }

        /**
         * drawAllCharts         - Collect data and draw each chart 
         * @param  {Object} data - from promises
         */
        $scope.drawAllCharts = function( data, selectedSymbol ) {
                    var result = data;

                    // Define Columns
                    var columnDefs = [
                            {headerName: 'Symbol', field: 'symbol', cellClass: 'text-left', cellRenderer:'agAnimateShowChangeCellRenderer'},
                            {headerName: 'Traded Volume', field: 'tradedVolume', cellRenderer:'agAnimateShowChangeCellRenderer', filter: "agNumberColumnFilter"/*, floatingCellFormatter*/},
                            {headerName: 'Total Swap', field: 'totalSwap', cellRenderer:'agAnimateShowChangeCellRenderer'},
                            {headerName: 'Total Profit', field: 'totalProfit', cellRenderer:'agAnimateShowChangeCellRenderer'},
                            {headerName: 'Losing Trades', field: 'losingTrades', cellRenderer:'agAnimateShowChangeCellRenderer'},
                            {headerName: 'Winning Trades', field: 'winningTrades', cellRenderer:'agAnimateShowChangeCellRenderer'},
                            {headerName: 'Gross Profit', field: 'grossProfit', cellRenderer:'agAnimateShowChangeCellRenderer'},
                            {headerName: 'Gross Loss', field: 'grossLoss', cellRenderer:'agAnimateShowChangeCellRenderer'}
                        ],
                        rowData    = {},
                        inited     = false;

                    // Other variables
                    var row = {
                            'symbol'        : 0,
                            'tradedVolume'  : 0,
                            'totalSwap'     : 0,
                            'totalProfit'   : 0,
                            'losingTrades'  : 0,
                            'winningTrades' : 0,
                            'grossProfit'   : 0,
                            'grossLoss'     : 0
                        },
                        update = {},
                        init   = false,

                        // Init Totals                        
                        tradedVolume    = 0,
                        totalSwap       = 0,
                        totalProfit     = 0,
                        losingTrades    = 0,
                        winningTrades   = 0,

                        grossProfit     = 0,
                        grossLoss       = 0;


                    /*------------------------------------------------------------------
                    [  CALCULATE SWAPS & COMMISIONS  ]
                    */
                   
                        var swapsListToSymbol = {},
                            commissionsListToSymbol = {};
                            // grandTotalSwap = 0;
                            // grandTotalCommissions = 0;

                        if(result['swaps'].length > 1) 
                            // Calculation swap
                            angular.forEach(result['swaps'], function(v,k){

                                // Reset values
                                if(typeof swapsListToSymbol[v.Symbol] === 'undefined'){
                                    swapsListToSymbol[v.Symbol] = {'swap': 0};
                                }

                                // Reset update object
                                update = {};

                                // calculate every row
                                update = {
                                    'swap'     : ( Number(swapsListToSymbol[v.Symbol]['swap']) + Number(v['Amount']) ).toFixed(2)
                                }

                                // Group result by symbol and update each one
                                swapsListToSymbol[v.Symbol] = update;    
                                
                            })
                        else
                            swapsListToSymbol = false;

                        // Calculate swap totals
                        // grandTotalSwap += _.map(rowData,function(symbol,k){
                        //     return Number(symbol['swap']);
                        // })

                        if(result['commissions'].length > 1) 
                            // Calculation commissions
                            angular.forEach(result['commissions'], function(v,k){

                                // Reset values
                                if(typeof commissionsListToSymbol[v.Symbol] === 'undefined'){
                                    commissionsListToSymbol[v.Symbol] = {'commission': 0};
                                }

                                // Reset update object
                                update = {};

                                // calculate every row
                                update = {
                                    'commission'     : ( Number(commissionsListToSymbol[v.Symbol]['commission']) + Number(v['Amount']) ).toFixed(2)
                                }

                                // Group result by symbol and update each one
                                commissionsListToSymbol[v.Symbol] = update;    
                                
                            })
                            else
                                commissionsListToSymbol = false;
                        
                        // Calculate commisions totals
                        // grandTotalCommissions += _.map(rowData,function(symbol,k){
                        //     return Number(symbol['commission']);
                        // })

                    /*------------------------------------------------------------------
                    [  CALCULATE DATATABLE  ]
                    */

                    var datatableInited = false;

                    // Is Inited
                    if(typeof($scope.datatable.data.gridOptionsApi) !== 'undefined' )
                        datatableInited = true;

                    // Get the API
                    if(datatableInited){
                        var api      = $scope.datatable.data.gridOptionsApi,
                            apiTotal = $scope.datatable.data.gridTotalOptionsApi;
                    }

                    // Calculation for datatable
                    angular.forEach(result['deals'], function(v,k){

                        // Reset values
                        if(typeof rowData[v.Symbol] === 'undefined'){
                            rowData[v.Symbol] = row;
                        }

                        // Reset update object
                        updateRow = {};

                        // swap per symbol
                        rowSwap = swapsListToSymbol ? parseFloat(  Number( swapsListToSymbol[v.Symbol]['swap']).toFixed(2) ) : parseFloat((0).toFixed(2));
                        
                        // commission per symbol
                        rowCommission = commissionsListToSymbol ? parseFloat(  Number( commissionsListToSymbol[v.Symbol]['commission']).toFixed(2) ) : parseFloat((0).toFixed(2));
                            
                        // trader volume per symbol
                        rowTradedVolume = (v.Symbol['DealType'] != 'AUTOIN' && v.Symbol['DealType'] != 'AUTOOUT') ? 
                                            rowData[v.Symbol]['tradedVolume'] + v['Volume'] : 
                                            rowData[v.Symbol]['tradedVolume'];

                        // calculate every row
                        updateRow = {
                            'id'            : v.Id,
                            'ticket'        : v.Ticket,
                            'dealType'      : v.DealType,
                            'symbol'        : v.Symbol,
                            'tradedVolume'  : parseFloat((rowTradedVolume).toFixed(2)),
                            'totalSwap'     : rowSwap,

                            'totalProfit'   : parseFloat( ( Number(rowData[v.Symbol]['totalProfit']) + Number(v['Profit']) + rowSwap + rowCommission ).toFixed(2) ),

                            'losingTrades'  : (Number(v['Profit']) < 0 ? (Number(rowData[v.Symbol]['losingTrades']) + 1) : Number(rowData[v.Symbol]['losingTrades'] )),
                            'winningTrades' : (Number(v['Profit']) > 0 ? (Number(rowData[v.Symbol]['winningTrades']) + 1) : Number(rowData[v.Symbol]['winningTrades'])),

                            'grossProfit'   : parseFloat( (Number(v['Profit']) > 0  ? ( Number(rowData[v.Symbol]['grossProfit'])+v['Profit'] + rowSwap + rowCommission ) : Number(rowData[v.Symbol]['grossProfit'])).toFixed(2) ),
                            'grossLoss'     : parseFloat( (Number(v['Profit']) <= 0 ? ( Number(rowData[v.Symbol]['grossLoss'])+v['Profit']   + rowSwap + rowCommission ) : Number(rowData[v.Symbol]['grossLoss']).toFixed(2)) )
                        }

                        // Group result by symbol and update each one
                        rowData[v.Symbol] = updateRow;    
                        
                    })

                    // Remove grouping and calculate grand totals
                    rowData = _.map(rowData,function(symbol,k){

                        // Collect grand totals
                        tradedVolume  += Number(symbol['tradedVolume']);
                        totalSwap     += Number(symbol['totalSwap']);
                        totalProfit   += Number(symbol['totalProfit']);
                        losingTrades  += Number(symbol['losingTrades']);
                        winningTrades += Number(symbol['winningTrades']);
                        grossProfit   += Number(symbol['grossProfit']);
                        grossLoss     += Number(symbol['grossLoss'  ]);
                        return symbol;
                    })
                    
                    // Crete Grand Totals
                    var dataForBottomGrid = [
                        {
                            'symbol'        : 'Grand Total',
                            'tradedVolume'  : Number(tradedVolume).toFixed(2),
                            'totalSwap'     : Number(totalSwap).toFixed(2),
                            'totalProfit'   : Number(totalProfit).toFixed(2),
                            'losingTrades'  : Number(losingTrades),
                            'winningTrades' : Number(winningTrades),
                            'grossProfit'   : Number(grossProfit).toFixed(2),
                            'grossLoss'     : Number(grossLoss).toFixed(2)
                        }
                    ];

                    // Update
                    if(datatableInited) {
                        api.setRowData(rowData);
                        apiTotal.setRowData(dataForBottomGrid);
                    }
                    
                    if(rowData.length) { 

                        // Configure Datatable
                        $scope.datatable.data.gridOptions = {
                            defaultColDef: {
                                cellClass: 'text-right',
                                suppressMovable: (ENV.mobile ? true : false)
                            },
                            getRowNodeId : function(data) {
                                return data.ticket + data.dealType;
                            },

                            columnDefs  : columnDefs,
                            rowData     : rowData,
                            rowClass    : 'usage',

                            // Pagination
                            // pagination: true,
                            // paginationAutoPageSize:true,
                            enableCellChangeFlash: true,
                            // suppressHorizontalScroll: true,

                            // Sorting
                            enableSorting: true,
                            enableFilter: true,

                            // Resize
                            enableColResize: true,

                            // Filter
                            floatingFilter:true,
                            // domLayout: 'autoHeight',
                            localeText: {noRowsToShow: ' '},
                            rowSelection:'single',

                            // embedFullWidthRows: true,

                            // sync grid
                            alignedGrids: [],
                            onGridReady: function(params){
                                api                             = params.api;
                                args                            = params;
                                $scope.datatable.data.gridOptionsApi = api;

                                if(!$rootScope.ENV.mobile)
                                    // resize to fit
                                    params.api.sizeColumnsToFit();
                            },
                            onGridSizeChanged: function(params){

                                if(!$rootScope.ENV.mobile)
                                    // TODO: add functionality for small devices
                                    params.api.sizeColumnsToFit();
                            },
                            onSelectionChanged: function(el) {

                                // Get selected Row
                                var selectedRow = el.api.getSelectedRows()[0];
                                
                                //-- Inner Profit Factor

                                    // Assign data for Profit Factor for current symbol
                                    var profitFactorData  = $scope.calculateProfitFactorData(1*selectedRow['grossLoss'],1*selectedRow['grossProfit']),
                                        profitFactorSymbol= $scope.calculateProfitFactor(1*selectedRow['grossLoss'],1*selectedRow['grossProfit']),
                                        profitChart       = $scope.piechart.profitFactor.data.getChartObj(),
                                        showLabelsLegend  = !!_.reduce(profitFactorData, function(sum, item){
                                                                return sum + item[1];
                                                            },0),
                                        dataLabelsOptions = {
                                            enabled: showLabelsLegend,
                                            distance: -5,
                                            textShadow: 'none',
                                            style: {
                                                textOutline: 0,
                                            },
                                            borderWidth: 2,
                                            borderColor: $scope.innerChart.color,
                                            backgroundColor: "#fff" || $scope.innerChart.color,
                                            color: '#000',
                                            align: 'left'
                                        };

                                        // Remove previous inner chart
                                        if(profitChart.series.length > 1){
                                            profitChart.series[1].remove()
                                        }

                                        var fontSize = '14px';

                                        //  Infinity symbol
                                        if (profitFactorSymbol == '∞' ) {
                                            fontSize = '28px';

                                            // Update subtitle
                                            profitChart.setTitle({}, { 
                                                useHTML : true,
                                                fontSize: '15px',
                                                y: -13, 
                                                align: 'center',
                                                verticalAlign: 'middle',
                                                widthAdjust : -100,
                                                text:   '<span style="font-size:15px">'+profitFactor+'<span><br/>'+
                                                        '<center><svg height="30" width="30" viewbox="00 40 200 200">'+
                                                          '<path fill="none" stroke="'+$scope.innerChart.color+'" stroke-width="12" d="M100,100'+
                                                                     'C200,0 200,200 100,100'+
                                                                     'C0,0 0,200 100,100z" />'+
                                                        '</svg></center>'
                                            });
                                        }else{

                                            // Update subtitle
                                            profitChart.setTitle({}, { 
                                                useHTML : false,
                                                fontSize: '15px',
                                                y: -10,
                                                text: '<span style="font-size:15px">'+profitFactor+'<span>' + "<br/>" + '<span style="font-size:'+fontSize+';color:'+$scope.innerChart.color+'">'+profitFactorSymbol+'</span>'
                                            });
                                        }

                                    // Add new inner chart
                                    profitChart.addSeries({
                                        type: 'pie',
                                        size: '50%',
                                        colors: [$scope.innerChart.color, Highcharts.Color("#fff").brighten(0).get('rgb')],
                                        showInLegend: showLabelsLegend,
                                        innerSize: '90%',
                                        shadow: false,
                                        data: profitFactorData.reverse(),
                                        dataLabels: dataLabelsOptions
                                    },true);

                                //-- Inner Win to Lose

                                    var wintoLoseRatioData = $scope.calculateWintoLoseRatioData(1*selectedRow['losingTrades'], 1*selectedRow['winningTrades']),
                                        wintoLoseRatioChart= $scope.piechart.wintoLoseRatio.data.getChartObj();
                                    
                                        // Remove previous inner chart
                                        if(wintoLoseRatioChart.series.length > 1){
                                            wintoLoseRatioChart.series[1].remove()
                                        }
                                    
                                    // Add new inner chart
                                    wintoLoseRatioChart.addSeries({
                                        type: 'pie',
                                        size: '50%',
                                        colors: [$scope.innerChart.color, Highcharts.Color("#fff").brighten(0).get('rgb')],
                                        showInLegend: showLabelsLegend,
                                        innerSize: '90%',
                                        inside: true,
                                        data: wintoLoseRatioData,
                                        dataLabels: dataLabelsOptions
                                    },true);


                                //-- Second Linechart    
                                
                                    var secondLineData    = [],
                                        groupedSymbolData = {},
                                        linechart         = $scope.linechart.data.getChartObj();

                                    // Group symbol's data
                                    angular.forEach(result['deals'], function(v,k){

                                        if(v['Symbol'] == selectedRow["symbol"]){ 

                                            // Create desired format for date
                                            date         = Date.parse(v.TimeStamp);
                                            formatedDate = (new Date(date).getFullYear()) + "." + (new Date(date).getMonth() + 1) + "." + (new Date(date).getDate());

                                            if(typeof groupedSymbolData[formatedDate] === 'undefined'){
                                                groupedSymbolData[formatedDate] = {
                                                    "day"   : 0,
                                                    "value" : 0
                                                };
                                            }
                                            // [ Year.month.day, value ]
                                            groupedSymbolData[formatedDate]['value'] = Number(( parseFloat(groupedSymbolData[formatedDate]['value']) + parseFloat(v.Profit) ).toFixed(2));
                                            groupedSymbolData[formatedDate]['day']   = Date.parse(formatedDate);
                                        }
                                    })

                                    // Sort by Day
                                    groupedSymbolData = _.sortBy(groupedSymbolData, function(o){
                                        return new moment(o.day);
                                    });


                                    // Adapt grouped symbols to linechart
                                    secondLineData = _.map(groupedSymbolData, function(x,i){
                                        return [ x.day, x.value ];
                                    });

                                    // Remove previous second line
                                    if(linechart.series.length > 1){
                                        linechart.series[1].remove()
                                    }

                                    // Add second linechart
                                    linechart.addSeries({
                                        "name" : selectedRow['symbol'],
                                        "tickInterval": 24 * 3600 * 1000,
                                        color: $scope.innerChart.color,
                                        data: secondLineData
                                    });

                            }

                        }

                        // Configure Datatable Grand Total
                        $scope.datatable.data.gridTotalOptions = {
                            defaultColDef: {
                                enableCellChangeFlash: true,
                                cellClass: 'text-right'
                            },
                            columnDefs: columnDefs,
                            // we are hard coding the data here, it's just for demo purposes
                            rowData: dataForBottomGrid,
                            enableColResize: true,
                            // debug: true,
                            suppressHorizontalScroll: true,

                            // add class
                            rowClass: 'font-bold',

                            // hide the header on the bottom grid
                            headerHeight: 0,

                            // sync grid
                            alignedGrids: [],
                            onGridReady: function(params){
                                api                             = params.api;
                                args                            = params;
                                $scope.datatable.data.gridTotalOptionsApi = api;
                            }
                        };

                        // Sync both grids
                        $scope.datatable.data.gridOptions.alignedGrids.push($scope.datatable.data.gridTotalOptions);
                        $scope.datatable.data.gridTotalOptions.alignedGrids.push($scope.datatable.data.gridOptions);

                        $scope.datatable.data.loading = true;
                    }
                    else{
                        $scope.datatable.data.loading = false;
                    }

                    /*------------------------------------------------------------------
                    [  LOAD LINE CHART   ]
                    */
                   
                    $scope.linechartData = result['equity'];
                    $scope.loadLinechart();


                    /*------------------------------------------------------------------
                    [  LOAD PIE CHARTS  ]
                    */
                   
                    //-- Profit Factor

                        var profitFactor        = $scope.calculateProfitFactor(grossLoss, grossProfit);

                        // Assign data for Profit Factor
                        $scope.profitFactorData = $scope.calculateProfitFactorData(grossLoss, grossProfit)

                        // Load Profit Factor
                        $scope.loadPiechart('profitFactor',$scope.profitFactorData, profitFactor);

                    //-- Deposit/Withdrawal
  
                        // Assign data for Deposit/Withdrawal
                        $scope.depositWithdrawalData = $scope.calculateDepositWithdrawalData( result['balance'] );

                        // Load Deposit/Withdrawal
                        $scope.loadPiechart('depositWithdrawal',$scope.depositWithdrawalData);
            
                    //-- Win2Lose
                    
                        // Assign data for Win2Lose
                        $scope.wintoLoseRatioData = $scope.calculateWintoLoseRatioData(losingTrades, winningTrades)
                    
                        // Load Win2Lose
                        $scope.loadPiechart('wintoLoseRatio',$scope.wintoLoseRatioData);


                    // All calculations and data is ready
                    $scope.allIsReady = true;
        };

        
        /**
         * CALCULATION Helper Functions 
         */
        
        //--

             $scope.calculateProfitFactor     = function (grossLoss, grossProfit) {
                return (grossLoss != 0 ? (Number(grossProfit) / Number(Math.abs(grossLoss))).toFixed(2) : '∞');
             }
             $scope.calculateProfitFactorData = function (grossLoss, grossProfit) {
                // return data for Profit Factor
                return [
                    ["Loss", Math.abs(Number(grossLoss.toFixed(2)))],
                    ["Profit", Math.abs(Number(grossProfit.toFixed(2)))]
                ];
             }

        //--

            $scope.calculateDepositWithdrawalData = function( balanceData ) {
                var depositTotal    = 0,
                    withdrawalTotal = 0;

                // Calculation 
                angular.forEach(balanceData, function(v,k){
                    if(v.BallanceOperationType == 'DEPOSIT')
                        depositTotal += v.Amount;   
                    else if(v.BallanceOperationType == 'WITHDRAWAL')
                        withdrawalTotal += v.Amount;
                })
            
                // return data for Deposit/Withdrawal
                return [
                    ["Deposit",   Math.abs(Number(depositTotal).toFixed(2))],
                    ["Withdrawal",Math.abs(Number(withdrawalTotal).toFixed(2))],
                ]        
            }

        //--

            $scope.calculateWintoLoseRatioData = function(losingTrades, winningTrades) {
                // Calculation
                var win2loseTotalDeals = losingTrades + winningTrades,
                    win2lose = Math.ceil( (winningTrades / win2loseTotalDeals) * 100 ),
                    lose2win = Math.floor( (losingTrades  / win2loseTotalDeals) * 100 );

                // return data for Win2Lose
                return [
                    ['Win', win2lose],
                    ['Lose', lose2win]                    
                ]
            }

        /**
         * DOM Ready
         */
        angular.element(function(){

            // Check for url
            var platform = helpers.exist($stateParams['platform'], false);
            var client   = helpers.exist($stateParams['client'], false);

            // If there is 
            if(platform && client){
                // Check are the params are numbers
                if(($stateParams['platform'].length > 0) && _.isNumber($stateParams['client']*1)){
                    $timeout(function() {
                        // Set value in the field
                        $scope.clientProfile = $stateParams['client'];

                        // Load all charts
                        $scope.init($stateParams['platform'],$stateParams['client'])
                    }, 200);

                }
            }
            
        })



    })