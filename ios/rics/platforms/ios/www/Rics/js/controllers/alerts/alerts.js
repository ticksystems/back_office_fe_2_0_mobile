angular
    .module('alertsModule',[])
    .controller('alerts',
        function alerts( $scope, $location, $cookies, apiAuthentication,redirectToUrlAfterLogin,homePath,$rootScope,$state,DTOptionsBuilder,DTColumnDefBuilder,apiAlerts) {

        /*------------------------------------------------------------------
        [  Varialbles  ]
        */
        $scope.errorLogin = false;
        $scope.effect = "";

        /**
         * check's - Few variables for checkbox input used in iCheck plugin. Only for demo purpose
         */
        $scope.checkOne     = false;
        // $scope.checkOne     = true;
        $scope.checkTwo     = false;
        $scope.PieChart3 = {
            data: [226, 360],
            options: {
                fill: ["#1ab394", "#d7d7d7"]
            }
        };
        // $scope.note         = "Lorem ipsum dolor sit amet...";

        $scope.alertsData = [
            {
                "id"        : "1",
                "alertType" : "price",
                "status"    : "high",
                "title"     : "Something Big",
                "desc"      : "Desktop publishing packages",
                "ackBy"     : "Stoyan Stanchev",
                "note"      : "Lorem ipsum dolor sit amet.",
                "duration"  : '0, 360',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 10:00",
                "checked"   : false
            },
            {
                "id"        : "2",
                "alertType" : "order",
                "status"    : "middle",
                "title"     : "Something Middle",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "",
                // "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : false
            },
            {
                "id"        : "3",
                "alertType" : "deal",
                "status"    : "low",
                "title"     : "Something Low",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : false
            },
            {
                "id"        : "4",
                "alertType" : "price",
                "status"    : "high",
                "title"     : "Something Big",
                "desc"      : "Desktop publishing packages",
                "ackBy"     : "Stoyan Stanchev",
                "note"      : "Lorem ipsum dolor sit amet.",
                "duration"  : '0, 360',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 10:00",
                "checked"   : true
            },
            {
                "id"        : "5",
                "alertType" : "order",
                "status"    : "middle",
                "title"     : "Something Middle",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "",
                // "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : true
            },
            {
                "id"        : "6",
                "alertType" : "deal",
                "status"    : "low",
                "title"     : "Something Low",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : true
            },
            {
                "id"        : "7",
                "alertType" : "price",
                "status"    : "high",
                "title"     : "Something Big",
                "desc"      : "Desktop publishing packages",
                "ackBy"     : "Stoyan Stanchev",
                "note"      : "Lorem ipsum dolor sit amet.",
                "duration"  : '0, 360',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 10:00",
                "checked"   : true
            },
            {
                "id"        : "8",
                "alertType" : "order",
                "status"    : "middle",
                "title"     : "Something Middle",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "",
                // "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : true
            },
            {
                "id"        : "9",
                "alertType" : "deal",
                "status"    : "low",
                "title"     : "Something Low",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : true
            },
            {
                "id"        : "10",
                "alertType" : "price",
                "status"    : "high",
                "title"     : "Something Big",
                "desc"      : "Desktop publishing packages",
                "ackBy"     : "Stoyan Stanchev",
                "note"      : "Lorem ipsum dolor sit amet.",
                "duration"  : '0, 360',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 10:00",
                "checked"   : true
            },
            {
                "id"        : "11",
                "alertType" : "order",
                "status"    : "middle",
                "title"     : "Something Middle",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "",
                // "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : true
            },
            {
                "id"        : "12",
                "alertType" : "deal",
                "status"    : "low",
                "title"     : "Something Low",
                "desc"      : "Lorem Ipsum as their default model text",
                "ackBy"     : "John Doe",
                "note"      : "Consectetur adipisicing elit. ",
                "duration"  : '0, 380',
                "closedBy"  : "",
                "closedAt"  : "",
                "createdAt" : "12.02.2015 12:00",
                "checked"   : true
            }
        ];

        /*------------------------------------------------------------------
        [  ALERTS  ]
        */
        $rootScope.alerts  = {};

        angular.element(function(){

            // Alert for error
            apiAlerts.addAlert({
                "type"    : 'warning',
                "title"   : "Api Error",
                "timeout" : 2000, 
                "body"    : 'Can\'t get Users',
                // "body": 'bind-unsafe-html',
                // "bodyOutputType": 'directive',
                "toasterId" : "quick-toaster-container",
                // "directiveData": "aaaaaddsdsdsdsWarning",
                // "directiveData": { 'name': 'Bob' },
                "clickHandler" : function(){
                    return true;
                }
            });
            // Alert Alerts - Price
            apiAlerts.addAlert({
                "type"    : 'warning',
                "title"   : "Price",
                "timeout" : 4000, 
                "body": 'bind-unsafe-html',
                // "body": 'bind-unsafe-html',
                "bodyOutputType": 'directive',
                "toasterId" : "note-toaster-container",
                // "directiveData": "aaaaaddsdsdsdsWarning",
                "directiveData": { 'name': 'Bob' },
                "clickHandler" : function(){
                    // on click  not close it
                    return true;
                }
            });
            // Alert Alerts - Order
            apiAlerts.addAlert({
                "type"    : 'info', 
                "title"   : "Order",
                "timeout" : 4000, 
                // "body": 'bind-unsafe-html',
                // "bodyOutputType": 'directive',
                "toasterId" : "note-toaster-container",
                "directiveData": "aaaaaddsdsdsdsInfo",
                "clickHandler" : function(obj){
                    // on click  not close it
                    return true;
                }
            });
            // Alert Alerts - Deal
            apiAlerts.addAlert({
                "type"    : 'error', 
                "title"   : "Deal",
                "timeout" : 0, 
                // "body": 'bind-unsafe-html',
                // "bodyOutputType": 'directive',
                "toasterId" : "note-toaster-container",
                "directiveData": "aaaaaddsdsdsdsError",
                "clickHandler" : function(obj){
                    
                    // on click close it
                    return apiAlerts.removeAlert(obj);
                    // return apiAlerts.removeAlert(obj.id);
                    // console.log($rootScope.alerts);
                }
            });
            // apiAlerts.addAlert({
            //     "type"      : 'error', 
            //     "title"     : "Some Error",
            //     "timeout"   : 2000, 
            //     "body"      : 'Some Text',
            //     // "bodyOutputType": 'directive',
            //     "toasterId" : "quick-toaster-container",
            //     // "toastId"   : "0000-xxxxx-0000",
            //     "clickHandler" : null
            // });
            // console.log($rootScope.alerts);
        })

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>lTfgitp')

            .withButtons([
                // {extend: 'copy'},
                // {extend: 'csv'},
                // {extend: 'excel', title: 'ExampleFile'},
                // {extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                //     customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //             .addClass('compact')
                //             .css('font-size', 'inherit');
                //     }
                // },
                {
                    text: 'Close',
                    // key: '1',
                    action: function (e, dt, node, config) {
                        // alert('Button activated');
                        // console.log(e);
                        // console.log(dt);
                        // console.log(node);
                        // console.log(config);
                    }
                }
            ]);
        // console.log("======================================================");
        // console.log(DTColumnDefBuilder.newColumnDef(0).DTColumn);
        // console.log(DTColumnDefBuilder);
        // console.log(DTColumnDefBuilder.newColumnDef(0));
        $scope.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0).withOption({
                  "columnDefs": [
                    { "width": "20%", "targets": 0 }
                  ]
            })
            // })
            // DTColumnDefBuilder.newColumnDef(1).notVisible(),
            // DTColumnDefBuilder.newColumnDef(2).notSortable()
        ];

        /**
         * persons - Data used in Tables view for Data Tables plugin
         */
        $scope.persons = [
            {
                id: '1',
                firstName: 'Monica',
                lastName: 'Smith'
            },
            {
                id: '2',
                firstName: 'Sandra',
                lastName: 'Jackson'
            },
            {
                id: '3',
                firstName: 'John',
                lastName: 'Underwood'
            },
            {
                id: '4',
                firstName: 'Chris',
                lastName: 'Johnatan'
            },
            {
                id: '5',
                firstName: 'Kim',
                lastName: 'Rosowski'
            }
        ];

        /**
         * Submit - make login
         * You can access the values by:
         * form.username.$viewValue
         * $scope.login.username
         * this
         * @param  {Object} form pass the form object
         */
        $scope.submit = function(form){
            
            // Check is the values are undefined
            if(
                form.username.$viewValue == undefined || 
                form.password.$viewValue == undefined
            ){
                $scope.errorLogin = true;
                $scope.effect = "shake animated";
                return false;
            }else
            {
                // console.log(form.username.$viewValue);
                // console.log(form.password.$viewValue);

                // Login Proccess - GetToken
                apiAuthentication.Login( form.username.$viewValue , form.password.$viewValue ).then(function (data) {
                    
                   // If there is no error
                    if(!data.hasError){

                        // console.log(data);

                        var params = {
                            "caller"    : Settings.Caller,
                            // "userToken" : data.data.token,
                            "userToken" : "DAEBA8233D05415D8BFF4334F893413F",
                            "instanceId": Settings.InstanceID
                        };

                        // Set Token
                        // Settings.UserToken = data.data.token;
                        Settings.UserToken = params.userToken;



                        // Setting a cookie with time
                        var d = new Date();
                        d.setMinutes(d.getMinutes() + Settings.ExpireTime);
                        $cookies.put('UserToken', Settings.UserToken, {"expires": d } );

                        //  Get Requester Provider
                        // apiBigNet.GetUserRequesterCountry( params ).then(function (response) {

                            // if(!response.hasError){
                            // if(!data.hasError){
                                
                                // Set Requester 
                                // Settings.RequesterCountryCode = response['data'];

                                // Setting a cookie - Requester Country Code
                                // $cookies.put('RequesterCountryCode', Settings.RequesterCountryCode );

                                // Emmit event and redirect to url
                                $scope.setLogged();

                            /*}else{

                                $scope.errorLogin = true;
                                $scope.effect = "shake animated";

                                // Remove token
                                Settings.UserToken = "";
                                // Remove the cookie
                                $cookies.remove('UserToken');
                            }*/

                        // })

                    }else
                    {
                        $rootScope.isWorkingForce = false;
                        $rootScope.isWorking = false;
                        $scope.errorLogin = true;
                        $scope.effect = "shake animated";

                        // Remove token
                        Settings.UserToken = "";
                        // Remove the cookie
                        $cookies.remove('UserToken');
                    }
                });

            }
        };

        /**
         * setLogged - Setup some user adjustments 
         */
        $scope.setLogged = function() {

            if(redirectToUrlAfterLogin.url != '/')
                // apiAuthentication.RedirectToAttemptedUrl();
                $state.go('index.dashboard', {});
            else{
                // console.log("homePath.url",homePath.url);
                // Redirect to the welcome
                // $location.path(homePath.url);
                $state.go('index.dashboard', {});
            }
        }
    })