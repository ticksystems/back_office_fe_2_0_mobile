/**
 * managementConfiguration - method that handles alert|accountTypes configuration
 * @param  {Object} $scope        - current scope
 * @param  {Object} $rootScope    - rootScope
 * @param  {Object} apiBackOffice - factory api layer 
 * @param  {Object} $uibModal     - modal instance
 * @param  {Object} $stateParams  - url state params
 * @param  {Object} $state        - state object | redirect methods
 * @param  {String} controller    - accountTypesConfiguration | monitorConfiguration
 */
function managementConfiguration( $scope, $rootScope, apiBackOffice, $uibModal, $stateParams, $state, controller, helpers )
{
             
    /*------------------------------------------------------------------
    [  Varialbles  ]
    */

    $scope.alertId = $stateParams.id || 0;

    $scope.alert                = {};
    $scope.configField          = [];
    $scope.configurationRules   = [];
    $scope.mapRowFields         = {};
    $scope.configMeta           = {};

    // Map Fields with their templates
    $scope.filters              = [];
    $scope.mapFields            = {};

    // For save errors
    $scope.saveResponse         = false;

    /**
     * fieldsConfigOnChange - Hook on Master Filter. Organize, Comparer and InputTemplate
     * @param  {Number} master
     * @param  {Number} index 
     */
    $scope.fieldsConfigOnChange = function(master,index) {

        // If nothing is selected as a Master Filter
        if(!master)
            return true;

        // Selected Value   For Template      
        var valueTemplate = $scope.definitions[index]
                .Items
                .InputTemplate[$scope.mapFields[master]['InputTemplate']];

        var valueComparer = $scope.definitions[index]
                .Items
                .Comparer[$scope.mapFields[master]['Comparer']];

        // If Comparer is Select
        if(valueComparer.ValueDisplayType == "Select") {
            // Check if we have some values for that dropdown or reset it
            if( $scope.definitions[index]
                    .Items
                    .Comparer[$scope.mapFields[master]['Comparer']]
                    .Value.length == 0){
                
                // Set Default values
                $scope.definitions[index]
                    .Items
                    .Comparer[$scope.mapFields[master]['Comparer']]
                    .Value = $scope.definitions[index]
                                .Items
                                .Comparer[$scope.mapFields[master]['Comparer']].DropDownValues[0].Value;
            }
        }
        // If Template is Select
        if(valueTemplate.ValueDisplayType == "Select") {
            
            // Check if we have some values for that dropdown or reset it
            if( $scope.definitions[index]
                    .Items
                    .InputTemplate[$scope.mapFields[master]['InputTemplate']]
                    .Value.length == 0){
                // Set Default values
                $scope.definitions[index]
                    .Items
                    .InputTemplate[$scope.mapFields[master]['InputTemplate']]
                    .Value = $scope.definitions[index]
                                .Items
                                .InputTemplate[$scope.mapFields[master]['InputTemplate']].DropDownValues[0].Value;
            }
        }
        // If it's TEXT
        else{
            // TODO: Something for text
        }
        
    }

    /**
     * fieldTypeFromDropdown - Switch required component: SELECT|TEXT
     * @param  {Object} obj  
     * @param  {Number} index
     * @param  {Number|Text} value
     */
    $scope.fieldTypeFromDropdown = function(obj,index,value) {
        $scope.filters[index] = {
            'type' : ((!!obj) ?'number':'text'),
            'value': value
        }
    }

    /**
     * notBlank - Remove -- Please Select an Option --
     * @param  {Object} item
     * @return {Bool}     
     */
    $scope.notBlank= function(item){
       return (!!item.Value.ThisValue);
    } 

    /**
     * setOption - set current value for InputTemplate
     * @param {Object} obj  
     * @param {Number} index
     */
    // $scope.setOption = function(obj,index) {
    //     if(obj.Value.length != 0){
    //         $scope.filters[index].value = obj.Name;
    //     }
    // }

    $rootScope.isProcessing     = false;

    /**
     * capitalize - Make First Letter Camel
     * @param  {String} input
     * @return {String}      
     */
    function capitalize(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
    }

    /**
     * deCapitalize - Make First Letter Small
     * @param  {String} input
     * @return {String}      
     */
    function deCapitalize(input) {
        return (!!input) ? input.charAt(0).toLowerCase() + input.substr(1) : '';
    }

    /**
     * exist - check if the el is undefined and add default value if it is
     * @param  {String|Object|false} val       - el for check
     * @param  {String|Object|false} changeTo  - default value
     * @return {String|Object|false}
     */
    // function exist(val, changeTo){
    //     return ((val !== undefined) ? val : (changeTo !== undefined ? changeTo : false) ); 
    // }

    $scope.prepareObjects = function (definition) {
        angular.copy($scope.property, definition.Items["Property"]);
        angular.copy($scope.comparer, definition.Items["Comparer"]);
        angular.copy($scope.inputTemplate, definition.Items["InputTemplate"]);
    }

    /**
     * modifyList - Modify Data from the Server 
     */
    $scope.modifyList = function () {
        angular.forEach($scope.definitions, function (definition, key) {

            // Copy for each element
            $scope.prepareObjects(definition);
            
            // Selected
            var selectedProperty = {};

            function setSelected() {
                
                angular.forEach(definition.Items["Property"].DropDownValues, function (value, key) {
                    value.Value.Comparer      = capitalize(value.Value.Comparer);
                    value.Value.InputTemplate = capitalize(value.Value.InputTemplate);
                    
                    if (definition.ItemValues.Property.Value == value.Value.ThisValue) {
                        selectedProperty.vals = value.Value;
                        selectedProperty.itemNumber = key;
                    }
                });
            }

            setSelected();

            // Place current property
            definition.Items["Property"].Value = definition.ItemValues.Property.Value;

            // Place current 
            if (selectedProperty.hasOwnProperty("vals")) {
                var selComparer      = capitalize(selectedProperty.vals.Comparer),
                    selInputTemplate = capitalize(selectedProperty.vals.InputTemplate);

                definition.Items["Comparer"][selComparer].Value = deCapitalize(definition.ItemValues.Comparer.Value);
                definition.Items["InputTemplate"][selInputTemplate].Value = definition.ItemValues.InputTemplate.Value;
                // definition.Items["InputTemplate"][selInputTemplate].Value = deCapitalize(definition.ItemValues.InputTemplate.Value);
            }
        });

    }

    /*------------------------------------------------------------------
    [  Init All Configuration  ]
    */
    $scope.loadInitialData = function(){

        // Add processing: ajax... 
        $rootScope.isProcessing     = true;
        
        if(controller == "accountTypesConfiguration"){

            //----ACCOUNT TYPES
            
            // with data with id
            apiBackOffice.GetAccountType($scope.alertId).then(function (data) {
                handleResponse(data);
            })
        }else if(controller == "dashboardWidgetConfiguration"){

            //----DASHBOARD WIDGET CONFIGURATION
            apiBackOffice.GetDashboards().then(function(dashboards){
                $scope.dashboards = dashboards;

                // with id
                if(!isNaN($scope.alertId*1))
                    apiBackOffice.GetWidget($scope.alertId).then(function (data) {
                        handleResponse(data);
                    })
                // with object
                else{

                    var widgetTypeId = $scope.alertId.split("-")[1],
                        calculatorId = $scope.alertId.split("-")[0]
                    
                    // console.log($stateParams);
                    // console.log($stateParams.dashboardId);
                    // apiBackOffice.GetDashboards().then(function(dashboards){
                        // $scope.dashboards = dashboards;
                        // console.log(dashboards);
                        apiBackOffice.GetWidgetByConfigType(calculatorId+"/"+widgetTypeId).then(function (data) {
                            
                            handleResponse(data,$stateParams.dashboardId);
                        })
                        
                    // })
                }
            })
        }else{
            //----ALERTS
            
            // with id
            if(!isNaN($scope.alertId*1))
                apiBackOffice.GetAlert($scope.alertId).then(function (data) {
                    handleResponse(data);
                })
            // with object
            else
                apiBackOffice.GetAlertByConfigType($scope.alertId).then(function (data) {
                    handleResponse(data);
                })
        }

        function handleResponse(data,dashboardId) {
            // Stop processing: ajax... 
            $rootScope.isProcessing     = false;

            // Assign alert
            $scope.alert                = data;

            // Add Dashboard
            if(typeof(dashboardId) != 'undefined'){
                $scope.alert.ConfigurationDetails.DashboardId = parseInt(dashboardId);
            }

            // Assign all descrtucted data 
            $scope.configMeta           = helpers.exist(data.ConfigurationRules.Meta,{});

            $scope.configDefinitionsModels = [];

            // Fields
            $scope.fields = data;

            // Configuration Fields
            $scope.filterExpression                 = $scope.fields.ConfigurationRules.FilterExpression;
            $scope.filterExpression['errorMessage'] = false;
            $scope.configFields                     = $scope.fields.ConfigurationDetails.ConfigurationFields;

            // configFields with dynamic fields
            $scope.configFieldsDynamic = [];                 
            // configFields with static fields
            $scope.configFieldsStatic = [];

            angular.forEach($scope.configFields, function(v, k) {
                if(v.Name == 'IsEnabled' || v.Name == 'SendEmailTo'){
                    $scope.configFieldsStatic.push(v);
                }else{
                    $scope.configFieldsDynamic.push(v);
                }
            });

            // Configuration Rules
            $scope.property                  = $scope.fields.ConfigurationRules.Meta.Property;
            $scope.comparer                  = $scope.fields.ConfigurationRules.Meta.Comparer;
            $scope.inputTemplate             = $scope.fields.ConfigurationRules.Meta.InputTemplate;

            // Configuration Definitions
            $scope.definitions               = $scope.fields.ConfigurationRules.Definitions;

            $scope.definitionsModel          = $scope.fields.ConfigurationRules.DefinitionModel;
            $scope.monitoredObjectProperties = $scope.fields.MonitoredObjectProperties;


            // Modify All result data
            $scope.modifyList();
            
            $scope.mapFields = {};

            // Map Fields - bridge - with their templates
            angular.forEach($scope.configMeta.Property.DropDownValues, function(v, k) {
                if(v.Value){
                    $scope.mapFields[v.Value.ThisValue]               = v.Value;
                    $scope.mapFields[v.Value.ThisValue].Comparer      = capitalize(v.Value.Comparer);
                    $scope.mapFields[v.Value.ThisValue].InputTemplate = capitalize(v.Value.InputTemplate);
                }
            });

        }
    }

    if($scope.alertId){
        $scope.loadInitialData();
    }else{

        if(controller == "accountTypesConfiguration")
            $state.go("management.accountTypes");    
        else if(controller == "dashboardWidgetConfiguration")
            $state.go("index.dashboard");    
        else
            $state.go("management.alerts");
    }

    /*------------------------------------------------------------------
    [  ROW RULES ]
    */
   
    // Add Row
    $scope.addRow = function(obj) {

        var newRow = new Object();

        angular.copy($scope.definitionsModel, newRow);

        $scope.prepareObjects(newRow);

        var index = ($scope.definitions.length);

        $scope.filters.push({
            'type' : 'number',
            'value': 0
        });

        // Push to all rows
        $scope.definitions.push(newRow);

        // Set Master Filter to first value
        $scope.definitions[index].Items.Property.Value = "";
    }

    /**
     * initChangeEmptyRow - init empty row 
     * @param  {String} value
     * @param  {Number} index
     * @param  {Array} values
     */
    $scope.initChangeEmptyRow = function(value,index,values){
        // If there is new row
        if(value == ""){
            // set first option
            $scope.definitions[index].Items.Property.Value = values[0].Value.ThisValue;
            // trigger change
            $scope.fieldsConfigOnChange($scope.definitions[index].Items.Property.Value, index);
        }
    }

    // Remove Row with Modal Prompt
    $scope.removeRow = function(index) {

        var modalInstance = $uibModal.open({
            templateUrl: 'views/modals/configuration/configDeleteRow.html',
            size: (!!typeof(size)?'md':size),
            controller: DeleteRowModalCtrl,
            resolve:{
                itemNumber: function () {
                    return index;
                },
                scope: function () {
                    return $scope;
                }
            }
        });
        /**
         *
         * @param $scope - it's the scope of the modal
         * @param $modalInstance - reference to modal
         * @param scope - reference to main scope
         * @param itemNumber - item number of the row that have to be removed
         * @constructor
         */
        function DeleteRowModalCtrl($scope, $uibModalInstance, scope, itemNumber) {
            $scope.ok = function () {
                scope.definitions.splice(itemNumber, 1);
                $uibModalInstance.close();
            }
            $scope.cancel = function () {
                $uibModalInstance.close();
            }
        }
    }

    /**
     * clearChanges 
     */
    $scope.clearChanges = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/modals/configuration/configClearChanges.html',
            controller: ModalCtrl,
            resolve: {
                scope: function () {
                    return $scope;
                }
            }
        });

        function ModalCtrl($scope, $uibModalInstance, scope) {

            $scope.ok = function () {
                scope.saveResponse = false;
                scope.loadInitialData();
                $uibModalInstance.close();
            }
            $scope.cancel = function () {
                $uibModalInstance.close();
            }
        }
    }

    /**
     * testFilter - Modal
     */
    // $scope.testFilter = function () {
    //     var modalInstance = $uibModal.open({
    //         size: (!!typeof(size)?'md':size),
    //         templateUrl: 'views/modals/configuration/configTestFilter.html',
    //         controller: ModalCtrl,
    //         resolve: {
    //             scope: function () {
    //                 return $scope;
    //             }
    //         }
    //     });

    //     function ModalCtrl($scope, $uibModalInstance, scope) {
            
    //         $scope.ok = function () {
    //             $uibModalInstance.close();
    //         }
    //         $scope.cancel = function () {
    //             $uibModalInstance.close();
    //         }

    //         /**
    //          * fieldTypeMonitored - Switch required component: SELECT|TEXT
    //          * @param  {Object} obj
    //          */
    //         $scope.fieldTypeMonitored = function(obj) {
    //             return ((!!obj) ?'select':'text');
    //         }

    //         $scope.data = {};
    //         angular.copy(scope.fields, $scope.data);
    //     }
    // }

    /**
     * Validate
     */
    $scope.validateExpression = function (){
        
        // console.log($scope.definitions);
        // console.log($scope.definitions.length);
        var payload = {
            "filtersCount"     : $scope.definitions.length,
            "filtersExpression": $scope.filterExpression.Value
        }
        // Add processing: ajax... 
        $rootScope.isProcessing     = true;

        apiBackOffice.ValidateExpression(payload).then(function (data) {

            // Stop processing: ajax... 
            $rootScope.isProcessing     = false;


            if(!data.Success){
                $scope.filterExpression.errorMessage = data.ErrorMessage;
            }
            else
                $scope.filterExpression.errorMessage = false;
        },function(){
            // Stop processing: ajax... 
            $rootScope.isProcessing     = false;                
        })
    }


    // Other Functions
    // ------------------------------------------------

    /**
     * fieldType collect and set appropriate input type
     * @param  {String}
     */
    $scope.fieldType = function(value,dropdown){

        if (dropdown)
            return 'select';

        var type = '';
        switch (typeof(value)){
            case 'number':
                type = 'number';
                break;
            case 'boolean':
                type = 'checkbox';
                break;
            default:
                type = 'text';
        }
        return type;
    }


    // SAVE
    // ------------------------------------------------
    $scope.saveConfiguration = function (form) {

        /*------------------------------------------------------------------
        [  COLLECT REQUIRED FIELDS AND OBJECTS  ]
        */

            // Get config Fields
        var configurationFields = JSON.parse(angular.toJson( $scope.configFieldsDynamic.concat($scope.configFieldsStatic) )),
            // Get Title
            title = $scope.alert.ConfigurationDetails.Title,
            // Get Monitor Filters
            definitions     = [],
            currentProperty = "",
            currentComparer = "",
            currentTemplate = "",
            definitions     = [],
            payload         = {};

            // Create definitions copy
            angular.copy($scope.definitions,definitions);
            // console.log(definitions);
        // Loop that copy and sync all changes
        angular.forEach(definitions, function (definition, key) {

            // Current value per row
            currentProperty = definition.Items.Property.Value;
            currentComparer = $scope.mapFields[definition.Items.Property.Value].Comparer;
            currentTemplate = $scope.mapFields[definition.Items.Property.Value].InputTemplate;

            // map that values to ItemValues
            definition.ItemValues.Property.Value      = currentProperty;                
            definition.ItemValues.InputTemplate.Value = definition.Items.InputTemplate[ currentTemplate ].Value;
            definition.ItemValues.Comparer.Value      = definition.Items.Comparer[ currentComparer ].Value;

            // Reset all Items like initial one
            definition.Items = $scope.alert['ConfigurationRules']['DefinitionModel']['Items'];

        });

        // Collect all changes and add it to the initial alert
        $scope.alert['ConfigurationDetails']['Title']               = title;
        $scope.alert['ConfigurationDetails']['ConfigurationFields'] = configurationFields;
        $scope.alert['ConfigurationRules']['Definitions']           = definitions;
        $scope.alert['ConfigurationRules']['FilterExpression']      = $scope.filterExpression;


        /*------------------------------------------------------------------
        [  SAVE  ]
        */

        // Create payload copy
        angular.copy($scope.alert,payload);

        // Add processing: ajax... 
        $rootScope.isProcessing     = true;

        // --- NEW ALERT OR WIDGET
        if(payload['ConfigurationDetails']['ConfigurationId'] == 0){


            // Dashboard Widget Configuration
            if(controller == "dashboardWidgetConfiguration"){

                apiBackOffice.AddWidget(payload).then(function (data) {

                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false;
                    
                    if(!data.Success){
                        $scope.saveResponse = data.ErrorMessage;
                    }
                    else{
                        $scope.saveResponse = false;
                        $state.go("index.dashboard",{"id":payload.ConfigurationDetails.DashboardId});
                    }
                        
                },function(){
                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false; 
                    $scope.saveResponse         = false;               
                })

            }else{
                apiBackOffice.AddAlert(payload).then(function (data) {

                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false;
                    
                    if(!data.Success){
                        $scope.saveResponse = data.ErrorMessage;
                    }
                    else{
                        $scope.saveResponse = false;
                        $state.go("management.alerts");
                    }
                        
                },function(){
                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false; 
                    $scope.saveResponse         = false;               
                })
            }
        }

        //--- UPDATE
        else{
            // Account types
            if(controller == "accountTypesConfiguration"){

                apiBackOffice.UpdateAccountTypes(payload).then(function (data) {

                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false;
                    
                    // If there is an error - get the error
                    if(!data.Success){
                        $scope.saveResponse = data.ErrorMessage;
                    }
                    // 
                    else{
                        $scope.saveResponse = false;
                        $state.go("management.accountTypes");
                    }
                },function(){
                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false; 
                    $scope.saveResponse         = false;               
                })
            }
            // Dashboard Widget Configuration
            else if(controller == "dashboardWidgetConfiguration"){

                apiBackOffice.UpdateWidget(payload).then(function (data) {

                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false;
                    
                    // If there is an error - get the error
                    if(!data.Success){
                        $scope.saveResponse = data.ErrorMessage;
                    }
                    // 
                    else{
                        $scope.saveResponse = false;
                        // $state.go("index.dashboard");
                        // console.log(payload);
                        $state.go("index.dashboard",{"id":payload.ConfigurationDetails.DashboardId});
                    }
                },function(){
                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false; 
                    $scope.saveResponse         = false;               
                })
            }
            // Alerts
            else{

                apiBackOffice.UpdateAlert(payload).then(function (data) {

                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false;
                    
                    // If there is an error - get the error
                    if(!data.Success){
                        $scope.saveResponse = data.ErrorMessage;
                    }
                    // 
                    else{
                        $scope.saveResponse = false;
                        $state.go("management.alerts");
                    }
                },function(){
                    // Stop processing: ajax... 
                    $rootScope.isProcessing     = false; 
                    $scope.saveResponse         = false;               
                })
            }
        }

    }

}