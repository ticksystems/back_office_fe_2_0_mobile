/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
var Settings = {
    CurrentLanguage     : 'en',
    DomainRequestName   : '',
    Domain              : '',
    UserToken           : "",
    UserTokenBckup      : "",
    AppVersion          : '4.0',
    InstanceID          : "",
    Caller              : "",
    ResultSearchRows    : 50,
    ExpireTime          : 60,
    SystemData          : (new UAParser()).getResult(),
    IP                  : {}
};

var ENV = {
    "prod"  : true,
    "mobile": true,
    "ga"    : true
}

Settings.DomainRequestName = ENV.prod ? 
    'https://rics.app' : 
    'http://18.130.134.16:80';

/**
 * SystemData
 * @type {Object}
 */
var SystemData = {
    Translations : {},
    language        : "en-US",
    utcOffsetSeconds: -3600,
    Auth         : {
        "management": {
            // "features": {
                "users": {
                    "auth": true
                },
                "symbols": {
                    "auth": true
                },
                "globalFilters": {
                    "auth": true
                },
                "configurationHistory": {
                    "auth": false
                },
            // },
            "auth": true
        },
        "index": {
            "dashboard": {
                "auth": true
            },
            "auth": true
        },
        "alerts": {
            // "features": {
                "active": {
                    "auth": true
                },
                "history": {
                    "auth": false
                },
            // },
            "auth": true
        }
    },
    InvocationIdLastGUID : ""
}

/*------------------------------------------------------------------
[  GET IP ]
*/
// $.getJSON('https://api.ipify.org?format=json', function(data) {
var geoApi = ENV.prod ? 
    "https://ssl.geoplugin.net/json.gp?k=5f0c671654adf68d":
    "http://www.geoplugin.net/json.gp?jsoncallback=?";

//$.getJSON( geoApi , function(data) {
//    Settings.IP = data;
//});


/*------------------------------------------------------------------
[  ATTEMPTED URL VARIABLES   ]
*/
inspinia.value('redirectToUrlAfterLogin', { url: '/' });
inspinia.value('homePath', { url: '/index/main' });


function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider,CacheFactoryProvider,$locationProvider) {

    // $locationProvider.html5Mode(true);

    angular.extend(CacheFactoryProvider.defaults, { maxAge: 15 * 60 * 1000 });

    // $urlRouterProvider.otherwise("/index/main");
    $urlRouterProvider.otherwise("/index/login");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        // debug: true,
        // events: true,
        // Set modules initially
        'modules': [{ 
            // Load custom modules
            name : 'dashboardModule',
            files: [                        
                    'js/plugins/gridstack-angular/gridstack.js',
                    'js/plugins/gridstack-angular/gridstack-angular.js',
                    'js/controllers/management/configuration.js',
                    'js/controllers/dashboard/dashboard.js'
                    ]
        },{ 
            // Load custom modules
            name : 'dashboardMobile',
            files: [

                    'js/plugins/gridstack-angular/gridstack.js',
                    'js/plugins/gridstack-angular/gridstack-angular.js',
                    // 'js/controllers/management/configuration.js',
                    'js/controllers/dashboard/dashboardMobile.js'
                    ]
        },{ 
            // Load custom modules
            name : 'loginModule',
            files: ['js/controllers/login/login.js']
        },{ 
            // Load custom modules
            name : 'clientProfilesModule',
            files: ['js/controllers/clientProfiles/clientProfiles.js']
        },{ 
            // Load custom modules
            name : 'registerModule',
            files: ['js/controllers/register/register.js']
        }
        ,{ 
            // Load custom modules
            name : 'changePasswordModule',
            files: ['js/controllers/changePassword/changePassword.js']
        },{ 

            // Load custom modules
            name : 'alertsModule',
            files: ['js/controllers/alerts/alerts.js']
        },{ 
            // Load custom modules
            name : 'managementModule',
            files: [
                    'js/controllers/management/configuration.js',
                    'js/controllers/management/management.js'//,
                    // 'js/controllers/management/configuration.js'
                    ]
        }]
    });

    

    $stateProvider
        .state('index', {
            abstract: true,
            url: "/index",
            data: { pageTitle: '', abstractFrom: false },
            templateUrl: "views/common/content.html"
        })
        .state('index.main', {
            url: "/main",
            templateUrl: "views/main.html",
            data: { pageTitle: 'Welcome to Rics' , abstractFrom: "index", permissions: []}//,
            // resolve : function(){
            //     var sUrl = this.self.url.replace("/","");
            //     this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;
            // }
        })
        .state('index.minor', {
            url: "/minor",
            templateUrl: "views/minor.html",
            data: { pageTitle: 'Example view' , abstractFrom: "index", permissions: [] }
        })
        .state('index.login', {
            url: "/login",
            templateUrl: "views/login.html",
            data: { pageTitle: 'Login', specialClass: 'gray-bg' , abstractFrom: "index", permissions: []},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // var sUrl = this.self.url.repllace("/","");
                    this.data.auth = true;//SystemData.Auth[this.data.abstractFrom][sUrl].auth;

                    $ocLazyLoad.load([]);
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['loginModule']); 
                }]
            }
        })
        .state('index.register', {
            url: "/register",
            templateUrl: "views/register.html",
            data: { pageTitle: 'Register', specialClass: 'gray-bg' , abstractFrom: "index", permissions: []},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // var sUrl = this.self.url.replace("/","");
                    // this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;

                    $ocLazyLoad.load([]);
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['registerModule']); 
                }]
            }
        })        
        .state('index.changePassword', {
            url: "/changePassword",
            templateUrl: "views/changePassword.html",
            data: { pageTitle: 'Change Password', specialClass: 'gray-bg' , abstractFrom: "index", permissions: ["change_password"]},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    
                    // console.log(this.data);

                    // var sUrl = this.self.url.replace("/","");
                    // this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;

                    // $ocLazyLoad.load([]);
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['changePasswordModule']); 
                }]
            }
        })
        .state('index.clientProfiles', {
            url: "/clientProfiles/:platform/:client",
            params: {
                platform : {squash: true, value: null},
                client   : {squash: true, value: null}
            },
            templateUrl: "views/clientProfiles.html",
            data: { pageTitle: 'Client Profiles', specialClass: 'gray-bg' , abstractFrom: "index", permissions: []},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    // Some charts
                    return $ocLazyLoad.load([
                        {
                            name: 'angular-flatpickr',
                            files: [
                                'css/plugins/flatpickr/flatpickr.css',
                                'css/plugins/flatpickr/material_green.css',
                                'js/plugins/angular-flatpickr/flatpickr.js',
                                'js/plugins/angular-flatpickr/dist/ng-flatpickr-comp.min.js'
                            ]
                        },
                        {
                            serie: true,
                            name: 'highcharts-ng',
                            files: ['js/plugins/highcharts/highstock.js'/*,'js/plugins/highcharts/highcharts-3d.js'*/,'js/plugins/highcharts/highcharts-ng.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },                        
                        {
                            files: ['js/plugins/exportToXLSX/xlsx.full.min.js']
                        }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {

                    $ocLazyLoad.load([]);
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['clientProfilesModule']); 
                }]
            }
        }) 
        .state('index.dashboard', {
            url: "/dashboard/:id",
            params: {
                id  : {squash: true, value: null}
            },
            templateUrl: "views/dashboard.html",
            data: { pageTitle: 'Dashboard', abstractFrom: "index", permissions: ["view_risk_dashboards","edit_risk_dashboards_configuration"] },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    // Some charts
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/SignalR/jquery.signalr-2.2.2.min.js',Settings.DomainRequestName+'/signalr/hubs']
                        },
                        {
                            serie: true,
                            name: 'highcharts-ng',
                            files: ['js/plugins/highcharts/highstock.js','js/plugins/highcharts/highcharts-ng.js']
                        }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // var sUrl = this.self.url.replace("/","");
                    //     console.log("koeeeeeeeeeeeeeeeeeeeee");
                    //     console.log(this.data.abstractFrom);
                        // this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;
                    $ocLazyLoad.load([
                        // {  
                        //     serie: true,
                        //     files: ['js/plugins/gridstack-angular/gridstack.js']
                        // },                        
                        // {
                        //     serie: true,
                        //     name: 'gridstack-angular',
                        //     files: ['js/plugins/gridstack-angular/gridstack-angular.js']
                        // }
                    ]);

                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['dashboardModule']); 
                }]
            }
        })

            .state('index.dashboardWidgetConfiguration', {
                url: "/dashboardWidgetConfiguration/:id/:dashboardId",
                params: {
                    id         : {squash: true, value: null},
                    dashboardId: {squash: true, value: null}
                },
                templateUrl: "views/dashboardWidgetConfiguration.html",
                data: { pageTitle: 'Dashboard Widget Configuration', abstractFrom: "index", permissions: []},
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                        // Resolve promise and load before view
                        return $ocLazyLoad.load(['dashboardModule']); 
                    }]
                }
            })

        .state('index.dashboardMobile', {
            url: "/dashboardMobile/:id",
            params: {
                id  : {squash: true, value: null}
            },
            templateUrl: "views/dashboardMobile.html",
            data: { pageTitle: 'Dashboard Mobile', abstractFrom: "index", permissions: ["view_risk_dashboards","edit_risk_dashboards_configuration"] },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    // Some charts
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/SignalR/jquery.signalr-2.2.2.min.js',Settings.DomainRequestName+'/signalr/hubs']
                        },
                        {
                            serie: true,
                            name: 'highcharts-ng',
                            files: ['js/plugins/highcharts/highstock.js','js/plugins/highcharts/highcharts-ng.js']
                        }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // var sUrl = this.self.url.replace("/","");
                    //     console.log("koeeeeeeeeeeeeeeeeeeeee");
                    //     console.log(this.data.abstractFrom);
                        // this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;
                    $ocLazyLoad.load([
                        // {  
                        //     serie: true,
                        //     files: ['js/plugins/gridstack-angular/gridstack.js']
                        // },                        
                        // {
                        //     serie: true,
                        //     name: 'gridstack-angular',
                        //     files: ['js/plugins/gridstack-angular/gridstack-angular.js']
                        // }
                    ]);

                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['dashboardMobile']); 
                }]
            }
        })

        .state('management', {
            abstract: true,
            url: "/management",
            data: { pageTitle: '', abstractFrom: false},
            templateUrl: "views/common/content.html"
        })
        .state('management.users', {
            url: "/users",
            templateUrl: "views/users.html",
            data: { pageTitle: 'Users', abstractFrom: "management", permissions: []},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        // Chart and iCheck
                        {
                            name: 'angular-peity',
                            files: ['js/plugins/peity/jquery.peity.min.js', 'js/plugins/peity/angular-peity.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css','js/plugins/iCheck/icheck.min.js']
                        },
                        // Datatables
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // var sUrl = this.self.url.replace("/","");
                    //     console.log(this.data.abstractFrom);
                        // this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;
                    // $ocLazyLoad.load([
                    //     {  
                    //         serie: true,
                    //         files: ['js/plugins/gridstack-angular/gridstack.js']
                    //     },                        
                    //     {
                    //         serie: true,
                    //         name: 'gridstack-angular',
                    //         files: ['js/plugins/gridstack-angular/gridstack-angular.js']
                    //     }
                    // ]);
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['managementModule']); 
                }]
            }
        })
        .state('management.symbols', {
            url: "/symbols",
            templateUrl: "views/symbols.html",
            data: { pageTitle: 'Symbols', abstractFrom: "management", permissions: []},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        // Datatables
                        // {
                        //     serie: true,
                        //     files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        // },
                        // {
                        //     serie: true,
                        //     name: 'datatables',
                        //     files: ['js/plugins/dataTables/angular-datatables.min.js']
                        // },
                        // {
                        //     serie: true,
                        //     name: 'datatables.buttons',
                        //     files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        // }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['managementModule']); 
                }]
            }
        })
        .state('management.alerts', {
            url: "/alerts",
            templateUrl: "views/monitor.html",
            data: { pageTitle: 'Alerts', abstractFrom: "management", permissions: []},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['managementModule']); 
                }]
            }
        })
            .state('management.alertConfiguration', {
                url: "/alertConfiguration/:id",
                templateUrl: "views/monitorConfiguration.html",
                data: { pageTitle: 'Alert Configuration', abstractFrom: "management", permissions: []},
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                        // Resolve promise and load before view
                        return $ocLazyLoad.load(['managementModule']); 
                    }]
                }
            })

        .state('management.accountTypes', {
            url: "/accountTypes",
            templateUrl: "views/accountTypes.html",
            data: { pageTitle: 'Account Types', abstractFrom: "management", permissions: []},
            resolve: {
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['managementModule']); 
                }]
            }
        })
            .state('management.accountTypesConfiguration', {
                url: "/accountTypesConfiguration/:id",
                templateUrl: "views/accountTypesConfiguration.html",
                data: { pageTitle: 'Account Types Configuration', abstractFrom: "management", permissions: []},
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                        // Resolve promise and load before view
                        return $ocLazyLoad.load(['managementModule']); 
                    }]
                }
            })

        .state('alerts', {
            abstract: true,
            url: "/alerts",
            data: { pageTitle: '', abstractFrom: false},
            templateUrl: "views/common/content.html"
        })
        .state('alerts.active', {
            url: "/active",
            templateUrl: "views/alerts.html",
            data: { pageTitle: 'Active Alerts', abstractFrom: "alerts", permissions: ["view_alerts","edit_alerts_configuration"]},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        // Chart and iCheck
                        {
                            name: 'angular-peity',
                            files: ['js/plugins/peity/jquery.peity.min.js', 'js/plugins/peity/angular-peity.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css','js/plugins/iCheck/icheck.min.js']
                        },
                        // Datatables
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                    var sUrl = this.self.url.replace("/","");
                    //     this.data.auth = SystemData.Auth[this.data.abstractFrom][sUrl].auth;
                    $ocLazyLoad.load([]);
                    // Resolve promise and load before view
                    return $ocLazyLoad.load(['alertsModule']); 
                }]
            }
        })

        

}

/*------------------------------------------------------------------
[  AG-GRI  ]
*/
// GET AG-GRID TO CREATE AN ANGULAR MODULE AND REGISTER THE AG-GRID DIRECTIVE
agGrid.initialiseAgGridWithAngular1(angular);


/*------------------------------------------------------------------
 [  ASSIGN CONFIGURATION  ]
 */
var CordovaInit = function() {
    
    var html = document.getElementById("inspinia");
    
    var onDeviceReady = function() {
        receivedEvent('deviceready');
        
        // Get offsetTime
        navigator.globalization.getDatePattern(
            function (date) { SystemData.utcOffsetSeconds = date.utc_offset; },
            function () { return false; }, 
            { formatLength: 'short', selector: 'date and time' }
       );
        // Get language
        navigator.globalization.getPreferredLanguage(
            function (language) { SystemData.language = language.value;},
            function () { return false;}
        );
        
        setTimeout(function() {
           navigator.splashscreen.hide();
        }, 200);
    };
    
    var receivedEvent = function(event) {

        // Clear all badge
        cordova.plugins.notification.badge.set(0);
        
        console.log('Start event received, bootstrapping application setup.');
//        alert('Device Name: '     + device.name     + '<br />' +
//              'Device Cordova: '  + device.cordova  + '<br />' +
//              'Device Platform: ' + device.platform + '<br />' +
//              'Device UUID: '     + device.uuid     + '<br />' +
//              'Device Version: '  + device.version  + '<br />')
        if(event != "manual"){
            Settings.DeviceID = device.uuid || "aaa123456abc11";
            Settings.Platform = (device.platform == "iOS" ? "iOS" : "other");
        }
        else{
            Settings.DeviceID = "aaa123456abc11";
            Settings.Platform = "other";
        }
        angular.bootstrap(html, ['inspinia']);
        
                // Test with ajax
    //             $.getJSON( geoApi , function(data) {
    //             //	let triggerDate = moment().add(1, "minute").seconds(0).milliseconds(0);
					cordova.plugins.notification.local.schedule({
    					id: 1,
    					title: "My Title",
                        text: "test notif every 2 minutes",
    					text: function (){
                            return "aaaaaa";
                        },
                        badge: 1,
    					trigger: {
                        	every: 1,
    						every: 'minute',
                        //	unit: "minute",
                            // count: 5,
                        }
				    });
									
     //            cordova.plugins.notification.local.schedule({
     //                title: data.geoplugin_request,
     //                // title: '$ 5,323.12',
     //                text: 'Your minute Pnl ',
     //                foreground: true,
     //                // data : data,
					// badge: 1,
     //                // led: ff0000,
     //                // trigger: { at: new Date(2018, 10, 27, 15) }
     //                // trigger: { in: 5, unit: 'second' },
     //                every : parseFloat((1 / 60))
     //            });
				
				// cordova.plugins.notification.local.schedule({
    // 				title: 'Justin Rhyss',
    // 				text: 'Do you want to go see a movie tonight?',
    //				actions: [{
    //					id: 'reply',
    //					type: 'input',
    //					title: 'Reply',
    //					emptyText: 'Type message'
    //					} ]
    // 				actions: [
    // 					   { id: 'yes', title: 'Yes' },
    // 					   { id: 'no',  title: 'No' }
    // 					]
				// });

//        cordova.plugins.notification.local.schedule({
//
//        title: '$ 5,323.12',
//        text: 'Your Pnl for Yeasterday',
//        foreground: true,
//
//        badge: 1,
//
//        });
				
        // On notification click - clear badges
        cordova.plugins.notification.local.on("click", function(notification) {
            // alert("clicked: " + notification.id);
            cordova.plugins.notification.badge.set(0);
            // cordova.plugins.notification.badge.decrease(1, function (badge) {
                // badge is now 9 (11 - 2)
            // });
        });

        
    };
    
    this.bindEvents = function() {
        document.addEventListener('deviceready', onDeviceReady, false);
    };
    
    //If cordova is present, wait for it to initialize, otherwise just try to
    //bootstrap the application.
    if (window.cordova !== undefined) {
        console.log('Cordova found, wating for device.');
        this.bindEvents();
    } else {
        console.log('Cordova not found, booting application');
        receivedEvent('manual')
    }
    
};


console.log('Bootstrapping!');
new CordovaInit();

angular
    .module('inspinia')
    .config(config)
