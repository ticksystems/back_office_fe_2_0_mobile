(function( ng, inspinia ){

    "use strict";

    inspinia.factory('apiBackOffice', [
        'api',
        function(api) {

        	return {

                /**
                 * GetUsers
                 * @param {Object} data - request params
                 */
                GetUsers : function(data) {
                    return api.Get("/api/users" + (data ? "/"+data : "") );
                },        		
                /**
                 * CreateUser
                 * @param {Object} data - request params
                 */
                CreateUser : function(username, password) {

                    var data = {
                        "password"  : password,
                        "UserName"  : username
                    };
                    return api.Post("/api/users", data);
                },
                /**
                 * PutUser
                 * @param {Object} data - request params
                 */
                PutUser : function(data) {
                    return api.Put("/api/users", data);
                },
                /**
                 * ChangePassword
                 * @param {Object} data - request params
                 */
                ChangePassword : function(id,oldPassword,newPassword) {
                    var data = "id="+id+"&currentpassword="+oldPassword+"&newpassword="+newPassword;
                    return api.Put("/api/passwords", data, "application/x-www-form-urlencoded",true);
                },


                /*------------------------------------------------------------------
                [  SYMBOLS  ]
                */
               
                /**
                 * GetSymbols
                 * @param {Object} data - request params
                 */
                GetSymbols : function() {
                    return api.Get("/api/TradingInstruments");
                },
                /**
                 * UpdateSymbol
                 * @param {Object} data - request params
                 */
                UpdateSymbol : function(data) {
                    return api.Put("/api/TradingInstruments",data);
                },

                /**
                 * GetGroups
                 * @param {Object} data - request params
                 */
                GetGroups : function() {
                    return api.Get("/api/TradingInstrumentsGroups");
                },

                /**
                 * AddGroup 
                 * @param {Object} data
                 */
                AddGroup : function(data) {
                    return api.Post("/api/TradingInstrumentsGroups", data);
                },

                /**
                 * UpdateGroup
                 * @param {Object} data - request params
                 */
                UpdateGroup : function(data) {
                    return api.Put("/api/TradingInstrumentsGroups", data);
                },

                /*------------------------------------------------------------------
                [  CONFIGURATION  ]
                */
               
                // ------- ALERTS

                /**
                 * GetAlert - Load alert configuration by id
                 * @param {Object} data - request params
                 */
                GetAlert : function(id) {
                    return api.Get("/api/Alerts/"+id);
                },

                /**
                 * GetAlertByConfigType - Load alert configuration by type
                 * @param {Object} data - request params
                 */
                GetAlertByConfigType : function(params) {
                    return api.Get("/api/ConfigurationDefinition/"+params);
                },

                /**
                 * GetAlerts - Get all alerts
                 * @param {Object} data - request params
                 */
                GetAlerts : function() {
                    return api.Get("/api/Alerts");
                },

                /**
                 * GetAllConfigurationTypes - Load configuration types for new alert 
                 */
                GetAllConfigurationTypes : function() {
                    return api.Get("/api/ConfigurationsTypes/alerts");
                },

                /**
                 * ValidateExpression - Validate Expression
                 */
                ValidateExpression : function(data) {
                    return api.Post("/api/FilterEpressionValidator",data);
                },

                /**
                 * AddAlert 
                 * @param {Object} data
                 */
                AddAlert : function(data) {
                    return api.Post("/api/Alerts", data);
                },

                /**
                 * UpdateAlert
                 * @param {Object} data - request params
                 */
                UpdateAlert : function(data) {
                    return api.Put("/api/Alerts", data);
                },

                // ------- ACCOUNT TYPES
                // 
                /**
                 * GetAccountTypes - Get all account types
                 * @param {Object} data - request params
                 */
                GetAccountTypes : function() {
                    return api.Get("/api/AccountTypes");
                },


                /**
                 * GetAccountType - Load Account Type configuration by id
                 * @param {Object} data - request params
                 */
                GetAccountType : function(id) {
                    return api.Get("/api/AccountTypes/"+id);
                },

                /**
                 * UpdateAccountTypes
                 * @param {Object} data - request params
                 */
                UpdateAccountTypes : function(data) {
                    return api.Put("/api/AccountTypes", data);
                },


                /*------------------------------------------------------------------
                [  DASHBOARD - WIDGETS ]
                */

                /**
                 * GetAvailableCalculators - Load available calculators
                 * @param {Object} data - request params
                 */
                GetAvailableCalculators : function() {
                    return api.Get("/api/ConfigurationsTypes/calculators");
                },

               
                /**
                 * GetAvailableWidgets - Load available widgets for new widget
                 * @param {Object} data - request params
                 */
                GetAvailableWidgets : function(id) {
                    return api.Get("/api/ConfigurationsTypes/"+id);
                },

                /**
                 * GetWidget - Load widget configuration by id
                 * @param {Object} data - request params
                 */
                GetWidget : function(id) {
                    return api.Get("/api/Widgets/"+id);
                },

                /**
                 * GetWidgets - Get all widgets
                 * @param {Object} data - request params
                 */
                GetWidgets : function() {
                    return api.Get("/api/WidgetsLayout");
                },

                /**
                 * UpdateAllWidgetsLayouts - Update all widget's layouts
                 * @param {Object} data    - request params
                 */
                UpdateAllWidgetsLayouts : function(data) {
                    return api.Put("/api/WidgetsLayout", data);
                },

                /**
                 * UpdateWidgetsLayout  - Update current widget's layout
                 * @param {Object} data - request params
                 */
                UpdateWidgetsLayout : function(dashId,data) {
                    return api.Put("/api/WidgetsLayout/"+dashId, data);
                },

                /**
                 * GetWidgetByConfigType - Load widget configuration by type
                 * @param {Object} data - request params
                 */
                GetWidgetByConfigType : function(params) {
                    return api.Get("/api/ConfigurationDefinition/"+params);
                },

                /**
                 * AddWidget 
                 * @param {Object} data
                 */
                AddWidget : function(data) {
                    return api.Post("/api/Widgets", data);
                },

                /**
                 * UpdateWidget
                 * @param {Object} data - request params
                 */
                UpdateWidget : function(data) {
                    return api.Put("/api/Widgets", data);
                },

                /**
                 * RemoveWidget - Remove widget by id
                 * @param {Object} data - request params
                 */
                // RemoveWidget : function(id) {
                //     return api.Delete("/api/Widgets",id);
                // }
                RemoveWidget : function(id) {
                    return api.Get("/api/Widgets/"+id,false,"DELETE");
                },

                /*------------------------------------------------------------------
                [  DASHBOARDS ]
                */
               
                /**
                 * GetDashboards - Get all dashboards
                 * @param {Object} data - request params
                 */
                GetDashboards : function() {
                    return api.Get("/api/Dashboards");
                },

                /**
                 * GetDashboardById - Get dashboard by id
                 * @param {Object} data - request id
                 */
                GetDashboardById : function(id) {
                    return api.Get("/api/Dashboards/"+id);
                },

                /**
                 * GetWidgetSnapshot - Get widget snapshot by widget id
                 * @param {Object} data - request id
                 */
                GetWidgetSnapshot : function(id) {
                    return api.GetZip("/api/Export/WidgetSnapshot/"+id);
                },

                /**
                 * AddDashboard
                 * @param {Object} data
                 */
                AddDashboard : function(data) {
                    return api.Post("/api/Dashboards", data);
                },

                /**
                 * UpdateDashboard
                 * @param {Object} data
                 */
                UpdateDashboard : function(data) {
                    return api.Put("/api/Dashboards", data);
                },

                /**
                 * RemoveDashboard - Remove Dashboard by id
                 * @param {Object} data - request params
                 */
                RemoveDashboard : function(id) {
                    return api.Get("/api/Dashboards/"+id,false,"DELETE");
                },

                /*------------------------------------------------------------------
                [  CLIENT PROFILES ]
                */

                /**
                 * GetAvailableClientLogins
                 * @param {String} query
                 */
                GetAvailableClientLogins : function(query) {
                    return api.Get("/api/clientprofiles/availablelogins/"+query);
                },
               
                /**
                 * GetBalanceOperations - Get Balance Operations by platformId and clientId
                 * @param {Object} data - platform id
                 * @param {Object} data - client id
                 */
                GetBalanceOperations : function(id,client) {
                    return api.Get("/api/clientprofiles/balanceops/"+id+"/"+client);
                },
                /**
                 * GetDeals - Get Deals by platformId and clientId
                 * @param {Object} data - platform id
                 * @param {Object} data - client id
                 */
                GetDeals : function(id,client) {
                    return api.Get("/api/clientprofiles/deals/"+id+"/"+client);
                },
                /**
                 * GetEquity - Get Equity by platformId and clientId
                 * @param {Object} data - platform id
                 * @param {Object} data - client id
                 */
                GetEquity : function(id,client) {
                    return api.Get("/api/clientprofiles/equity/"+id+"/"+client);
                },
                /**
                 * GetSwaps - Get Swaps by platformId and clientId
                 * @param {Object} data - platform id
                 * @param {Object} data - client id
                 */
                GetSwaps : function(id,client) {
                    return api.Get("/api/clientprofiles/swaps/"+id+"/"+client);
                },
                /**
                 * GetCommissions - Get Commissions by platformId and clientId
                 * @param {Object} data - platform id
                 * @param {Object} data - client id
                 */
                GetCommissions : function(id,client) {
                    return api.Get("/api/clientprofiles/commissions/"+id+"/"+client);
                },

                /*------------------------------------------------------------------
                [  GET IP  ]
                */
                GetIP : function(){
                    return api.GetAnonymous("http://www.geoplugin.net/json.gp?jsoncallback=?");
                },

                /*------------------------------------------------------------------
                [  USER TRACKING  ]
                */
               
                /**
                 * AddUserInfo
                 * @param {Object} data
                 */
                AddUserInfo : function(data) {
                    return api.Post("/api/UserStatistics/UserInfo", data);
                },
                /**
                 * AddUserEnterExit
                 * @param {Object} data
                 */
                AddUserEnterExit : function(data) {
                    return api.Post("/api/UserStatistics/UserEnterExit", data);
                },
                /**
                 * AddUserClick
                 * @param {Object} data
                 */
                AddUserClick : function(data) {
                    return api.Post("/api/UserStatistics/UserClick", data);
                }
                
                

        	}      

        }
    ]);

})( angular , inspinia )