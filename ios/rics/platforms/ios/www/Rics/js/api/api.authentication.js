(function( ng, inspinia ){

    "use strict";

    inspinia.factory('apiAuthentication', [
        '$q', 
        '$http', 
        '$location',
        '$rootScope',
        '$cookies',
        'redirectToUrlAfterLogin',
        // '$route',
        '$injector',
        // 'apiLogging',
        function($q, $http, $location, $rootScope,$cookies,redirectToUrlAfterLogin/*,$route*/,$injector/*,apiLogging*/) {

            /**
             * UserTokenRequestParams - some default get token settings
             * @type {Object}
             */
            var UserTokenRequestParams = {
                "username": "",
                "password": "",
                "grant_type": ""
            }

            /**
             * ExpireTokenRequestParams - object for expire token
             * @type {Object}
             */
            var ExpireTokenRequestParams = {
                "userToken": ""
            }

            /**
             * TokenValidateRequestParams - object for validate token
             * @type {Object}
             */
            var TokenValidateRequestParams = {
                "userToken" : ""
            }

            return {
                /**
                 * Login - Get Token
                 * @param {Object} email    - Username
                 * @param {Object} password - Password
                 */
                Login: function ( email, password ) {

                    var deferred = $q.defer(),
                        serviceName = '/token',
                        url = Settings.DomainRequestName + serviceName;

                    $rootScope.isWorking = true;

                    // Assign values to the parameters
                    UserTokenRequestParams.username = email;                    
                    UserTokenRequestParams.password   = password;
                    UserTokenRequestParams.grant_type = "password";

                    $rootScope.isWorkingForce = true;

                    $http({ 
                            method  : 'POST',
                            url     : url,
                            data    : $.param(UserTokenRequestParams),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}    
                        })
                        .success(function(result, status, headers, config) {
                            deferred.resolve(result);
                        })
                        .error(function(result, status, headers, config) {
                            deferred.reject(status);
                        });

                    return deferred.promise;
                },
                
                /**
                 * getSHA1 - Secure Hash
                 * @param  {String} string - String for 
                 * @return {String} hash
                 */
                getSHA1: function (string) {
                    var hash = Sha1.hash(string, true).toUpperCase();
                    return hash;
                },

                /**
                 * Logout
                 */
                Logout: function () {
                    $rootScope.isWorking        = true;
                    $rootScope.isWorkingForce   = true;
                    var  self                   = this;
                    
                    this.ExpireToken().then(function(response){
                        $rootScope.isWorking        = false;
                        $rootScope.isWorkingForce   = false;
                        self.SetUnLoggedState({"withRedirect":true,"hardRefresh":true});
                    });
                },
                /**
                 * SaveAttemptUrl - save last attempted url
                 */
                SaveAttemptUrl: function() {

                    if($location.path().toLowerCase() != '/login') {
                        redirectToUrlAfterLogin.url = $location.path();
                    }
                    else
                        redirectToUrlAfterLogin.url = '/';
                },
                
                /**
                 * RedirectToAttemptedUrl - redirect to given url
                 */
                RedirectToAttemptedUrl: function() {
                    $location.path(redirectToUrlAfterLogin.url);
                },

                /**
                 * ValidateToken 
                 */
                ValidateToken : function(overlay) {
                    
                    var overlay = overlay || false;

                    var getUserToken = $cookies.get("UserToken") || Settings.UserToken;
                    
                    if(getUserToken == undefined){
                        $location.path('/login');
                    }

                    // var apiAuthorization = $injector.get('apiAuthorization');
                    
                    // TODO: Load-Reload authorization rules
                    // apiAuthorization.init();

                    var deferred = $q.defer(),
                        serviceName = '/validateToken';

                    TokenValidateRequestParams.userToken    = getUserToken;

                    var url    = Settings.DomainRequestName + serviceName,
                        params = {};
                        params = JSON.stringify(TokenValidateRequestParams);

                    // Show overlay
                    if(!overlay){
                        $rootScope.isWorking = false;
                        $rootScope.isWorkingForce = true;
                    }

                    // $http({ 
                    //         method  : 'POST',
                    //         url     : url,
                    //         data    : params
                    //     })
                    //     .success(function(result, status, headers, config) {
                    //         deferred.resolve(result);
                    //         if(!overlay)
                    //             $rootScope.isWorkingForce = false;
                    //         Settings.UserToken = getUserToken;
                    //     })
                    //     .error(function(result, status, headers, config) {
                    //         delete Settings.UserToken;
                    //         deferred.reject(status);
                    //         if(!overlay)
                    //             $rootScope.isWorkingForce = false;
                    //     });


                    $http({ method: 'GET', url : 'text/languages.js' })
                        .success(function (result, status, headers, config) {
                            deferred.resolve(result);
                            $rootScope.isWorking = false;
                        }).
                        error(function (result, status, headers, config) {
                            deferred.reject(status);
                            handleError(result, status, headers, config);
                            $rootScope.isWorking = false;
                        });

                    return deferred.promise;
                },
                
                /**
                 * ExpireToken
                 */
                ExpireToken : function() {

                    var getUserToken = $cookies.get("UserToken") || Settings.UserToken;

                    if(getUserToken == undefined){ $location.path('/login'); }

                    var deferred = $q.defer(),
                        serviceName = '/expireToken';

                    ExpireTokenRequestParams.userToken  = getUserToken;

                    var url    = Settings.DomainRequestName + serviceName,
                        params = {};
                        params = JSON.stringify(UserTokenRequestParams);

                    $rootScope.isWorking = true;
                    $rootScope.isWorkingForce = true;

                    // $http({ 
                    //         method  : 'POST',
                    //         url     : url,
                    //         data    : params
                    //     })
                    //     .success(function(result, status, headers, config) {
                    //         deferred.resolve(result);
                    //     })
                    //     .error(function(result, status, headers, config) {
                    //         deferred.reject(status);
                    //     });
                    // return deferred.promise;

                    $http({ method: 'GET', url : 'text/languages.js' })
                        .success(function (result, status, headers, config) {
                            deferred.resolve(result);
                            $rootScope.isWorking = false;
                        }).
                        error(function (result, status, headers, config) {
                            deferred.reject(status);
                            handleError(result, status, headers, config);
                            $rootScope.isWorking = false;
                        });

                    return deferred.promise;
                },
                /**
                 * SetUnLoggedState 
                 * @param {Object} options [description]
                 */
                SetUnLoggedState : function(options) {

                    var $window     = $injector.get('$window'),
                        apiAlerts   = $injector.get('apiAlerts');

                    // Remove token
                    Settings.UserToken = "";

                    // Remove the cookies
                    $cookies.remove('UserToken');
                    $cookies.remove('TokenType');
                    $cookies.remove('UserRigths');
                    $cookies.remove('User');

                    if(options.removeParams)
                        $location.$$search = {};

                    // Remove all alerts
                    apiAlerts.removeAlert();

                    // With redirect
                    if(options.withRedirect){

                        // Should we use hard refresh
                        if(options.hardRefresh)
                            $window.location.href =  Settings.ProjectUrl || Settings.Domain;
                        // or not
                        else
                            $location.path("/index/login");            
                    }
                }

            };     

        }
    ]);

})( angular , inspinia )