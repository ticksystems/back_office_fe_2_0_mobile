(function( ng, inspinia ){

    "use strict";

    Settings || (Settings = {});
    
    inspinia.config(['$httpProvider', function ($httpProvider) {
        // TODO: Add here some additional config options
    }]);


    // inspinia.factory('api', function ($q, $http, $location, $rootScope, apiAuthentication, apiErrorStack, $window, /*apiLogging,*/ helpers,$cookies,apiAlerts) {

    //     $http.defaults.headers.post["Content-Type"] = 'application/json'; 

    //     var handleError = function(result, status, headers, config) {
        
    //         if (status == 400 || status == 401 || status == 403 || status == 404 || status == 500) {
    //             console.log(result);
    //             var message = "Something went wrong!";
    //             if(result && typeof(result.Message) != 'undefined')
    //                 message = result.Message;
    //             // config['status'] = status;
    //             // apiErrorStack.prepareError(config,'api');
    //             apiAlerts.addAlert({
    //                 "type"    : 'error',
    //                 "title"   : "Api Error",
    //                 "timeout" : 5000, 
    //                 "body"    : message ,
    //                 "toasterId" : "quick-toaster-container",
    //                 "clickHandler" : function(){
    //                     return true;
    //                 }
    //             });
                
    //         }

    //     };



    // app.value('version', '0.1')
    // .value('test', 'Test And little more')
    inspinia.factory('widgetSvc', ['$injector', '$q', '$rootScope', 'apiAlerts', function ($injector, $q, $rootScope, apiAlerts) {
        var proxy = null;
        return {
            initialize: function ($scope, method){//,widgets, method) {
                var defer = $q.defer(),
                    self = this;

                // console.log($scope);
                // if (authData) {
                    // connect(authData);
                    
                // }
                // else {
                //     $rootScope.$broadcast('sessionIsExpired', 'session is expired');
                // }

                // $.connection.hub.url = Settings.DomainRequestName + '/signalr/hubs';
                // $.connection.hub.qs = {'token': authData.token};
                // $cookies.get("UserToken") || Settings.UserToken
                // $.connection.hub.logging = false;
                var tryingToReconnect = false;

                $rootScope.widgetTotal = 9223372036854776000;

                var hub       = $.signalR.hub;
                    hub.url   = Settings.DomainRequestName + '/signalr/hubs';
                    // $.connection.hub.logging = true;
                    hub.logging = true;

                var states    = {
                        "1" : "connected",   
                        "0" : "connecting",  
                        "4" : "disconnected",
                        "2" : "reconnecting"
                    }

                    /*------------------------------------------------------------------
                    [  HOOKS  ]
                    */

                    hub.stateChanged(function (change) {
                        // console.log(states[change.newState]);
                        if(change.newState === $.signalR.connectionState.connected)
                            $scope.$broadcast('connectedToHub', {proxyObj: proxy, methodName: method});
                        // if (change.newState === $.signalR.connectionState.reconnecting) {
                        //     console.log('Re-connecting');
                        // }
                        // else if (change.newState === $.signalR.connectionState.connected) {
                        //     console.log('The server is online');
                        // }
                    });
                    // console.log($.signalR.connectionState);
                    // hub.connected(function () {
                    //     $rootScope.$broadcast('connectedToHub', {proxyObj: proxy, methodName: method});
                    // });
                    hub.reconnecting(function () {
                        $scope.$broadcast('reconnectingToHub', {proxyObj: proxy, methodName: method});
                        tryingToReconnect = true;
                    });

                    hub.reconnected(function () {
                        $scope.$broadcast('reconnectedToHub', {proxyObj: proxy, methodName: method});
                        tryingToReconnect = false;
                    });

                    hub.disconnected(function () {
                        if (tryingToReconnect) {
                            $scope.$broadcast('disconnectedFromHub', {proxyObj: proxy, methodName: method});
                            tryingToReconnect = false;
                        }
                    });

                    var riskProxy = hub.proxies["riskwidgetshub"];

                    // this.listenersForProxy($scope,riskProxy);
                    this.listenersForProxy($scope,riskProxy);
                    // riskProxy.on("widgetChanged", function (widgetData) {
                        // console.log("widgetChanged");
                        // console.log(widgetData);
                    // });

                function connect() {
                    // if ($.connection.hub.state === $.signalR.connectionState.disconnected || method.toLowerCase() !== proxy.hubName.toLowerCase()) {
                        // stopHub().then(function () {
                            // $.connection.hub.start().done(function () {
                            //     intializeHub();
                            // }).fail(function (error) {
                            //     $rootScope.$broadcast('connectionFailed', {error: error});
                            //     console.log('Invocation of start failed. Error: ' + error)
                            // });
                        // });
                    // }
                    // else {
                    //     if (proxy.server.unSubscribeForPage !== undefined) {
                    //         proxy.server.unSubscribeForPage("trades");
                    //         proxy.server.unSubscribeForPage("dashboard");
                    //     }
                    //     console.log(proxy)
                    //     defer.resolve(proxy);
                    // }
                    // 
                    
                    if ($.connection.hub.state === $.signalR.connectionState.disconnected){
                        // console.log("aaaa"); 
                        self.stopHub(hub).then(function() {
                            hub.start().done(function (proxy) {
                                defer.resolve(proxy);
                            // Subscribe for data
                            // proxy.proxies["riskwidgetshub"].server.subscribeForWidgetsData();

                            }).fail(function () {
                                deferred.reject();
                                console.log("Could not Connect!");
                            });    
                        })
                        
                    }

                }



                /*------------------------------------------------------------------
                [  INIT  ]
                */
                /**
                 * DOM Ready
                 */
                angular.element(function(){
                    connect();
                })

                return defer.promise;
            },
            stopHub: function(hub) {
                    
                var deferred = $q.defer();
                setTimeout(function () {
                    if (hub)
                        // $.connection.hub.stop();
                        hub.stop();

                    deferred.resolve(true);
                }, 0);
                return deferred.promise;
            },
            getProxy: function () {
                var deferred = $q.defer();
                setTimeout(function () {
                    if (proxy)
                        deferred.resolve(proxy);
                    else
                        deferred.reject();
                }, 0);
                return deferred.promise;
            },
            // listenersForProxy: function($scope,proxy) {
            listenersForProxy: function($scope,proxy) {

                // var widgets = $scope.widgets;

                proxy.on("widgetChanged", function(data){
                    
                    var v = data;

                    $scope.triggerWidgetChange(v);

                        // mapWidgetsObj
                            // $scope.$broadcast(mapWidgetsObj[widgets[v.WidgetId].view.ur], v);

                        // switch(widgets[v.WidgetId].view.url){
                        //     case '<wdatatable></wdatatable>':
                        //         $scope.$broadcast('wdatatableData', v);//, widgets);
                        //         break;
                        //     case '<aggregated></aggregated>':
                        //         $scope.$broadcast('aggregatedData', v);//, widgets);
                        //         break;
                        //     case '<barchart></barchart>':
                        //         $scope

                        //         var.$broadcast('barchartData', v);//, widgets);
                        //         break;
                        //     case '<hbarchart></hbarchart>':
                        //         $scope.$broadcast('hbarchartData', v);//, widgets);
                        //         break;
                        //     case '<linechart></linechart>':
                        //         $scope.$broadcast('linechartData', v);//, widgets);
                        //         break;
                        // }

                });

                // proxy.on("reinitWidget", function (widget) {
                //     // $route.reload();
                //     // console.log("DATA-Reinit");
                // });
                // proxy.on("widgetAdded", function (widget) {
                //     // $route.reload();
                //     // console.log("DATA-Added");
                // });

                // proxy.on("widgetRemoved", function (WidgetId) {
                //     // $route.reload();
                //     // console.log("DATA-Removed");
                // });
            }
        }
    }]);

    /**
     * WidgetsHubEvents - Angular hooks - services
     * @param  {[type]} 'WidgetsHubEvents'
     * @param  {[type]} ['$injector'      
     * @param  {[type]} '$q'              
     * @param  {[type]} '$rootScope'      
     */
    inspinia.factory('WidgetsHubEvents', ['$injector', '$q', '$rootScope', '$state', '$stateParams'/*, '$route'*/, function ($injector, $q, $rootScope/*, $route*/, $state, $stateParams) {
        var proxy     = null,
            apiAlerts = $injector.get('apiAlerts'),
            interval;
            // console.log("time");

            
        return {
            init: function ($scope) {
                $scope.$on("connectionFailed", function (event, args) {
                    console.log("connectionFailed");
                    // if ($scope.$parent) {
                    //     $scope.$parent.content.showInfoNotification("Error", args.error, "warning", 1000000);
                    //     $scope.$parent.content.removeContentLoader();
                    // }
                });
                $scope.$on("connectedToHub", function (event, args) {
                    clearInterval(interval);
                    // console.log("connectedToHub");
                    $scope.$evalAsync(function () {
                        apiAlerts.addAlert({
                            "type"           : 'zS',
                            "title"          : "Successfully connected!",
                            "body"           : "bind-hub-msg",
                            "bodyOutputType" : "directive",
                            "directiveData"  : { 'data': '' },
                            "toasterId"      : "quick-hub-messages",
                            "clickHandler"   : function(){
                                return true;
                            }
                        });
                    })
                }); 
                $scope.$on("reconnectingToHub", function (event, args) {
                    console.log("reconnectingToHub");
                    // console.log(apiAlerts);
                    $scope.$evalAsync(function () {
                        apiAlerts.addAlert({
                            "type"           : 'zW',
                            "title"          : "Information",
                            "body"           : "bind-hub-msg",
                            "bodyOutputType" : "directive",
                            "directiveData"  : { 'data': 'Connection error occured. Please wait for reconnecting.' },
                            "toasterId"      : "quick-hub-messages",
                            "clickHandler"   : function(){
                                return true;
                            }
                        });
                    })
                    // if ($scope.$parent)
                    //     $scope.$parent.content.showInfoNotification("Information", "Connection error occured. Please wait for reconnecting.", "shield", 10000);
                });

                $scope.$on("reconnectedToHub", function (event, args) {
                    console.log("reconnectedToHub");
                    clearInterval(interval);
                    $scope.$evalAsync(function () {
                        apiAlerts.addAlert({
                            "type"           : 'zS',
                            "title"          : "Reconnected",
                            "body"           : "bind-hub-msg",
                            "bodyOutputType" : "directive",
                            "directiveData"  : { 'data': '' },
                            "toasterId"      : "quick-hub-messages",
                            "clickHandler"   : function(){
                                return true;
                            }
                        }); 
                    })
                });

                $scope.$on("disconnectedFromHub", function (event, args) {
                    console.log("disconnectedFromHub");
                    
                    $scope.$evalAsync(function () {
                        apiAlerts.addAlert({
                            "type"           : 'zE',
                            "title"          : "Error",
                            "body"           : "bind-hub-msg",
                            "bodyOutputType" : "directive",
                            "directiveData"  : { 'data': 'Disconnected from server. Please wait for reinitialize.' },
                            "toasterId"      : "quick-hub-messages",
                            "clickHandler"   : function(){
                                return true;
                            }
                        });
                    })

                    interval = setInterval(function () {
                        $state.transitionTo($state.$current,$stateParams, {reload: true, inherit: true, notify: true});
                    }, 10000);
                    
                });
            }//,
            // off: function($scope){
            //     $rootScope.$on()
            //     clearInterval(interval);
            //     $rootScope.$off("connectedToHub");
            //     $rootScope.$off("connectedToHub");
            //     $rootScope.$off("reconnectingToHub");
            //     $rootScope.$off("reconnectedToHub");
            //     $rootScope.$off("disconnectedFromHub");
            // }

        }
    }]);


})( angular, inspinia );