(function( ng, inspinia ){

    "use strict";

    Settings || (Settings = {});
    
    inspinia.config(['$httpProvider', function ($httpProvider) {
        // TODO: Add here some additional config options
    }]);


    inspinia.factory('api', function ($q, $http, $location, $rootScope, apiAuthentication, apiErrorStack, $window,$sce, /*apiLogging,*/ helpers,$cookies,apiAlerts) {

        $http.defaults.headers.post["Content-Type"] = 'application/json'; 

        var handleError = function(result, status, headers, config) {
        
            if (status == 400 || status == 401 || status == 403 || status == 404 || status == 500) {
                console.log(result);
                var message = "Something went wrong!";
                if(result && typeof(result.Message) != 'undefined')
                    message = result.Message;
                // config['status'] = status;
                // apiErrorStack.prepareError(config,'api');
                apiAlerts.addAlert({
                    "type"    : 'error',
                    "title"   : "Api Error",
                    "timeout" : 5000, 
                    "body"    : message ,
                    "toasterId" : "quick-toaster-container",
                    "clickHandler" : function(){
                        return true;
                    }
                });
                
            }

        };
        
        return {

            /*------------------------------------------------------------------
            [  POST  ]
            */

            Post: function (url, data, method, overlay) {
                var deferred    = $q.defer(),
                    url         = Settings.DomainRequestName + url;
                var params      = {},
                    UserRigths  = JSON.parse($cookies.get("UserRigths") || "{}") || Settings.UserRigths;

                    data.Claims = UserRigths || [];
                    // params      = JSON.stringify(data);
                    params      = data;
                      
                $rootScope.isWorking = true;

                var getUserToken    = $cookies.get("UserToken") || Settings.UserToken,
                    geTokenType     = $cookies.get("TokenType") || Settings.TokenType,
                    
                    header          = {};

                header.Authorization = geTokenType +" "+ getUserToken;
                header['Content-Type'] = 'application/json';
                
                // console.log(header);
                // console.log(params);
                // Validate Token
                // apiAuthentication.ValidateToken(overlay)
                // .then(function (response){
                    
                    $rootScope.isWorking = false;

                    // If there is no error
                    // if(!response.hasError){

                        // If the token is valid and is defined
                        {
                            $rootScope.isWorking = true;

                            $http({ 
                                    method  : "POST", 
                                    url     : url,
                                    data    : params,
                                    headers : header
                                })
                                .success(function(result, status, headers, config) {
                                    // TODO: Set error for hasError
                                    deferred.resolve(result);
                                    $rootScope.isWorking = false;
                                })
                                .error(function(result, status, headers, config) {
                                    deferred.reject(status);
                                    handleError(result, status, headers, config);
                                    $rootScope.isWorking = false;
                                });

                            // $http({ method: 'GET', url : 'text/languages.js' })
                            //     .success(function (result, status, headers, config) {
                            //         deferred.resolve(result);
                            //         $rootScope.isWorking = false;
                            //     }).
                            //     error(function (result, status, headers, config) {
                            //         deferred.reject(status);
                            //         handleError(result, status, headers, config);
                            //         $rootScope.isWorking = false;
                            //     });


                        }

                        return deferred.promise;
                //     }
                //     else{
                        
                //         deferred.reject(false);
                //     }

                // })

                // return deferred.promise;
            },

            Put: function (url, data, contentType, specialInURL) {
                var deferred    = $q.defer(),
                    url         = Settings.DomainRequestName + url;
                var params      = {},
                    UserRigths  = JSON.parse($cookies.get("UserRigths")) || Settings.UserRigths;
                    // data.Claims = UserRigths || [];
                    params      = data;

                    if(specialInURL)
                        // url = url + "?"+encodeURIComponent(JSON.stringify(data));  
                        url = url + "?"+data;  
                    else
                        params      = data;
                $rootScope.isWorking = true;
                var getUserToken    = $cookies.get("UserToken") || Settings.UserToken,
                    geTokenType     = $cookies.get("TokenType") || Settings.TokenType,
                    header          = {};

                header.Authorization = geTokenType +" "+ getUserToken;
                if(!contentType)
                    header['Content-Type'] = 'application/json';
                else
                    header['Content-Type'] = contentType;
                
                // console.log(header);
                // console.log(params);
                // application/x-www-form-urlencoded
                // Validate Token
                // apiAuthentication.ValidateToken(overlay)
                // .then(function (response){
                    
                    $rootScope.isWorking = false;

                    // If there is no error
                    // if(!response.hasError){

                        // If the token is valid and is defined
                        {
                            $rootScope.isWorking = true;

                            $http({ 
                                    method  : "PUT", 
                                    url     : url,
                                    data    : params,
                                    headers : header
                                })
                                .success(function(result, status, headers, config) {
                                    // TODO: Set error for hasError
                                    deferred.resolve(result);
                                    $rootScope.isWorking = false;
                                })
                                .error(function(result, status, headers, config) {
                                    deferred.reject(status);
                                    handleError(result, status, headers, config);
                                    $rootScope.isWorking = false;
                                });

                            // $http({ method: 'GET', url : 'text/languages.js' })
                            //     .success(function (result, status, headers, config) {
                            //         deferred.resolve(result);
                            //         $rootScope.isWorking = false;
                            //     }).
                            //     error(function (result, status, headers, config) {
                            //         deferred.reject(status);
                            //         handleError(result, status, headers, config);
                            //         $rootScope.isWorking = false;
                            //     });


                        }

                        return deferred.promise;
                //     }
                //     else{
                        
                //         deferred.reject(false);
                //     }

                // })

                // return deferred.promise;
            },

            Delete: function (url, data, contentType, specialInURL) {
                var deferred    = $q.defer(),
                    url         = Settings.DomainRequestName + url;
                var params      = {},
                    UserRigths  = JSON.parse($cookies.get("UserRigths")) || Settings.UserRigths;
                    // data.Claims = UserRigths || [];
                    params      = data;

                    if(specialInURL)
                        // url = url + "?"+encodeURIComponent(JSON.stringify(data));  
                        url = url + "?"+data;  
                    else
                        params      = data;
                $rootScope.isWorking = true;
                var getUserToken    = $cookies.get("UserToken") || Settings.UserToken,
                    geTokenType     = $cookies.get("TokenType") || Settings.TokenType,
                    header          = {};

                header.Authorization = geTokenType +" "+ getUserToken;
                if(!contentType)
                    header['Content-Type'] = 'application/json';
                else
                    header['Content-Type'] = contentType;
                
                // console.log(header);
                // console.log(params);
                // application/x-www-form-urlencoded
                // Validate Token
                // apiAuthentication.ValidateToken(overlay)
                // .then(function (response){
                    
                    $rootScope.isWorking = false;

                    // If there is no error
                    // if(!response.hasError){

                        // If the token is valid and is defined
                        {
                            $rootScope.isWorking = true;

                            $http({ 
                                    method  : "DELETE", 
                                    url     : url,
                                    data    : params,
                                    headers : header
                                })
                                .success(function(result, status, headers, config) {
                                    // TODO: Set error for hasError
                                    deferred.resolve(result);
                                    $rootScope.isWorking = false;
                                })
                                .error(function(result, status, headers, config) {
                                    deferred.reject(status);
                                    handleError(result, status, headers, config);
                                    $rootScope.isWorking = false;
                                });

                            // $http({ method: 'GET', url : 'text/languages.js' })
                            //     .success(function (result, status, headers, config) {
                            //         deferred.resolve(result);
                            //         $rootScope.isWorking = false;
                            //     }).
                            //     error(function (result, status, headers, config) {
                            //         deferred.reject(status);
                            //         handleError(result, status, headers, config);
                            //         $rootScope.isWorking = false;
                            //     });


                        }

                        return deferred.promise;
                //     }
                //     else{
                        
                //         deferred.reject(false);
                //     }

                // })

                // return deferred.promise;
            },

            /*------------------------------------------------------------------
            [  getLanguages  ]
            */
            POSTtext : function() {
                // fol local only
                // var url = 'text/languages.js';
                // return this.PostAnonymous(url);

                var deferred = $q.defer();
                var url = url;

                $rootScope.isWorking = true;

                $http({ method: 'GET', url : 'text/languages.js' })
                    .success(function (result, status, headers, config) {
                        deferred.resolve(result);
                        $rootScope.isWorking = false;
                    }).
                    error(function (result, status, headers, config) {
                        deferred.reject(status);
                        handleError(result, status, headers, config);
                        $rootScope.isWorking = false;
                    });

                return deferred.promise;
            },

            PostAnonymous: function (url, data, reqheaders) {
                var deferred = $q.defer();
                var url = url;

                $rootScope.isWorking = true;

                $http({ method: 'POST', url : url , data: data, headers: reqheaders })
                    .success(function (result, status, headers, config) {
                        deferred.resolve(result);
                        $rootScope.isWorking = false;
                    }).
                    error(function (result, status, headers, config) {
                        deferred.reject(status);
                        handleError(result, status, headers, config);
                        $rootScope.isWorking = false;
                    });
                return deferred.promise;
            },

            Get: function (url, data, method,reqheaders) {
                var deferred           = $q.defer(),
                    url                = Settings.DomainRequestName + url;
                    if(!data)
                        data           = {};
                    var getUserToken   = $cookies.get("UserToken") || Settings.UserToken;
                    var geTokenType   = $cookies.get("TokenType") || Settings.TokenType;

                    data.Authorization = geTokenType +" "+ getUserToken;

                if(method === undefined){
                    method = "GET";
                    // Assemble the url
                    // url = url + "?request="+encodeURIComponent(JSON.stringify(data));                    
                }
                // console.log(data);
                // else{
                //     data = data;
                // }
                      
                $rootScope.isWorking = true;

                // Validate Token
                // apiAuthentication.ValidateToken()
                // .then(function (response){
                    
                    $rootScope.isWorking = false;

                    // If there is no error
                    // if(!response.hasError){

                        // If the token is valid and is defined
                        {
                            $rootScope.isWorking = true;

                            $http({ 
                                    method  : method, 
                                    url     : url,
                                    headers: data
                                    // params  : encodeURIComponent(data)
                                    // params  : data
                                })
                                .success(function(result, status, headers, config) {
                                    deferred.resolve(result);
                                    $rootScope.isWorking = false;
                                })
                                .error(function(result, status, headers, config) {
                                    deferred.reject(status);
                                    handleError(result, status, headers, config);
                                    $rootScope.isWorking = false;
                                });
                        }

                        // return deferred.promise;
                    // }
                    // else{
                    //     deferred.reject(false);
                    //     delete Settings.UserToken;
                    //     $cookies.remove('UserToken');
                        
                    //     $location.path("/login");
                    // }

                // })

                return deferred.promise;
            },

            GetAnonymous: function (url, data, method,reqheaders) {
                var deferred           = $q.defer(),
                    url                = Settings.DomainRequestName + url;
                    if(!data)
                        data           = {};

                if(method === undefined){
                    method = "GET";
                }                
                $rootScope.isWorking = true;

                $http({ 
                        method  : method, 
                        url     : url,
                        headers: data
                    })
                    .success(function(result, status, headers, config) {
                        deferred.resolve(result);
                        $rootScope.isWorking = false;
                    })
                    .error(function(result, status, headers, config) {
                        deferred.reject(status);
                        handleError(result, status, headers, config);
                        $rootScope.isWorking = false;
                    });
                

                return deferred.promise;
            },

            GetZip: function (url, data, method,reqheaders) {
                var deferred           = $q.defer(),
                    url                = Settings.DomainRequestName + url;
                    if(!data)
                        data           = {};
                    var getUserToken   = $cookies.get("UserToken") || Settings.UserToken;
                    var geTokenType   = $cookies.get("TokenType") || Settings.TokenType;

                    data.Authorization = geTokenType +" "+ getUserToken;

                if(method === undefined){
                    method = "GET";
                }

                // If the token is valid and is defined
                {
                    $rootScope.isWorking = true;

                    $http({ 
                            method  : method, 
                            url     : url,
                            responseType: 'arraybuffer',
                            headers: {
                                    'Content-type': 'application/json',
                                    // 'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                            }
                        })
                        .success(function(result, status, headers, config) {
                             var file = new Blob([result], {type: 'application/zip'});
                             var fileURL = URL.createObjectURL(file);
                             // $sce.trustAsResourceUrl(fileURL);
                            $window.open(fileURL, '_self', '');

                            deferred.resolve(result);
                            $rootScope.isWorking = false;
                        })
                        .error(function(result, status, headers, config) {
                            deferred.reject(status);
                            handleError(result, status, headers, config);
                            $rootScope.isWorking = false;
                        });
                }


                return deferred.promise;
            }

        };
    });


})( angular, inspinia );