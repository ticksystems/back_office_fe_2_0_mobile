(function( ng, inspinia ){

    "use strict";

    inspinia.factory('apiAlerts', [
        '$injector',
        function($injector) {

        	return {

        		/*------------------------------------------------------------------
	            [  Alerts  ]
	            */
                /**
                * addAlert
                *     error  : 'error',
                *     info   : 'info',
                *     wait   : 'wait',
                *     success: 'success',
                *     warning: 'warning'
                * @param {[type]} obj [description]
                */
                addAlert : function (obj){

                    var $rootScope  = $injector.get('$rootScope'),
                        toaster     = $injector.get('toaster'),
                        invocationIdLastGUID = this.invocationId();
                    obj.toastId = invocationIdLastGUID;
                    var alert = toaster.pop(obj);
                },

                /**
                 * removeAlert
                 * @param  {[type]} id [description]
                 */
                removeAlert : function(obj){
                    var toaster     = $injector.get('toaster');
                    if(obj && typeof(obj.id) != undefined){
                        toaster.clear(1, obj.id);
                    }
                    else
                        toaster.clear('*');
                    return true;
                },

                /*------------------------------------------------------------------
                [  GUID CREATION - InvocationID  ]
                */
                invocationId : function(){
                    var helpers = $injector.get('helpers');
                    return helpers.guid(); 
                }

        	}      

        }
    ]);

})( angular , inspinia )

inspinia.directive('bindUnsafeHtml', [function () {
    return {
        template: ' <div class="col-md-8">Text of note {{directiveData.name}} <a ui-sref="alerts.active">Go to Alert</a> </div>'
                    +'    <div class="col-md-2 pull-right text-right">'
                    +'        <h4>TOTAL: 10</h4>'
                    +'    </div>'
                    +'<div class="clearfix"></div>'
    };
}]);

inspinia.directive('bindHubMsg', [function () {
    return {
        template: ' <div class="">{{directiveData.data}}</div>'
                    // +'<div class="clearfix"></div>'
    };
}]);