(function( ng, inspinia ){

    "use strict";

    inspinia.factory('apiAuthorization', [
        '$q', 
        '$http', 
        '$location',
        '$rootScope',
        '$cookies',
        "api",
        function($q, $http, $location, $rootScope, $cookies, api) {

            return {

               /**
                 * init - Set auth permissions
                 */
                init : function() {

                    var self     = this,
                        deferred = $q.defer();

                    $rootScope.getPermissions = true;

                    var permissionsPromise = this.getAuthorization().then(function(data) {
                        var permissions = [];

                        if(!data.hasError){
                            // if(data.data.permissions !== undefined && data.data.permissions.length){
                            //     angular.forEach(data.data.permissions, function(v){
                            //         permissions[v['permission']] = v['value'];
                            //     })                   
                            // }
                            // $rootScope["permissions"] = permissions;

                            deferred.resolve(true);

                        }else{
                            
                            deferred.reject(false);
                        }

                        $rootScope.getPermissions = false;

                        return deferred.promise;
                    });
                    
                    // Set getted authorization
                    // if(!angular.equals({}, SystemData.Auth)){
                        SystemData.Auth = {
                            "management": {
                                "features": {
                                    "users": {
                                        "auth": true
                                    },
                                    "symbols": {
                                        "auth": true
                                    },
                                    "globalFilters": {
                                        "auth": true
                                    },
                                    "configurationHistory": {
                                        "auth": false
                                    }
                                },
                                "auth": true
                            },
                            "dashboard": {
                                "features": {},
                                "auth": true
                            },
                            "alerts": {
                                "features": {
                                    "active": {
                                        "auth": true
                                    },
                                    "history": {
                                        "auth": false
                                    }
                                },
                                "auth": true
                            }
                        }
                    // }else{
                        /*
                        SystemData.Auth = {
                            "management" : {
                                "features" : {
                                    // users list -> user (authorization) 
                                    "users"         : { 
                                        "read" : true, // enabled / disabled
                                        "write": false // action perform
                                    },
                                    "symbols"       : { 
                                        "read" : true, // enabled / disabled
                                        "write": false // action perform
                                    },
                                    "globalFilters" : { 
                                        "read" : true, // enabled / disabled
                                        "write": false // action perform
                                    },
                                    "configurationHistory" : { 
                                        "read" : true, // enabled / disabled
                                        "write": false // action perform
                                    }
                                },
                                "auth" : true
                            },
                            "dashboard" : {
                                "auth" : true
                            },
                            "alerts" : {
                                "features" : {
                                    "active"  : true,
                                    "history" : true
                                },
                                "auth" : true
                            }
                        };
                    }*/
                    
                    $rootScope.$broadcast("set-active-feature");

                    return deferred.promise;
                },

                /**
                 * getAuthorization
                 */
                getAuthorization : function() {
                    var data = {
                        "caller"    : Settings.Caller,
                        "userToken" : $cookies.get("UserToken") || Settings.UserToken,
                        "instanceId": Settings.InstanceID
                    }
                    // return api.PostAnonymous("/getUserPermissions",data);
                    return api.POSTtext("/getUserPermissions",data);
                },

                /**
                 * featureControlsAuth
                 * @param  {String} feature
                 */
                featureControlsAuth : function(feature){

                    $rootScope["permissions"]["permission"] = feature;

                }
                
            };     

        }
    ]);

})( angular , inspinia )