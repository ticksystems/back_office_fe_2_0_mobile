(function( ng, inspinia ){

    "use strict";

    inspinia.factory('apiErrorStack', [
        // 'apiLogging',
        '$window',
        function(/*apiLogging,*/$window) {



        	return {
                    /**
                     * prepareError 
                     * @param  {Object} error - Contain error object
                     * @param  {String} type  - Contain error type
                     */
        		    prepareError : function(error,type) {

                        /*------------------------------------------------------------------
                        [  Initialize ErrorStack  ]
                        */

                        // Cannot handle the error
                        if(error === undefined){
                            return false;
                        }
                        // if the is no set type
                        if(type === undefined)
                            type = "unknown";

                        // prepare error
                        var userAgent   = $window.navigator.userAgent,
                            errorLog    = {};

                        // Assemble browser ver. and app ver.
                        errorLog['browser']     = userAgent; 
                        errorLog['app-version'] = Settings.AppVersion;

                        /**
                         * Application errors
                         */
                        if(type === 'app'){
                            errorLog['message'] = error.message; 
                            errorLog['stack']   = error.stack;
                        }
                        /**
                         * Api errors
                         */
                        if(type === 'api'){
                            errorLog['data']    = error.data; 
                            errorLog['status']  = error.status; 
                            errorLog['method']  = error.method;
                            errorLog['url']     = error.url; 
                        }
                        /**
                         * Image errors
                         */
                        if(type === 'img'){
                            errorLog['src']           = error['ngSrc']; 
                            errorLog['replacedWith']  = error['errSrc'];
                            errorLog['id']            = error['id']; 
                            errorLog['alt']           = error['alt']; 
                        }
                        errorLog['errorType']   = type;

                        // Log Error
                        // apiLogging.logError(errorLog);
				    }

        	}      

        }
    ]);

})( angular , inspinia )

inspinia.config(function($provide) {
    $provide.decorator('$exceptionHandler', ['$log', '$delegate','apiErrorStack',
      function($log, $delegate,apiErrorStack) {
        return function(exception, cause) {

            apiErrorStack.prepareError(exception,'inspinia');
            $log.debug('Default exception handler.');
            $delegate(exception, cause);
        };
      }
    ]);
});

/**
 * Detect corrupt image log status as error and replace it if 
 * there is an error with it with another image
 * @param  {Object} apiErrorStack - error stack object                 
 */
inspinia.directive('errSrc', function(apiErrorStack) {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                if (attrs.src != attrs.errSrc) {
                    // Log Error
                    apiErrorStack.prepareError(attrs,'img');
                    attrs.$set('src', attrs.errSrc);
                }
            });
          
            attrs.$observe('ngSrc', function(value) {
                if (!value && attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});