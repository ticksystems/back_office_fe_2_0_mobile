(function( ng, inspinia ){

    "use strict";

    inspinia.factory('apiLogging', [
        '$injector',
        function($injector,$window) {

            var logs          = [],
            // Get dependencies (avoid circular dependency)
                helpers       = $injector.get('helpers'),
                $cookies      = $injector.get('$cookies'),
                $state        = $injector.get('$state'),
                apiBackOffice = $injector.get('apiBackOffice'),
                $q            = $injector.get('$q');

        	return {

	            /*------------------------------------------------------------------
	            [  Log Usage  ]
	            */
               
                lastLog   : {},

                /**
                 * logSystem - 
                 * @param  {Number} userId
                 */
                logSystem : function(userId){

                    // Log User/System Data
                    var payload = {
                        "ip"         : Settings.IP,
                        "systemData" : Settings.SystemData,
                        "userId"     : parseInt(userId),
                        "time"       : moment().format()
                    }
                    // console.log("User Info:",payload);
                    
                    /**
                     * userLog - user Log object for logging
                     * @type {Object} 
                     * @param ip           - user's IP Data
                     * @param systemData   - all info collected from user browser
                     * @param userId       - user's ID
                     * @param time         - date object
                     */    
                    
                    apiBackOffice.AddUserInfo(payload).then(
                        function(data){
                            // gtag('event', 'ipAndSystemData', payload);
                        }, 
                        function(){
                        }
                    );
                },

                /**
                 * logPageStay
                 * @param  {String}  page  
                 * @param  {Boolean} isExit
                 */
                logPageStay : function(page,isExit){

                    // Get current user id
                    var user     = helpers.exist($cookies.get('User'),JSON.stringify({'user_id':-1})),
                        userId   = JSON.parse(user).user_id,
                        deferred = $q.defer();

                    // Log User/System Data
                    var payload = {
                        "page"   : page,
                        "userId" : parseInt(userId),
                        "isExit" : !!isExit,
                        "time"   : moment().format()
                    }

                    // If user is logged in
                    if(parseInt(userId) != -1){
                        // if exit page
                        if((payload.isExit)){
                            // console.log("Exit from: ", payload);
                        }// if enter page
                        else{
                            this.lastLog = payload;
                            // console.log("Enter in: ", payload);
                        }

                        if(page == 'Login'){
                            deferred.resolve(true);
                        }else
                            apiBackOffice.AddUserEnterExit(payload).then(
                                function(data){
                                    deferred.resolve(true);
                                }, 
                                function(){
                                    deferred.reject(false);        
                                }
                            );
                    }else{
                        deferred.reject(false);
                    }

                    return deferred.promise;
                        
                },

                /**
                 * logLastVisit
                 * @param  {Object} lastLog
                 */
                logLastVisit: function(lastLog) {

                    var deferred = $q.defer();
                    // console.log("Exit from: ", lastLog);
                    // console.log("will log last visit",lastLog);
                    apiBackOffice.AddUserEnterExit(lastLog).then(
                        function(data){
                            deferred.resolve(true);
                        }, 
                        function(){
                            deferred.reject(false);        
                        }
                    );

                    return deferred.promise;
                },

                /**
                 * logUsage
                 * @param  {Object} el   
                 * @param  {String} page 
                 * @param  {Object} attrs
                 */
	            logUsage : function(el, page, attrs){

                    // Get current user id
                    var user = helpers.exist($cookies.get('User'),JSON.stringify({'user_id':-1})),
                        userId = JSON.parse(user).user_id;

                    /**
                     * userLog - user Log object for logging
                     * @type {Object} 
                     * @param userId       - user's ID
                     * @param systemData   - all info collected from user browser
                     * @param logMsg       - method name or message
                     * @param level        - element | page ...
                     * @param time         - Sat Oct 12 2018 17:47:52 GMT+0300 (Eastern European Summer Time)
                     * @param host         - Settings.DomainRequestName ex. http://...
                     * @param page         - ex. http://.../clientProfiles
                     * @param elementTxt   - ex. Search
                     * @param elementAttrs - ex. classes, disabled ...
                     */
                    var userLog = {
                        "userId"        : parseInt(userId),
                        "logMsg"        : "",
                        "level"         : "E",
                        "time"          : moment().format(),
                        "page"          : $state.current.data.pageTitle || "",
                        "params"        : $state.params || "",
                        "elementTxt"    : $.trim(el[0].outerText),
                        "elementType"   : $.trim(el[0].nodeName),
                        "elementName"   : $.trim(el[0].name)//,
                        // "elementAttrs"  : attrs
                        // "stackTrace"    : JSON.stringify(el)
                    }
                    if(ENV.ga)
                        // Google Analytics event
                        gtag('event', ($state.current.data.pageTitle || "")+" click/focus on : "+(userLog.elementTxt || userLog.elementName) + " "+ userLog.elementType, {
                            'event_category': userLog.elementType,
                            'event_label'   : (userLog.elementTxt || userLog.elementName)
                        });

                    apiBackOffice.AddUserClick(userLog).then(
                        function(data){
                            // console.log("success event log",data);
                        }, 
                        function(){
                        }
                    );

	            }

        	}      

        }
    ]);

})( angular , inspinia )

/**
 * usage - custom usage for logging
 * @param  {Object} apiLogging - logging factory
 */
inspinia.directive('usage', function(apiLogging, $location) {
    return {
        restrict: 'AEC',
        link: function(scope, element, attrs) {
          	element.bind('click', function() {
                if(attrs.usage.length)
                    // Log page, tab stay 
                    apiLogging.logPageStay(attrs.usage);
                else
                    // Log click, focus ...
                    apiLogging.logUsage(element,$location.absUrl(),attrs);
          	});
        }
    }
});