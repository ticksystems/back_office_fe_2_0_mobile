(function( ng, inspinia ) {
    
    "use strict";

    inspinia.directive("navbar", function($location,apiAuthorization) {
        return {

            // templateUrl: 'src/views/common/navbar.html',
            restrict : "A",
            controller: function($scope, $rootScope, $state){
                var routes = $rootScope.routes;
                
                $scope.oneAtATime = true;
                $scope.modules = [];

                $scope.setActiveFeature = function(){
                    
                    return routes;
                }

                // Listen when is logged in
                $scope.$on("loggedin", function(event, current, previous, rejection){
                    // TODO: SOME ACTIONS ON loggedin
                })

                $scope.modules = routes;

                // When everythig is setup init routes in module array
                $scope.$on("set-active-feature", function(event, current, previous, rejection){
                    $scope.modules = $scope.setActiveFeature();
                })

                $scope.setActiveFeature();

                // Listen when is logout
                $scope.$on("logout", function(event, current, previous, rejection){
                    // empty routes array
                    $scope.modules = [];
                })

            },

            link   : function(scope, element, attrs){
            }
        }
    });

})( angular, inspinia )

inspinia.directive('repeatLoader', function() {
    /**
     * Directive try to add animation in sidebar
     */
    return function(scope, element, attrs) {
        if (scope.$last){
            angular.element(element.parent()).removeClass("fadeOut animated hidden").css('visibility','visible').addClass("fadeIn animated");
        }
    };
})