/**
 * INSPINIA - Responsive Admin Theme
 *
 */
// (function () {
    var inspinia = angular.module('inspinia', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'angular-cache',                 // angular-cache
        'ngCookies',
        'toaster'
    ])
// })();