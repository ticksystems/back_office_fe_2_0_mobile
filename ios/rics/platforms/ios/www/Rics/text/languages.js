[
	{
		"id" 	: 1,
		"short"	: "en",
		"long"	: "English",
		"native": "English"
	},
	{
		"id" 	: 2,
		"short"	: "no",
		"long"	: "Norwegian",
		"native": "Norsk"
	},
	{
		"id" 	: 3,
		"short"	: "dk",
		"long"	: "Danish",
		"native": "Dansk"
	}
]